#include <QtCore/QDebug>
#include <QtSql/QSqlQuery>

#include "editsessionlogmodel.hpp"

EditSessionLogModel::EditSessionLogModel(QObject *parent) : QSqlQueryModel(parent)
{
    //
}

void EditSessionLogModel::initModel(int key)
{
    foreignKey = key;
    QString strDDL = "SELECT DISTINCT ";
    strDDL += "sessionlogdtl.id, ";
    strDDL += "sessionlogdtl.dtlseq, ";
    strDDL += "sessionlogdtl.rectime, ";
    strDDL += "status.Label ,";
    strDDL += "sessionlogdtl.callsign, ";
    strDDL += "sessionlogdtl.firstname, ";
    strDDL += "sessionlogdtl.city, ";
    strDDL += "sessionlogdtl.st, ";
    strDDL += "sessionlogdtl.gridsquare, ";
    strDDL += "loctype.Label, ";
    strDDL += "sessionlogdtl.traffic, ";
    strDDL += "sessionlogdtl.ares, ";
    strDDL += "sessionlogdtl.satern, ";
    strDDL += "sessionlogdtl.mars, ";
    strDDL += "sessionlogdtl.notes ";
    strDDL += "FROM sessionlogdtl, sessionloghdr, loctype, status ";
    strDDL += "WHERE sessionloghdr.id = ";
    strDDL += QString("%1").arg(foreignKey);
    strDDL += " AND sessionlogdtl.hdrndx = sessionloghdr.id ";
    setQuery(strDDL);
//    qDebug() << "SQL:" << strDDL;
    setHeaderData(0,  Qt::Horizontal, "ID");
    setHeaderData(1,  Qt::Horizontal, tr("Seq"));
    setHeaderData(2,  Qt::Horizontal, tr("Time"));
    setHeaderData(3,  Qt::Horizontal, tr("Status"));
    setHeaderData(4,  Qt::Horizontal, tr("  Call\n   Sign "));
    setHeaderData(5,  Qt::Horizontal, tr("     Name      "));
    setHeaderData(6,  Qt::Horizontal, tr("City"));
    setHeaderData(7,  Qt::Horizontal, tr("St/Pr"));
    setHeaderData(8,  Qt::Horizontal, tr("Grid"));
    setHeaderData(9,  Qt::Horizontal, tr("Loc \n Type  "));
    setHeaderData(10, Qt::Horizontal, tr("Tr"));
    setHeaderData(11, Qt::Horizontal, tr("ARES"));
    setHeaderData(12, Qt::Horizontal, tr("SATERN"));
    setHeaderData(13, Qt::Horizontal, tr("MARS"));
    setHeaderData(14, Qt::Horizontal, tr("         N O T E S         "));
}

Qt::ItemFlags EditSessionLogModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QSqlQueryModel::flags(index);
    switch ( index.column() )
    {
    case 3:
        flags |= Qt::ItemIsEditable;
        break;

    case 4:
        flags |= Qt::ItemIsEditable;
        break;

    case 5:
        flags |= Qt::ItemIsEditable;
        break;

    case 6:
        flags |= Qt::ItemIsEditable;
        break;

    case 7:
        flags |= Qt::ItemIsEditable;
        break;

    case 8:
        flags |= Qt::ItemIsEditable;
        break;

    case 9:
        flags |= Qt::ItemIsEditable;
        break;

    case 10: // TR
        flags |= Qt::ItemIsEditable;
        break;

    case 11: // ARES
        flags |= Qt::ItemIsEditable;
        break;

    case 12: // SATERN
        flags |= Qt::ItemIsEditable;
        break;

    case 13: //MARS
        flags |= Qt::ItemIsEditable;
        break;

    case 14:
        flags |= Qt::ItemIsEditable;
        break;

    default:
        break;
    }
    return flags;
}

bool EditSessionLogModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role);

    bool ok;

    QModelIndex primaryKeyIndex = QSqlQueryModel::index(index.row(), 0);
    QModelIndex foriegnKey = QSqlQueryModel::index(index.row(),1);
    Q_UNUSED(foriegnKey);  // saved for use later
    int id = data(primaryKeyIndex).toInt();

    clear();

    switch ( index.column() )
    {
    case 0:
        return false;  // cannot edit row id in log
        break;

    case 1:
        return false;  // cannot edit hdrndx in log
        break;

    case 2:
        return false;  // cannot edit seq in log
        break;

    case 3:
        qDebug() << "Editing col 3";
        ok = setStatus(id,value.toString());
        break;

    case 4:
        qDebug() << "Editing col 4";
        ok = setCallsign(id, value.toString());
        break;

    case 5:
        qDebug() << "Editing col 5";
        ok = setFirstName(id, value.toString());
        break;

    case 6:
        qDebug() << "Editing col 6";
        ok = setCity(id, value.toString());
        break;

    case 7:
        qDebug() << "Editing col 7";
        ok = setSt(id, value.toString());
        break;

    case 8:
        qDebug() << "Editing col 8";
        ok = setGrid(id, value.toString());
        break;

    case 9:
    {
        qDebug() << "Editing col 9";
        switch ( value.toInt() )
        {
        case 1:
//            qDebug() << "Editing col 9-fixed";
            ok = setLocTyp(id,"1");
            break;

        case 2:
//            qDebug() << "Editing col 9-mobile";
            ok = setLocTyp(id, "2");
            break;

        case 3:
//            qDebug() << "Editing col 9-qrp";
            ok = setLocTyp(id, "3");
            break;

        default:
//            qDebug() << "Editing col 9-default";
            ok = setLocTyp(id, "1");
            break;
        }
        break;
    }

    case 10:
//        qDebug() << "Editing col 10";
        ok = setTraffic(id,value);
        break;

    case 11:
//        qDebug() << "Editing col 11";
        ok = setARES(id, value);
        break;

    case 12:
//        qDebug() << "Editing col 12";
        ok = setSATERN(id, value);
        break;

    case 13:
//        qDebug() << "Editing col 13";
        ok = setMARS(id, value);
        break;

    case 14:
//        qDebug() << "Editing col 14";
        ok = setNotes(id, value.toString());
        break;

    default:
        break;
    }

    refresh();
    return ok;
}

void EditSessionLogModel::refresh()
{
    QString strDDL = "SELECT DISTINCT ";
    strDDL += "sessionlogdtl.id, ";
    strDDL += "sessionlogdtl.dtlseq, ";
    strDDL += "sessionlogdtl.rectime, ";
    strDDL += "status.Label, ";
    strDDL += "sessionlogdtl.callsign, ";
    strDDL += "sessionlogdtl.firstname, ";
    strDDL += "sessionlogdtl.city, ";
    strDDL += "sessionlogdtl.st, ";
    strDDL += "sessionlogdtl.gridsquare, ";
    strDDL += "loctype.Label, ";
    strDDL += "sessionlogdtl.traffic, ";
    strDDL += "sessionlogdtl.ares, ";
    strDDL += "sessionlogdtl.satern, ";
    strDDL += "sessionlogdtl.mars, ";
    strDDL += "sessionlogdtl.notes ";
    strDDL += "FROM sessionlogdtl, sessionloghdr, loctype, status ";
    strDDL += "WHERE sessionloghdr.id = ";
    strDDL += QString("%1").arg(foreignKey);
    strDDL += " AND sessionlogdtl.hdrndx = sessionloghdr.id ";
    strDDL += " AND (sessionlogdtl.status+1) = status.id ";
    strDDL += " AND sessionlogdtl.stationtype = loctype.id ";
    setQuery(strDDL);
    setHeaderData(0,  Qt::Horizontal, "ID");
    setHeaderData(1,  Qt::Horizontal, tr("Seq"));
    setHeaderData(2,  Qt::Horizontal, tr("Time"));
    setHeaderData(3,  Qt::Horizontal, tr("Status"));
    setHeaderData(4,  Qt::Horizontal, tr("  Call\n   Sign "));
    setHeaderData(5,  Qt::Horizontal, tr("     Name      "));
    setHeaderData(6,  Qt::Horizontal, tr("City"));
    setHeaderData(7,  Qt::Horizontal, tr("St/Pr"));
    setHeaderData(8,  Qt::Horizontal, tr("Grid"));
    setHeaderData(9,  Qt::Horizontal, tr("Loc \n Type  "));
    setHeaderData(10, Qt::Horizontal, tr("Tr"));
    setHeaderData(11, Qt::Horizontal, tr("ARES"));
    setHeaderData(12, Qt::Horizontal, tr("SATERN"));
    setHeaderData(13, Qt::Horizontal, tr("MARS"));
    setHeaderData(14, Qt::Horizontal, tr("         N O T E S         "));
}

bool EditSessionLogModel::setStatus(int logId, const QString &status)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set status = ? where id = ?");
    query.addBindValue(status.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setFirstName(int logId, const QString &firstName)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set firstname = ? where id = ?");
    query.addBindValue(firstName);
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setCallsign(int logId, const QString &call)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set callsign = ? where id = ?");
    query.addBindValue(call);
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setCity(int logId, const QString &city)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set city = ? where id = ?");
    query.addBindValue(city);
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setSt(int logId, const QString &st)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set st = ? where id = ?");
    query.addBindValue(st);
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setLocTyp(int logId, const QString &type)
{
    QSqlQuery query;
    qDebug() << "Type:" << type;
    query.prepare("UPDATE sessionlogdtl set stationtype = ? where id = ?");
    query.addBindValue(type.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setGrid(int logId, const QString &grid)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set gridsquare = ? where id = ?");
    query.addBindValue(grid);
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setTraffic(int logId, QVariant yesno)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set traffic = ? where id = ?");
    query.addBindValue(yesno.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setARES(int logId, QVariant yesno)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set ares = ? where id = ?");
    query.addBindValue(yesno.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setSATERN(int logId, QVariant yesno)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set satern = ? where id = ?");
    query.addBindValue(yesno.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setMARS(int logId, QVariant yesno)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set mars = ? where id = ?");
    query.addBindValue(yesno.toInt());
    query.addBindValue(logId);
    return query.exec();
}

bool EditSessionLogModel::setNotes(int logId, const QString &notes)
{
    QSqlQuery query;
    query.prepare("UPDATE sessionlogdtl set notes = ? where id = ?");
    query.addBindValue(notes);
    query.addBindValue(logId);
    return query.exec();
}

