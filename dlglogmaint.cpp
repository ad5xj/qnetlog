#include <QtCore/QDebug>
#include <QtCore/QPoint>
#include <QtCore/QSize>
#include <QtCore/QSettings>
#include <QtCore/QList>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QMessageBox>

#include "globals.hpp"
#include "nets.hpp"
#include "editsessionlogmodel.hpp"
#include "chkboxdelegate.hpp"
#include "sessionloghdr.hpp"
#include "sessionlogdtl.hpp"
#include "dlglogedit.hpp"

#include "dlglogmaint.hpp"

class DlgLogMaint::lmPrivateData
{
public:
    // local vars on the stack
    bool header_loaded;
    bool font_bold;
    bool font_italic;
    bool font_underline;

    quint16 color_r;
    quint16 color_g;
    quint16 color_b;
    quint16 color_a;
    quint16 logged_cnt;
    quint32 sizeh;
    quint32 font_size;
    quint32 m_netid;
    quint32 m_nethdrndx;
    quint32 m_netdtlndx;

    QPoint pos;
    QSize  size;

    QString m_netdate;
    QString netcolhdrs;
    QString dtlcolhdrs;
    QString netcolwidths;
    QString dtlcolwidths;
    QString font_family;
    QString lbls;

    QColor altColor;      // color to be used

    QFont font;
    QFont altFont;

    CheckBoxDelegate chkd;

    // local vars on the heap
    QMap<QString,quint16> *modesList;

    QSqlQueryModel *sqlmodel;

    Nets          *nets;
    SessionHeader *nethdr;
};

DlgLogMaint::DlgLogMaint(QWidget *parent) : QMainWindow(parent)
{
    lm_data = new lmPrivateData;
    lm_data->font_family = "";
    lm_data->lbls = "";
    lm_data->m_netdate = "";
    lm_data->netcolhdrs = "";
    lm_data->netcolwidths = "";
    lm_data->dtlcolhdrs = "";
    lm_data->dtlcolwidths = "";

    ui = new Ui::DlgLogMaint;

    ui->setupUi(this);
    ui->retranslateUi(this);

    createObjects();

    loadNetsCombo();
    connectSlots();

    readSettings();
    displaySettings();

    ui->btnGetDtls->setVisible(false);

    ui->tbrEdit->setVisible(false);
    ui->tabWidget->setCurrentIndex(0);
}

DlgLogMaint::~DlgLogMaint()
{
    delete ui;
    delete lm_data;
}

// ============================================================= //
// ===================  Event Handlers  ======================== //
// ============================================================= //
void DlgLogMaint::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DlgLogMaint::resizeEvent(QResizeEvent *e)
{
    Q_UNUSED(e)
    lm_data->size = size();
    saveSettings();
}

void DlgLogMaint::moveEvent(QMoveEvent *m)
{
    Q_UNUSED(m)
    lm_data->pos = pos();
    saveSettings();
}

void DlgLogMaint::closeEvent(QCloseEvent *)
{
    // save the log columns to stored string
    //qDebug() << "Window Closing....saving persistent data";
    quint16 x = 0;
    quint16 y = 0;
    quint16 z = 0;

    lm_data->netcolwidths = "";
    x = ui->tblLogHdr->columnCount();
    for ( y = 0; y < x; ++y )
    {
        if ( y > 0 ) lm_data->netcolwidths += "|";
        z = ui->tblLogHdr->columnWidth(y);
        lm_data->netcolwidths += QString("%1").arg(z);
    }

    lm_data->dtlcolwidths = "";
    x = ui->tblLogsDtl->model()->columnCount();
    for ( z = 0; z < x; ++z )
    {
        if ( z > 0 ) lm_data->dtlcolwidths += "|";
        y = ui->tblLogsDtl->columnWidth(z);
        lm_data->dtlcolwidths += QString("%1").arg(y);
    }
    lm_data->pos = this->pos();
    lm_data->size = this->size();
    saveSettings();
}

// ============================================================= //
// ===================  Slot Handlers  ========================= //
// ============================================================= //
void DlgLogMaint::slotTabSel(int ndx)
{
    if ( ndx < 0 ) return;  // should not ever happen.
    switch (ndx)
    {
    case 0:
        ui->tblLogsDtl->reset();
        ui->tblLogHdr->currentItem()->setSelected(false);
        ui->btnGetDtls->setVisible(false);
        lm_data->m_netdtlndx = 0;
        lm_data->m_nethdrndx = 0;
        break;

    case 1:
        slotGetDtls();
        break;
    default:
        ui->tblLogsDtl->reset();
        ui->tblLogHdr->currentItem()->setSelected(false);
        ui->btnGetDtls->setVisible(false);
        lm_data->m_netdtlndx = 0;
        lm_data->m_nethdrndx = 0;
        break;
    }
}

void DlgLogMaint::slotAddDtl()
{
    lm_data->m_netid = ui->tblLogHdr->currentRow();
    // get hdrndx for dtl foreign key
    lm_data->m_nethdrndx = ui->tblLogHdr->selectedItems().at(0)->text().toInt();
    lm_data->m_netdtlndx = 0;

    DlgLogEdit dlg;
    dlg.setHdrNdx(lm_data->m_nethdrndx);
    dlg.setDtlNdx(lm_data->m_netdtlndx);
    dlg.setModal(true);

    dlg.exec();
}

void DlgLogMaint::slotEditDtl()
{
    // the edit tool has been clicked so show the edit dialog
    // get index values for edit dialog
    lm_data->m_nethdrndx = ui->tblLogHdr->selectedItems().at(0)->text().toInt();
    /*
    lm_data->m_netdtlndx = ui->tblLogsDtl->model()->selectedItems().at(0)->text().toInt();
    // set index values into dialog before display
    DlgLogEdit *dlg = new DlgLogEdit(this);
    dlg->setHdrNdx(lm_data->m_nethdrndx);
    dlg->setDtlNdx(lm_data->m_netdtlndx);
    dlg->setModal(true);

    dlg->exec();
    */
}

void DlgLogMaint::slotDelDtl()
{
    // the delete tool has been clicked so show the confirm dialog
    QMessageBox msgBox;
    msgBox.setText("Are you sure you wish to delete this contact from the log?");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();
    switch (ret)
    {
    case QMessageBox::Yes:
        lm_data->m_nethdrndx = ui->tblLogHdr->selectedItems().at(0)->text().toInt();
//        lm_data->m_netdtlndx = ui->tblLogsDtl->selectedItems().at(0)->text().toInt();
        deleteContact();
        break;
    case QMessageBox::No:
        break;
    case QMessageBox::Cancel:
        break;
    }
    displayNetDtls();
}

void DlgLogMaint::slotSearch()
{
    //
}

void DlgLogMaint::slotSelectHdr()
{
    if ( ui->tblLogHdr->selectedItems().count() <= 0 ) return;
    // a row in the net log hdr table has been selected
    // save row number for reference
    lm_data->m_netid = ui->tblLogHdr->currentRow();
    // get hdrndx for dtl foreign key
    QString itemtext = ui->tblLogHdr->selectedItems().at(0)->text();
    lm_data->m_nethdrndx = itemtext.toInt();

    ui->btnGetDtls->setVisible(false);
//    qDebug() << QString("Row Selected - %1").arg(ui->tblLogHdr->currentRow());

    ui->tblLogsDtl->reset();
    lm_data->m_netid = ui->tblLogHdr->item(ui->tblLogHdr->currentRow(),0)->text().toInt();
    ui->btnGetDtls->setVisible(true);
}

void DlgLogMaint::slotSelectDtl()
{
    // a row in the detail display table has been selected
    // if the details tab is visible show the edit toolbar
    if ( ui->tabWidget->currentIndex() > 0 )
    {
        ui->tbrEdit->setVisible(true);
        ui->actionAddNewContact->setEnabled(false);
        ui->actionDelContact->setEnabled(true);
        ui->actionEditContact->setEnabled(true);
    }
    else
    {
        ui->tbrEdit->setVisible(false);
        ui->actionAddNewContact->setEnabled(true);
        ui->actionDelContact->setEnabled(false);
        ui->actionEditContact->setEnabled(false);
    }
}

void DlgLogMaint::slotDtlSel()
{
    // a row in the detail display table has been selected so show the edit button
    lm_data->m_netdtlndx = ui->tblLogHdr->selectedItems().at(0)->text().toInt();
}

void DlgLogMaint::slotGetDtls()
{
    int rw = 0;
    EditSessionLogModel *dtlmodel;
    dtlmodel = new EditSessionLogModel(this);

    ui->tblLogsDtl->reset();
    lm_data->logged_cnt = 0;
    lm_data->m_nethdrndx = ui->tblLogHdr->currentItem()->data(0).toInt();


    // model/view display
    // editing in the table done with EditSessionLogModel delegate
    dtlmodel->initModel(lm_data->m_nethdrndx);

    ui->tblLogsDtl->setModel(dtlmodel);
    ui->tblLogsDtl->show();
    ui->tblLogsDtl->setRowHeight(0,22);
    ui->tblLogsDtl->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked);

    lm_data->logged_cnt = dtlmodel->rowCount(QModelIndex());
    displayNetDtls();                      // display the results

    // get dtl rows from db for net hdr
    ui->tabWidget->tabBar()->setCurrentIndex(1);
    rw = ui->tblLogsDtl->model()->rowCount();
    if ( rw > 1 ) ui->tblLogsDtl->setVisible(true);
    ui->tbrEdit->setVisible(true);
    ui->actionAddNewContact->setEnabled(true);
    ui->actionDelContact->setEnabled(false);
    ui->actionEditContact->setEnabled(false);
}

void DlgLogMaint::slotNetChgd()
{
    // get ID from cbobox
    int id, x;
    x = ui->cboNets->currentIndex();
    if (x == 0) return;
    id = ui->cboNets->itemData(x).toInt();
    if ( ui->tblLogHdr->selectedItems().count() > 0 ) ui->tblLogHdr->currentItem()->setSelected(false);
    ui->tblLogHdr->clearContents();
    ui->tblLogHdr->reset();
    ui->tblLogHdr->setRowCount(0);
    ui->tblLogHdr->setFont(lm_data->font);
    resizeLogHdrCols(lm_data->netcolwidths);
    loadNetHdrLog(id);
}

void DlgLogMaint::slotPrint()
{
    // not yet
}

// ============================================================= //
// =================  Friendly Functions  ====================== //
// ============================================================= //
void DlgLogMaint::loadNetHdrLog(int id)
{
    // find record in net table
    SessionHeader hdr;

    QSqlQuery qry = hdr.findNets(id);
    qry.first();
    // check for valid query
    if (!qry.isValid() )
    {
        qDebug() << "no records found returning...";
        return;
    }
    // we have results to show
    int r = 0;

    // now load the display with results - store first item in table
    ui->tblLogHdr->insertRow(r);  // one more than header
    ui->tblLogHdr->setRowHeight(r,22);

    // fill in blanks in new row
    int n;
    QString s;
    QTableWidgetItem *netIdItem;
    QTableWidgetItem *netDtTimeItem;
    QTableWidgetItem *stTimeItem;
    QTableWidgetItem *timebaseItem;
    QTableWidgetItem *timeLenItem;
    QTableWidgetItem *stationsItem;
    QTableWidgetItem *sessionFreqItem;
    QTableWidgetItem *modeItem;

    n = qry.record().field("id").value().toInt();
    netIdItem = new QTableWidgetItem(QString("%1").arg(n),0);
    n = qry.record().field("timebase").value().toInt();
    switch (n)
    {
    case 0:
        s = "UTC";

    case 1:
        s = "Local";
    }
    timebaseItem = new QTableWidgetItem(s,0);
    s = qry.record().field("sessiondate").value().toString(); // net date time
    netDtTimeItem = new QTableWidgetItem(s,0);
    s = qry.record().field("sessionst").value().toString(); // net st time
    stTimeItem = new QTableWidgetItem(s,0);
    s = qry.record().field("sessionlen").value().toString();
    timeLenItem = new QTableWidgetItem(s,0);
    n = qry.record().field("stationcnt").value().toInt();
    stationsItem = new QTableWidgetItem(QString("%1").arg(n),0);
    s = qry.record().field("sessionfreq").value().toString();
    sessionFreqItem = new QTableWidgetItem(s,0);
    n = qry.record().field("netmode").value().toInt();
    if ( lm_data->modesList->find("SSB").value() == n )
        s = "SSB";
    else if ( lm_data->modesList->find("Digital").value() == n )
        s = "Digital";
    else if ( lm_data->modesList->find("AM").value() == n )
        s = "AM";
    else if ( lm_data->modesList->find("FM").value() == n )
        s = "FM";
    else
        s = "SSB";
    modeItem = new QTableWidgetItem(s,0);

    ui->tblLogHdr->setItem(r,0,netIdItem);
    ui->tblLogHdr->setItem(r,1,netDtTimeItem);
    ui->tblLogHdr->setItem(r,2,stTimeItem);
    ui->tblLogHdr->setItem(r,3,timebaseItem);
    ui->tblLogHdr->setItem(r,4,timeLenItem);
    ui->tblLogHdr->setItem(r,5,stationsItem);
    ui->tblLogHdr->setItem(r,6,sessionFreqItem);
    ui->tblLogHdr->setItem(r,7,modeItem);
    qry.next();  // to enter the processing loop
    if ( !qry.isValid() )
    { // EOF
        ui->tblLogHdr->setCornerButtonEnabled(false);
        ui->tblLogHdr->repaint();
        qry.finish();
        return;
    }
    // now if more than one -  load all left
    do
    {
        if ( !qry.isValid() ) break; // ?EOF? //
        // resize table
        ++r;
        ui->tblLogHdr->insertRow(r);
        ui->tblLogHdr->setRowHeight(r,28);

        // fill in blanks in new row
        n = qry.record().field("id").value().toInt();
        netIdItem = new QTableWidgetItem(QString("%1").arg(n),0);
        n = qry.record().field("timebase").value().toInt();
        switch (n)
        {
        case 0:
            s = "UTC";

        case 1:
            s = "Local";
        }
        timebaseItem = new QTableWidgetItem(s,0);
        s = qry.record().field("sessiondate").value().toString(); // net date time
        netDtTimeItem = new QTableWidgetItem(s,0);
        s = qry.record().field("sessionst").value().toString(); // net st time
        stTimeItem = new QTableWidgetItem(s,0);
        s = qry.record().field("sessionlen").value().toString();
        timeLenItem = new QTableWidgetItem(s,0);
        n = qry.record().field("stationcnt").value().toInt();
        stationsItem = new QTableWidgetItem(QString("%1").arg(n),0);
        s = qry.record().field("sessionfreq").value().toString();
        sessionFreqItem = new QTableWidgetItem(s,0);
        n = qry.record().field("netmode").value().toInt();
        if ( lm_data->modesList->find("SSB").value() == n )
            s = "SSB";
        else if ( lm_data->modesList->find("Digital").value() == n )
            s = "Digital";
        else if ( lm_data->modesList->find("AM").value() == n )
            s = "AM";
        else if ( lm_data->modesList->find("FM").value() == n )
            s = "FM";
        else
            s = "SSB";
        modeItem = new QTableWidgetItem(s,0);

        ui->tblLogHdr->setItem(r,0,netIdItem);
        ui->tblLogHdr->setItem(r,1,netDtTimeItem);
        ui->tblLogHdr->setItem(r,2,stTimeItem);
        ui->tblLogHdr->setItem(r,3,timebaseItem);
        ui->tblLogHdr->setItem(r,4,timeLenItem);
        ui->tblLogHdr->setItem(r,5,stationsItem);
        ui->tblLogHdr->setItem(r,6,sessionFreqItem);
        ui->tblLogHdr->setItem(r,7,modeItem);

        qry.next();  // to enter the processing loop
    } while ( qry.isValid() );
    ui->tblLogHdr->setCornerButtonEnabled(false);
    ui->tblLogHdr->repaint();
    qry.finish();
    resizeLogHdrCols(lm_data->dtlcolwidths);
}

void DlgLogMaint::loadNetsCombo()
{
    lm_data->nets = new Nets(this);

    //qDebug() << "Loading Nets Combo..";
    QSqlQuery n = lm_data->nets->findNets();
    n.first();
    n.last();
    //int rx = n.record().count();
    n.first();
    if (!n.isValid()) return;
    ui->cboNets->addItem("Select Net",0);
    // add the first one position now
    ui->cboNets->addItem(n.record().field("netname").value().toString(),n.record().field("id").value().toInt());
    while ( n.next() )
    {
        if ( !n.isValid() ) break;
        ui->cboNets->addItem(n.record().field("netname").value().toString(),n.record().field("id").value().toInt());
        if ( !n.isValid() ) break;
    }
}

void DlgLogMaint::resizeLogHdrCols(QString cols)
{    // resize the log columns by settings value
    int x = 0;
    int y = 0;
    QStringList col_list;
    QList<QString> lbls;

    // prepare display to show results
    lm_data->font.setBold(true);  // only headers are bold here
    ui->tblLogHdr->setFont(lm_data->font);
    ui->tblLogHdr->setColumnCount(9);
    lm_data->netcolhdrs = "ID|Net Dt|St Time|TZ|Duration|Count|Scheduled\n   Freq.  |Mode|Actual Freq.";
    //                     69|    69|     67|46|      68|   49|        120          |  74|  136
    lbls << lm_data->netcolhdrs.split("|",QString::KeepEmptyParts);
    ui->tblLogHdr->setHorizontalHeaderLabels(lbls);
    lm_data->font.setBold(false);
    ui->tblLogHdr->setFont(lm_data->font);

    // resize the log columns by stored string
    col_list = cols.split("|",QString::KeepEmptyParts);
    x = lbls.count();
    for ( y = 0; y < (x-1); ++y )
    {
        ui->tblLogHdr->setColumnWidth(y,col_list.at(y).toInt());
    }
    ui->tblLogHdr->repaint();
}

void DlgLogMaint::resizeLogDtlCols(QString cols)
{   // resize the dtl columns by settings value
    if ( lm_data->header_loaded ) return;

    int x = 0;
    int y = 0;
    QStringList col_list;
    QHeaderView *headerView;

    // only headers are bold here
    lm_data->font.setBold(false);
    headerView = ui->tblLogsDtl->horizontalHeader();
    headerView->setStretchLastSection(true);
    lm_data->sqlmodel->setHeaderData(0, Qt::Horizontal, "ID");
    lm_data->sqlmodel->setHeaderData(1, Qt::Horizontal, tr("Seq"));
    lm_data->sqlmodel->setHeaderData(2, Qt::Horizontal, tr("Time"));
    lm_data->sqlmodel->setHeaderData(3, Qt::Horizontal, tr("Status"));
    lm_data->sqlmodel->setHeaderData(4, Qt::Horizontal, tr("  Call\n   Sign "));
    lm_data->sqlmodel->setHeaderData(5, Qt::Horizontal, tr("     Name      "));
    lm_data->sqlmodel->setHeaderData(6, Qt::Horizontal, tr("City"));
    lm_data->sqlmodel->setHeaderData(7, Qt::Horizontal, tr("St/Pr"));
    lm_data->sqlmodel->setHeaderData(8, Qt::Horizontal, tr("Grid"));
    lm_data->sqlmodel->setHeaderData(9, Qt::Horizontal, tr("Loc \n Type  "));
    lm_data->sqlmodel->setHeaderData(10, Qt::Horizontal, tr("Tr"));
    lm_data->sqlmodel->setHeaderData(11, Qt::Horizontal, tr("ARES"));
    lm_data->sqlmodel->setHeaderData(12,Qt::Horizontal, tr("SATERN"));
    lm_data->sqlmodel->setHeaderData(13,Qt::Horizontal, tr("MARS"));
    lm_data->sqlmodel->setHeaderData(14, Qt::Horizontal, tr("         N O T E S         "));
    // 30|30|166|70|69|102|34|55|60|33|80|200
    lm_data->font.setBold(false);
    // resize the log columns by stored string
    col_list = cols.split("|",QString::KeepEmptyParts);
    x = col_list.count();
    for ( y = 0; y < x; ++y )
    {
        ui->tblLogsDtl->setColumnWidth(y,col_list.at(y).toInt());
    }


/*
    // prepare display to show results
    lm_data->font.setBold(true);  // only headers are bold here
    ui->tblLogsDtl->setFont(lm_data->font);
    // headers are not going to change so they are defined here
    lm_data->dtlcolhdrs     = "ID|Seq|TimeStamp|Callsign|Name|City|ST|Grid|Station\nType|Affiliation|Traffic|Notes";
//    lm_data->dtlcolwidths =  60| 60|      169|      75|  86|  80|49|  80|      80     |     74    |96     |200
    QList<QString> lbls = lm_data->dtlcolhdrs.split("|",QString::KeepEmptyParts);
    QList<QString> colwidths = lm_data->dtlcolwidths.split("|",QString::KeepEmptyParts);
//    ui->tblLogsDtl->setColumnCount(lbls.count());
    ui->tblLogsDtl->model()->setHeaderData(Qt::Horizontal,lbls);
    lm_data->font.setBold(false);
    ui->tblLogsDtl->setFont(lm_data->font);

    // resize the log columns by stored string
    int x = colwidths.count();
    for ( int y = 0; y < (x-1); ++y )
    {
        ui->tblLogsDtl->setColumnWidth(y,colwidths[y].toInt());
    }
*/
    ui->tblLogsDtl->repaint();
}

void DlgLogMaint::deleteContact()
{
    bool ret = false;
    SessionDetail dtl;
    SessionDtlRecord rec;

    rec.id = lm_data->m_netdtlndx;

    ret = dtl.del(rec);
    if ( !ret )
    {
        QMessageBox msgBox;
        msgBox.setText("Contact Delete");
        msgBox.setInformativeText("The contact was unable to be deleted. Try again later");
        msgBox.setStandardButtons(QMessageBox::Ok);
    }
    displayNetDtls();
}

void DlgLogMaint::createObjects()
{
    //
}

void DlgLogMaint::connectSlots()
{
    // connect actions and objects to private slots
    connect(ui->cboNets,           SIGNAL(currentIndexChanged(int)),this, SLOT(slotNetChgd()));
    connect(ui->actionExit,        SIGNAL(triggered()),             this, SLOT(close()));
    connect(ui->actionSelSession,  SIGNAL(triggered()),             this, SLOT(slotSelectHdr()));
    connect(ui->actionEditContact, SIGNAL(triggered(bool)),         this, SLOT(slotEditDtl()));
    connect(ui->actionDelContact,  SIGNAL(triggered()),             this, SLOT(slotDelDtl()));
    connect(ui->actionSearch,      SIGNAL(triggered()),             this, SLOT(slotSearch()));
    connect(ui->actionPrint,       SIGNAL(triggered()),             this, SLOT(slotPrint()));
    connect(ui->tblLogHdr,         SIGNAL(itemSelectionChanged()),  this, SLOT(slotSelectHdr()));
//    connect(ui->tblLogsDtl,        SIGNAL(doubleClicked(QModelIndex)),  this, SLOT(slotSelectDtl()));
    connect(ui->btnGetDtls,        SIGNAL(clicked()),               this, SLOT(slotGetDtls()));
    connect(ui->tabWidget,         SIGNAL(tabBarClicked(int)),      this, SLOT(slotTabSel(int)));
}

void DlgLogMaint::displayNetDtls()
{
    // get the details from the database
    // using the same model as in mainwindow
    quint16 r   = 0; // header record index
    int     ndx = 0;       // header record sel index

    SessionDetail dtl;
    EditSessionLogModel *dtlmodel;

    Q_UNUSED(r)
    Q_UNUSED(dtl)

    dtlmodel = new EditSessionLogModel(this);

    r = ui->tblLogHdr->currentRow();
    ndx = lm_data->m_nethdrndx;

    // model/view display
    // editing in the table done with EditSessionLogModel delegate
    dtlmodel->initModel(ndx); // set up the edit model with data for header with index ndx

    ui->tblLogsDtl->setModel(dtlmodel);
    ui->tblLogsDtl->show();
    ui->tblLogsDtl->setRowHeight(0,22);
    ui->tblLogsDtl->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked);

    // in the Ui - creating edit delegates
    ui->tblLogsDtl->setItemDelegateForColumn(10,&lm_data->chkd);
    ui->tblLogsDtl->setItemDelegateForColumn(11,&lm_data->chkd);
    ui->tblLogsDtl->setItemDelegateForColumn(12,&lm_data->chkd);
    ui->tblLogsDtl->setItemDelegateForColumn(13,&lm_data->chkd);

    resizeLogDtlCols(lm_data->dtlcolwidths);
    ui->tblLogsDtl->repaint();

}

void DlgLogMaint::displaySettings()
{
    move(lm_data->pos);
    resize(lm_data->size);

    lm_data->modesList = new QMap<QString,quint16>;
    lm_data->modesList->insert("SSB",0);
    lm_data->modesList->insert("Digital",1);
    lm_data->modesList->insert("AM",2);
    lm_data->modesList->insert("FM",3);

    QFont font;
    font.setFamily(lm_data->font_family);
    font.setPointSize(lm_data->font_size);
    font.setBold(lm_data->font_bold);
    font.setItalic(lm_data->font_italic);
    font.setUnderline(lm_data->font_underline);
    lm_data->altFont = font;

    resizeLogHdrCols(lm_data->netcolwidths);
}

void DlgLogMaint::readSettings()
{
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("LogMaint");
     lm_data->pos  = settings.value("pos", QPoint(100,200)).toPoint();
     lm_data->size = settings.value("size", QSize(656,583)).toSize();
    settings.endGroup();

    settings.beginGroup("LogHdrList");
     lm_data->color_r = settings.value("altRowColorR", "100").toInt();
     lm_data->color_g = settings.value("altRowColorG", "100").toInt();
     lm_data->color_b = settings.value("altRowColorB", "255").toInt();
     lm_data->color_a = settings.value("altRowColorA", "100").toInt();
     lm_data->altColor = QColor(lm_data->color_r,lm_data->color_g,lm_data->color_b,lm_data->color_a);
     lm_data->font_family = "DejaVu Sans";
     lm_data->font_family = settings.value("font_family", "DejaVu Sans").toString();
     int p = settings.value("font_point", 9).toInt();
     if ( p < 8 || p > 54 ) p = 10;
     lm_data->font_size  = p;
     lm_data->font_bold = false;
     if (settings.value("font_bold", "0").toString() == "1") lm_data->font_bold = true;
     lm_data->font_italic = false;
     if (settings.value("font_italic", "0").toString() == "1") lm_data->font_italic = true;
     lm_data->font_underline = false;
     if (settings.value("font_underline", "0").toString() == "1") lm_data->font_underline = true;
     lm_data->netcolwidths = settings.value("NetsColWidths","69|69|67|46|68|49|120|74|136").toString();
    settings.endGroup();

    settings.beginGroup("LogDtlList");
      lm_data->dtlcolwidths = settings.value("DtlColWidths","60|60|169|75|86|80|49|80|80|74|80|96|200").toString();
    settings.endGroup();
}

void DlgLogMaint::saveSettings()
{
    // save the log columns to stored string
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("LogMaint");
     settings.setValue("pos", pos());
     settings.setValue("size", size());
    settings.endGroup();

    settings.beginGroup("LogHdrList");
     settings.setValue("altRowColorR", lm_data->color_r);
     settings.setValue("altRowColorG", lm_data->color_g);
     settings.setValue("altRowColorB", lm_data->color_b);
     settings.setValue("altRowColorA", lm_data->color_a);
     settings.setValue("font_family",  lm_data->font_family);
     settings.setValue("font_point",   lm_data->font_size);
     settings.setValue("font_bold",    lm_data->font_bold);
     settings.setValue("font_italic",  lm_data->font_italic);
     settings.setValue("font_underline", lm_data->font_underline);
     settings.setValue("NetsColWidths", lm_data->netcolwidths);
    settings.endGroup();

    settings.beginGroup("LogDtlList");
     settings.setValue("DtlColWidths", lm_data->dtlcolwidths);
    settings.endGroup();
    settings.sync();
}
