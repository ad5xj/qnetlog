#ifndef DLGLOGMAINT_HPP
#define DLGLOGMAINT_HPP
/*! \file dlglogmaint.hpp  */
#include <QtCore/QEvent>
#include <QtGui/QFont>
#include <QtGui/QColor>
#include <QtGui/QCloseEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QMoveEvent>

#include "ui_dlglogmaint.h"

class QSqlQuery;

namespace Ui {
class DlgLogMaint;
}

/*! \ingroup QNETLOG
 * \details
 * This dialog provides database maintenance functions for the net log in the database.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotSelectHdr()<br />
 * <span style="color: green;">void</span>:slotSelectDtl()<br />
 * <span style="color: green;">void</span>:slotDtlSel()<br />
 * <span style="color: green;">void</span>:slotAddDtl()<br />
 * <span style="color: green;">void</span>:slotEditDtl()<br />
 * <span style="color: green;">void</span>:slotDelDtl()<br />
 * <span style="color: green;">void</span>:slotSearch()<br />
 * <span style="color: green;">void</span>:slotNetChgd()<br />
 * <span style="color: green;">void</span>:slotGetDtls()<br />
 * <span style="color: green;">void</span>:slotTabSel(<span style="color: green;">int</span> ndx)<br /> */
class DlgLogMaint : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief The DlgLogMaint class
     * \param QWidget parent
     * \sa DATABASE                  */
    explicit DlgLogMaint(QWidget *parent = 0);

    /*!
     * \brief
     * &nbsp;&nbsp;DlgLogMaint::<span style="color: green;">~DlgLogMaint()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~DlgLogMaint();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *);
    void moveEvent(QMoveEvent *);
    void resizeEvent(QResizeEvent *);

private Q_SLOTS:
    void slotSelectHdr();
    void slotSelectDtl();
    void slotDtlSel();
    void slotAddDtl();
    void slotEditDtl();
    void slotDelDtl();
    void slotSearch();
    void slotNetChgd();
    void slotGetDtls();
    void slotPrint();
    void slotTabSel(int ndx);

private:
    /*! \class lmPrivateData
     * \brief Local definition of private data  */
    class lmPrivateData;
    lmPrivateData *lm_data;                //!< *d-ptr to local private data values

    Ui::DlgLogMaint *ui;                   //!< *d-ptr to GUI form

    void createObjects();
    void connectSlots();
    void loadNetsCombo();
    void loadNetHdrLog(int id);
    void resizeLogHdrCols(QString cols);
    void resizeLogDtlCols(QString cols);
    void displayNetDtls();
    void displaySettings();
    void deleteContact();
    void readSettings();
    void saveSettings();
    QSqlQuery getNetLog();
};
#endif // DLGLOGMAINT_HPP
