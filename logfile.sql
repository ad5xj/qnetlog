BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS bands
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    ndx     INTEGER,
    Label   VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS callsigns
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    call     VARCHAR(10),
    operator VARCHAR(30),
    city     VARCHAR(50),
    st       VARCHAR(5),
    grid     VARCHAR(6),
    marscall VARCHAR(10),
    notes    VARCHAR(254)
);

CREATE TABLE IF NOT EXISTS modes
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    dnx     INTEGER,
    Lable   VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS nets
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    timebase    BOOLEAN,
    occurs      INTEGER,
    dayofweek   INTEGER,
    netmode     VARCHAR(10),
    netband     VARCHAR(6),
    netsttime   VARCHAR(6),
    netname     VARCHAR(50),
    netfreq     VARCHAR(17),
    netnotes    VARCHAR(254)
);

CREATE TABLE IF NOT EXISTS sessionloghdr
(
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    netid        INTEGER,
    timebase     INTEGER,
    sessionmode  INTEGER,
    sessionoccr  INTEGER,
    sessiondays  INTEGER,
    sessionband  INTEGER,
    stationcnt   INTEGER,
    sessiondate  VARCHAR(8),
    sessionfreq  VARCHAR(15),
    sessionst    VARCHAR(12),
    sessionend   VARCHAR(12),
    sessionlen   VARCHAR(10),
    sessionncs   VARCHAR(10),
    sessionnotes VARCHAR(254)
);

CREATE TABLE IF NOT EXISTS sessionlogdtl
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    hdrndx      INTEGER,
    dtlseq      INTEGER,
    stationtype INTEGER,
    timebase    INTEGER,
    traffic     INTEGER,
    ares        INTEGER,
    satern      INTEGER,
    mars        INTEGER,
    rectime     VARCHAR(20),
    status      VARCHAR(10),
    callsign    VARCHAR(10),
    marscall    VARCHAR(10),
    firstname   VARCHAR(50),
    city        VARCHAR(50),
    st          VARCHAR(3),
    gridsquare  VARCHAR(6),
    notes       VARCHAR(254)
);

CREATE TABLE IF NOT EXISTS loctype
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    Label       VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS status
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    Label       VARCHAR(15)
);

CREATE TABLE IF NOT EXISTS contcols
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    seq         INTEGER NOT NULL,
    hdrtxt      VARCHAR(20) NOT NULL,
    dbfield     VARCHAR(20) NOT NULL,
    adifnam     VARCHAR(20)
);

CREATE TABLE sqlite_sequence(name,seq);
INSERT INTO sqlite_sequence VALUES('nets',3);
INSERT INTO sqlite_sequence VALUES('callsigns',3);
INSERT INTO sqlite_sequence VALUES('modes',5);
INSERT INTO sqlite_sequence VALUES('bands',16);
INSERT INTO sqlite_sequence VALUES('sessionlogdtl',1);
INSERT INTO sqlite_sequence VALUES('sessionloghdr',1);
CREATE INDEX [callidx]  ON [callsigns] ( [call] );
CREATE INDEX [grididx]  ON [callsigns] ( [grid] );
CREATE INDEX [stateidx] ON [callsigns] ( [st] );
COMMIT;

BEGIN TRANSACTION;
INSERT INTO bands VALUES(1,1,'160m');
INSERT INTO bands VALUES(2,2,'75/80m');
INSERT INTO bands VALUES(3,3,'60m');
INSERT INTO bands VALUES(4,4,'40m');
INSERT INTO bands VALUES(5,5,'30m');
INSERT INTO bands VALUES(6,6,'20m');
INSERT INTO bands VALUES(7,7,'17m');
INSERT INTO bands VALUES(8,8,'15m');
INSERT INTO bands VALUES(9,9,'12m');
INSERT INTO bands VALUES(10,10,'10m');
INSERT INTO bands VALUES(11,11,' 6m');
INSERT INTO bands VALUES(12,12,'2m');
INSERT INTO bands VALUES(13,13,'1.5m');
INSERT INTO bands VALUES(14,14,'70cm');
INSERT INTO bands VALUES(15,15,'UHF');
INSERT INTO bands VALUES(16,16,'SHF');

INSERT INTO callsigns VALUES(1,'AD5XJ','Ken','Houma','LA','EL49po',NULL,'SATERN So. Terr. Net Mgr.');
INSERT INTO callsigns VALUES(2,'WB8BZH','Bill','Jackson','MS','EM48xx',NULL,NULL);
INSERT INTO callsigns VALUES(3,'KN5GRK','Herman','Lafayette','LA','EM40zo',NULL,'Satern Affiliate');

INSERT INTO modes VALUES(1,1,'LSB');
INSERT INTO modes VALUES(2,2,'USB');
INSERT INTO modes VALUES(3,3,'Digital');
INSERT INTO modes VALUES(4,4,'FM');
INSERT INTO modes VALUES(5,5,'AM');

INSERT INTO loctype VALUES(1,'Fixed');
INSERT INTO loctype VALUES(2,'Mobile');
INSERT INTO loctype VALUES(3,'Port/QRP');

INSERT INTO status VALUES(1,'Contacted');
INSERT INTO status VALUES(2,'Early Ck In');
INSERT INTO status VALUES(3,'Reg Chk in');
INSERT INTO status VALUES(4,'Check Out');
INSERT INTO status VALUES(5,'Comments');
INSERT INTO status VALUES(6,'Short time');
INSERT INTO status VALUES(7,'Traffic');
INSERT INTO status VALUES(8,'Re-Check');
INSERT INTO status VALUES(9,'Radio Check');

INSERT INTO nets VALUES(1,2,3,2,1,3,'10:00 AM','SATERN So. Terr. SSB net','7.262 Mhz','Rotating NCS operators');
INSERT INTO nets VALUES(2,1,3,2,3,5,'12:00 PM','SATERN SO. Terr. Digital Net','14.065 Mhz','Rotating NCS operators. OLIVIA Mode 8/500 +1000. Practice training messages sent in other modes.');
INSERT INTO nets VALUES(3,1,3,1,1,1,'07:30 PM','LA Section ARES HF SSB Net','3.873 Mhz','Rotating NCS operators. All stations welcome. EchoLink session after the net.');

insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('1', '0', '#', 'id', 'app_qnetlogger_id');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('2', '1', 'QSO DATE', 'qso_date', 'qso_date');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('3', '2', 'TIME ON', 'timeon', 'timeon');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('4', '3', 'TIME OFF', 'timeoff', 'timeoff');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('5', '4', 'CALL', 'callsign', 'call');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('6', '5', 'FIRST NAME', 'fname', 'name');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('7', '6', 'BAND', 'band', 'band');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('8', '7', 'MODE', 'mode', 'mode');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('9', '8', 'FREQUENCY', 'frequency', 'freq');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('10', '9', 'LAST NAME', 'lname', 'app_qnetlogger_lname');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('11', '10', 'QTH CITY', 'addrqth', 'city');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('12', '11', 'ST', 'addrstate', 'state');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('13', '12', 'GRid SQ', 'grid', 'gridsquare');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('14', '13', 'ARRL SECTION', 'arrlsection', 'arrl');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('15', '14', 'DXCC', 'dxcc', 'dxcc');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('16', '15', 'CONT', 'cont', 'cont');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('17', '16', 'COUNTRY', 'country', 'country');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('18', '17', 'ADDRESS 1', 'addr1', 'address');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('19', '18', 'ADDRESS 2', 'addr2', 'app_qnetlogger_addr2');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('20', '19', 'ZIPCODE', 'addrzip', 'zip');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('21', '20', 'TZ', 'timezone', 'app_qnetlogger_tz');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('22', '21', 'QSL via', 'qslvia', 'qslvia');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('23', '22', 'QSL MESSAGE', 'qslmsg', 'qslmsg');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('24', '23', 'RST Sent', 'rstsent', 'rstsent');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('25', '24', 'RST Rcvd', 'rstrcvd', 'rstrcvd');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('26', '25', 'PREFIX', 'prefix', 'pfx');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('27', '26', 'IOTA', 'iota', 'iota');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('28', '27', 'OPERATOR', 'operator', 'oper');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('31', '30', 'CQZ', 'cqzone', 'cqz');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('32', '31', 'ITUZ', 'ituzone', 'ituz');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('33', '32', '10-10', 'tenten', 'app_qnetlogger_tenten');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('34', '33', 'eQSL Sent', 'eqslsent', 'app_qnetlogger_eqslsent');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('35', '34', 'eQSL Rcvd', 'eqslrcvd', 'app_qnetlogger_eqslrcvd');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('36', '35', 'LoTW Sent', 'lotwsent', 'app_qnetlogger_lotwsent');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('37', '36', 'LoTW Rcvd', 'lotwrcvd', 'app_qnetlogger_lotwrcvd');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('38', '37', 'Notes For This QSO', 'notes', 'notes');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('39', '38', 'USER 1', 'user1', 'user1');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('40', '39', 'USER 2', 'user2', 'user2');
insert into contcols ("id", "seq", "hdrtxt", "dbfield", "adifnam") values ('41', '40', 'USER 3', 'user3', 'user3');
COMMIT;
