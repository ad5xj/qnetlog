#ifndef CALLSIGNS_HPP
#define CALLSIGNS_HPP
/*! \file callsigns.hpp */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>

#include "globals.hpp"
/*! \ingroup DATABASE
 * \class CallSigns
 * \details
 * Constructs a database interface to the Callsigns table. Implementation is the same
 * no matter what database server is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">bool</span>:save(CallSignsRecord rec)<br />
 * <span style="color: green;">bool</span>:del(CallSignsRecord rec)<br />
 * <span style="color: green;">bool</span>:update(CallSignsRecord rec)<br />
 * <span style="color: green;">bool</span>:findCallSignsRecord(CallSignsRecord rec)<br />
 * <span style="color: green;">bool</span>:findACallSign(QString strCall)(int id)<br />
 * <span style="color: green;">QSqlQuery</span>:findAll()<br />
 * <span style="color: green;">QSqlQuery</span>:findID(quint32 id)<br />
 * <span style="color: green;">QSqlQuery</span>:findCallSigns(QString callstr)<br /><br /> */
class CallSigns : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)        //!<
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
     * \brief
     * <span style="color: green;">QObject::Callsigns(QWidget * parent = 0)</span><br><br>
     * &nbsp;&nbsp;The implementation of the interface to the Callsigns table in the database
     * provides a wrapper around the SQL functions needed to access and maintain
     * records in the callsigns table of the database.
     * \param QObject *parent                            */
    explicit CallSigns(QObject *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;Bands::<span style="color: green;">~Callsigns(QWidget * parent = 0)</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~CallSigns();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(CallsignsRecord rec)</span><br /><br />
     * SQL function to save callsign record. This is an interface functiont to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(CallSignsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(CallSignsRecord rec)</span><br /><br />
     * SQL function to delete a specific callsign record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(CallSignsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(CallSignsRecord rec)</span><br /><br />
     * SQL function to update a specific callsign record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(CallSignsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">findCallSignsRecord(CallSignsRecord rec)</span><br /><br />
     * SQL function returns true if callsign is found for a specific callsign record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool findCallSignsRecord(CallSignsRecord rec); //!<

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">findACallSign(QString strCall)</span><br /><br />
     * SQL function returns true if callsign is found for a specific callsign record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool findACallSign(QString strCall);       //!< returns true if callsign specific string exists

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findCallSignsRecord(CallSignsRecord rec)</span><br /><br />
     * SQL query returning all rows in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Success returns QSqlQuery result of all rows - falure returns null             */
    QSqlQuery findAll();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findID(CallSignsRecord rec)</span><br /><br />
     * SQL query returns SQL query result of one row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Success returns QSqlQuery result of rows with ID - falure returns null             */
    QSqlQuery findID(quint32 id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findCallSigns(QStrong callstr)</span><br /><br />
     * SQL query returns SQL query result of rows that match the pattern in callstr. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Success returns QSqlQuery result of all rows - falure returns null             */
    QSqlQuery findCallSigns(QString callstr);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;

};

#endif // CALLSIGNS_HPP
