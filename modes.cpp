#include <QtCore/QDebug>
#include <QtSql/QSqlError>

#include "modes.hpp"

Modes::Modes(QObject *parent) : QObject(parent)
{
    // Modes database maintenance implementation
}

Modes::~Modes()
{
    //
}

bool Modes::del(ModesRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    if ( rec.id <= 0 )
    {
        qDebug() << "DB Error no record ID specified: err - " << err << " " << errmsg;
        return success;
    }

    sqlStr  = "DELETE FROM modes ";
    sqlStr += "WHERE id=" + QString("%1").arg(rec.id) + ", ');";
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error deleteing Modes data: err - " << err << " " << errmsg;
        success = false;
    }
    return success;
}

bool Modes::save(ModesRec rec)
{
    bool success = false;
    QString sqlStr;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);

    //qDebug() << "creating new record to add...";

    sqlStr  = "INSERT INTO Modes ( ndx, Label ) ";
    sqlStr += "VALUES (" + QString("%1").arg(rec.ndx) + ", ');";
    qDebug() << "preparing ADD SQL:" << sqlStr;
    query.prepare(sqlStr);
    //qDebug() << "executing...";
    query.exec();
    err = query.lastError().type();
    errmsg = query.lastError().text();
    if ( err == QSqlError::NoError )
    {
      //qDebug() << "DB Error saving Modes data: err - " << err << " " << errmsg;
      success = true;
    }
    //qDebug() << "ADD query success ";
    return success;
}

bool Modes::update(ModesRec rec)
{
    bool success = false;
    QString sqlStr = "";
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query(db);


    //qDebug() <<  "creating record update criteria...";
    sqlStr  = "UPDATE Modes SET ";
    sqlStr += "ndx=" + QString("%1").arg(rec.ndx) + ", ";
    sqlStr += "Lable='" + rec.Label + "' ";
    sqlStr += " WHERE id = ";
    sqlStr += QString("%1").arg(rec.id) + "; ";
    qDebug() <<  "preparing UPDT SQL:" << sqlStr;
    query.prepare(sqlStr);
    //qDebug() << "executing...";
    query.exec();
    if ( query.lastError().type() == QSqlError::NoError ) success = true;
    err = query.lastError().number();
    errmsg = query.lastError().text();
    //qDebug() << "UPDATE query success ";
    return success;
}

QSqlQuery Modes::findModesRecord(int id)
{
    QString sqlStr;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);

    //qDebug() << "selecting records from table Modes...";

    sqlStr  = "SELECT (id, ndx, Lable) FROM Modes WHERE id = " + QString("%1").arg(id);
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if (qry.isValid())
    {
        //qDebug() << "Specific net query success ";
    }
    else
    {
        //qDebug() << "Specific net query invalid ";
        return qry;
    }
    return qry;
}

QSqlQuery Modes::findModesID(int id)
{
    QString sqlStr;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);

    //qDebug() << "selecting records from table Modes...";

    sqlStr  = "SELECT (id, ndx, Lable) FROM Modes WHERE id = " + QString("%1").arg(id);
    sqlStr += " ORDER BY ndx";
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    //qDebug() << "Modes query success ";
    return qry;
}

QSqlQuery Modes::findModes()
{
    QString sqlStr;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);

    //qDebug() << "selecting records from table Modes...";

    sqlStr  = "SELECT id, dnx, Lable FROM Modes ORDER BY dnx";
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    //qDebug() << "Modes query success ";
    return qry;
}


