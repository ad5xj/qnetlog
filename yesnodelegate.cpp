#include <QtWidgets/QCheckBox>

#include "yesnodelegate.hpp"

YesNoDelegate::YesNoDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{

}

YesNoDelegate::~YesNoDelegate()
{
    //
}


QWidget* YesNoDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    QCheckBox *selector = new QCheckBox(parent);
    selector->setChecked(false);
    selector->setText(tr("Checked is YES"));
    return selector;
}

void YesNoDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(index)

    //    qint16 val = index.model()->data(index, Qt::EditRole).toInt();

    QCheckBox *selector = static_cast<QCheckBox*>(editor);
    selector->isChecked();
}

void YesNoDelegate::setModelData(QWidget *selector, QAbstractItemModel *model, const QModelIndex &index) const
{
    Q_UNUSED(index)

    // find the item in the model and update it
    QCheckBox *chkBox = static_cast<QCheckBox*>(selector);
    model->setData(index, chkBox->isChecked(), Qt::EditRole);
}

void YesNoDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    // resize to container
    editor->setGeometry(330,20,95,25);
}
