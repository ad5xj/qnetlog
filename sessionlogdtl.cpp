#include <QtCore/QDebug>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>

#include "sessionlogdtl.hpp"

SessionDetail::SessionDetail(QObject *parent) : QObject(parent)
{
    //
}

SessionDetail::~SessionDetail()
{
    //
}

QSqlQueryModel* SessionDetail::getCurSession(int ndx)
{
    QString strDDL = "SELECT DISTINCT ";
    strDDL += "sessionlogdtl.id, ";
    strDDL += "sessionlogdtl.dtlseq, ";
    strDDL += "sessionlogdtl.rectime, ";
    strDDL += "sessionlogdtl.status, ";
    strDDL += "sessionlogdtl.callsign, ";
    strDDL += "sessionlogdtl.firstname, ";
    strDDL += "sessionlogdtl.city, ";
    strDDL += "sessionlogdtl.st, ";
    strDDL += "sessionlogdtl.gridsquare, ";
    strDDL += "sessionlogdtl.stationtype, ";
    strDDL += "sessionlogdtl.traffic, ";
    strDDL += "sessionlogdtl.ares, ";
    strDDL += "sessionlogdtl.satern, ";
    strDDL += "sessionlogdtl.mars, ";
    strDDL += "sessionlogdtl.notes ";
    strDDL += "FROM sessionlogdtl sessionloghdr ";
    strDDL += "JOIN sessionlogdtl ";
    strDDL += "ON sessionlogdtl.hdrndx = sessionloghdr.id ";
    strDDL += "WHERE sessionloghdr.id = ";
    strDDL += QString("%1").arg(ndx);
    //qDebug() << strDDL;
    QSqlQueryModel *sqlmodel = new QSqlQueryModel(this);
    sqlmodel->setQuery(strDDL);
    return sqlmodel;
}

QSqlQuery SessionDetail::getLogEntry(int id)
{
    QString strDML;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);

    strDML =  "SELECT DISTINCT id, ";
    strDML += "hdrndx, ";
    strDML += "dtlseq, ";
    strDML += "rectime, ";
    strDML += "status, ";
    strDML += "callsign, ";
    strDML += "marscall, ";
    strDML += "firstname, ";
    strDML += "city, ";;
    strDML += "st, ";
    strDML += "gridsquare, ";
    strDML += "stationtype, ";
    strDML += "traffic, ";
    strDML += "ares, ";
    strDML += "satern, ";
    strDML += "mars, ";
    strDML += "notes ";
    strDML += "FROM sessionlogdtl ";
    strDML += "WHERE id = ";
    strDML += QString("%1").arg(id);
    //qDebug() << "Preparing SessionDetail SELECT SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "Error on query of log entries:" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery SessionDetail::getNetLog(int hdrndx)
{
    QString strDML;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);

    strDML =  "SELECT DISTINCT id,";
    strDML += "hdrndx, ";
    strDML += "dtlseq, ";
    strDML += "rectime, ";
    strDML += "status, ";
    strDML += "callsign, ";
    strDML += "marscall, ";
    strDML += "firstname, ";
    strDML += "city, ";;
    strDML += "st, ";
    strDML += "gridsquare, ";
    strDML += "stationtype, ";
    strDML += "traffic, ";
    strDML += "ares, ";
    strDML += "satern, ";
    strDML += "mars, ";
    strDML += "notes ";
    strDML += "FROM sessionlogdtl ";
    strDML += "WHERE hdrndx = ";
    strDML += QString("%1").arg(hdrndx);
    strDML += " ORDER BY dtlseq ASC";
    // qDebug() << "Preparing SessionDetail SELECT SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error executing query: err:" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    else
    {
        err = QSqlError::NoError;
        errmsg = "";
    }
    return qry;
}

bool SessionDetail::save(SessionDtlRecord rec)
{
    bool success;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    success = false;
    // the id column is autoincrement so we do not supply a value
    strDML  = "INSERT INTO sessionlogdtl (";
    strDML += "hdrndx, ";
    strDML += "dtlseq, ";
    strDML += "rectime, ";
    strDML += "status, ";
    strDML += "callsign, ";
    strDML += "marscall, ";
    strDML += "firstname, ";
    strDML += "city, ";;
    strDML += "st, ";
    strDML += "gridsquare, ";
    strDML += "stationtype, ";
    strDML += "traffic, ";
    strDML += "ares, ";
    strDML += "satern, ";
    strDML += "mars, ";
    strDML += "notes ";
    strDML += ")";
    strDML += " VALUES ";
    strDML += "(";
    strDML += QString("%1").arg(rec.hdrndx);
    strDML += ", ";
    strDML += QString("%1").arg(rec.dtlseq);
    strDML += ", ";
    if ( rec.timebase == 0 )
        strDML += "'" + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "', ";
    else if ( rec.timebase == 1 )
        strDML += "'" + QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd hh:mm:ss") + "', ";
    else
        strDML += "'" + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "', ";
    strDML += "1, ";
    strDML += "'" + rec.callsign + "',";
    strDML += "'" + rec.marscall + "',";
    strDML += "'" + rec.firstname + "', ";
    strDML += "'" + rec.city + "', ";
    strDML += "'" + rec.st + "', ";
    strDML += "'" + rec.gridsquare + "', ";
    strDML += QString("%1").arg(rec.stationtype) + ", ";
    if ( rec.traffic ) strDML += "1, "; else strDML += "0, ";
    if ( rec.ares )    strDML += "1, "; else strDML += "0, ";
    if ( rec.satern )  strDML += "1, "; else strDML += "0, ";
    if ( rec.mars )    strDML += "1, "; else strDML += "0, ";
    strDML += "'" + rec.notes + "');";

    qry.exec(strDML);
    if (qry.lastError().type() != QSqlError::NoError )
    {
        err    = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error executing ADD query: err" << QString("%1").arg(err) << " " << errmsg;
        qDebug() << "SQL:" << strDML;
        success = false;
    }
    else
    {
        success = true;
        err = QSqlError::NoError;
        errmsg = "";
    }
    return success;
}


bool SessionDetail::update(SessionDtlRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    // note: not all fields are updated here - just those that should change
    strDML  = "UPDATE sessionlogdtl SET ";
    strDML += "  rectime='" + rec.rectime +"' ";
    strDML += ", dtlseq=" + QString("%1").arg(rec.dtlseq) + " ";
    strDML += ", status='"  + rec.status + "'";
    strDML += ", callsign='" + rec.callsign + "'";
    strDML += ", marscall='" + rec.marscall + "'";
    if (rec.timebase == false)
        strDML += ", timebase=0";
    else
        strDML += ", timebase=1";
    strDML += ", firstname='"  + rec.firstname + "'";
    strDML += ", city='" + rec.city + "'";
    strDML += ", st='" + rec.st + "'";
    strDML += ", gridsquare='" + rec.gridsquare + "'";
    strDML += ", traffic=" + QString("%1").arg(rec.traffic);
    strDML += ", ares=" + QString("%1").arg(rec.ares);
    strDML += ", satern=" + QString("%1").arg(rec.satern);
    strDML += ", mars=" + QString("%1").arg(rec.mars);
    strDML += ", stationtype=" + QString("%1").arg(rec.stationtype);
    strDML += ", notes='" + rec.notes + "' ";
    strDML += "WHERE id = ";             // this is id of the actual contact record
    strDML += QString("%1").arg(rec.id) + "; ";
    // qDebug() <<  "preparing UPDATE SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: err-" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text()
                 << "SQL:" << strDML;
        return false;
    }
    return true;
}

bool SessionDetail::del(SessionDtlRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML =  "DELETE FROM sessionlogdtl ";
    strDML += "WHERE id= " + rec.id;
    //qDebug() << "preparing DELETE SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing delete query: err" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
        return false;
    }
    return true;
}

QSqlQuery SessionDetail::findLogCallSign(SessionDtlRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML =  "SELECT ";
    strDML += "hdrndx, dtlseq, stationtype, timebase, status, traffic, ares, satern, mars, ";
    strDML += "timestamp, callsign, firstname, city, st, gridsquare, notes ";
    strDML += "FROM sessionlogdtl ";
    strDML += "WHERE callsign = '";
    strDML += rec.callsign;
    strDML += "' AND hdrndx=";
    strDML += QString("%1").arg(rec.hdrndx);
    strDML += " ORDER BY timestamp, dtlseq ASC";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: err" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
        return qry;
    }
    return qry;
}

bool SessionDetail::findLogRecord(SessionDtlRecord)
{
    //  this may not be necessary
    return false;
}

QString  SessionDetail::findLastLogged(SessionDtlRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML = "SELECT max(rectime), callsign FROM sessionlogdtl ";
    strDML += "WHERE callsign = '";
    strDML += rec.callsign + "' ";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: "<< strDML
                 << "err" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
        return "";
    }
    qry.first();
    QString r = qry.value(0).toString();
    //qDebug() << "Found " << r;
    QString rr = r.left(10);
    QString y = rr.left(4);
    QString m = rr.mid(5,2);
    QString d = rr.right(2);
    rr = m + "/" + d + "/" + y;
    //qDebug() << "Returning " << rr;
    return rr;
}
