#include "dlgimportstats.hpp"

class dlgImportStats::statPrivateData
{
public:
    quint16 imported;
    quint16 added;
    quint16 updated;
};

dlgImportStats::dlgImportStats(QWidget *parent) : QDialog(parent)
{
    stat_data = new statPrivateData;

    ui = new Ui::dlgImportStats;
    ui->setupUi(this);
    //
    ui->retranslateUi(this);
    stat_data->added = 0;
    stat_data->imported = 0;
    stat_data->updated = 0;

}

dlgImportStats::~dlgImportStats()
{
    delete ui;
    delete stat_data;
}

void dlgImportStats::slotImportAdd()
{
    ++stat_data->added;
    ui->txtDBAddedCnt->setText(QString("%1").arg(stat_data->added));
}

void dlgImportStats::slotImported()
{
    ++stat_data->imported;
    ui->txtImportCnt->setText(QString("%1").arg(stat_data->imported));
}

void dlgImportStats::slotImportUpdt()
{
    ++stat_data->updated;
    ui->txtDBUpdtCnt->setText(QString("%1").arg(stat_data->updated));
}
