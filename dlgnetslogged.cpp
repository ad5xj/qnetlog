#include <QtCore/QDebug>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlRelationalTableModel>

#include "dlgnetslogged.hpp"

class dlgNetsLogged::NetsMaintPrivateData
{
public:

    QSqlRelationalTableModel *model;
    SessionHeader log;  // log headers of all nets logged
    Nets          net;  // definition file of nets
};

dlgNetsLogged::dlgNetsLogged(QWidget *parent) : QDialog(parent), ui(new Ui::dlgNetsLogged)
{
    nm_data = new NetsMaintPrivateData;
    p_netname = 0;

    ui = new Ui::dlgNetsLogged;
    ui->setupUi(this);

}

dlgNetsLogged::~dlgNetsLogged()
{
    if ( nm_data->model ) delete nm_data->model;
    delete ui;
    delete nm_data;
}

void dlgNetsLogged::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void dlgNetsLogged::showEvent()
{
    // if p_netname property has not been set do nothing
    if ( p_netname <= 0 ) return;
    if ( p_datetime == "" ) return;
    // property is set so use it to find nets logged
    slotFindIt();
}

void dlgNetsLogged::slotFindIt()
{
    nm_data->model = new QSqlRelationalTableModel(this);
    nm_data->model->setTable("logfile");
    nm_data->model->setRelation(2, QSqlRelation("nets", "id", "netname"));
    QString strDML;
    strDML = "SELECT DISTINCT ";
    strDML += "netlogheader.netid, ";
    strDML += "netlogheader.netname,";
    strDML += "netlogheader.sessionst, ";
    strDML += "netlogheader.sessionfreq, ";
    strDML += "netlogheader.sessiondays, ";
    strDML += "netlogheader.netmode, ";
    strDML += "netlogheader.netfreq ";
    strDML += "FROM netlogheader ";
    strDML += "LEFT JOIN nets ON netlogheader.netid = nets.id ";
    strDML += "WHERE ";
    strDML += "netlogheader.netname=" + QString("%1").arg(p_netname);
    strDML += " AND netsttime >= '" + p_datetime;
    strDML += "'; ";
    nm_data->model->setFilter(strDML);
    nm_data->model->select();
    loadNetLog();
}

void dlgNetsLogged::loadNetLog()
{
    ui->tblNetsLogged->clearContents();

    if ( nm_data->model->record().count() <= 0 )
    {
        qDebug() << "No valid query for log records";
        return;
    }

    int r, q;
    QString s1 = "";
    QString s2 = "";
    QString s3 = "";
    QString s4 = "";

    r = 0;
    q = nm_data->model->record().count();
    nm_data->model->index(0,0);
    // store first item in table
    ui->tblNetsLogged->insertRow(0);  // one more than header
    ui->tblNetsLogged->setRowHeight(0,28);
    // fill in blanks in new row
    QTableWidgetItem *timeItem = new QTableWidgetItem(nm_data->model->index(0,0).data(Qt::DisplayRole).toString(),0);
    QTableWidgetItem *nameItem = new QTableWidgetItem(nm_data->model->index(0,1).data(Qt::DisplayRole).toString(),0);

    ui->tblNetsLogged->setItem(r,0,timeItem);
    ui->tblNetsLogged->setItem(r,1,nameItem);
    // now if more than one -  load all left
    s1 = "";
    s2 = "";
    s3 = "";
    s4 = "";
    while (r < q)
    {
        if ( (r >= q) ) break; // ?EOF? //
        // resize table
        r++;
        ui->tblNetsLogged->setRowHeight(r,28);
        // fill in blanks in new row
        QTableWidgetItem *timeItem = new QTableWidgetItem(nm_data->model->index(0,0).data(Qt::DisplayRole).toString(),0);
        QTableWidgetItem *nameItem = new QTableWidgetItem(nm_data->model->index(0,1).data(Qt::DisplayRole).toString(),0);

        ui->tblNetsLogged->setItem(r,0,timeItem);
        ui->tblNetsLogged->setItem(r,1,nameItem);
        if (r >= q) break;
    }
    ui->tblNetsLogged->setCornerButtonEnabled(false);
    ui->tblNetsLogged->repaint();
    nm_data->model->clear();
}
