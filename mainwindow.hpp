#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP
/*! \file mainwindow.hpp  */
#include <QtCore/QTimer>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <QtCore/QEvent>
#include <QtCore/QStringListModel>
#include <QtCore/QItemSelectionModel>
#include <QtGui/QStandardItemModel>
#include <QtGui/QMoveEvent>
#include <QtGui/QCloseEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QDataWidgetMapper>
#include <QtWidgets/QTableWidgetItem>

class QSqlQuery;
class QSqlRecord;
class QSqlQueryModel;

#include "globals.hpp"
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

/*! \ingroup QNETLOG
 * \class MainWindow
 * \details
 * This is the main dialog window for the application.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotCurTimerTimeout() -  connected to tmrCurTime timeout event <br />
 * <span style="color: green;">void</span>:slotLogIt()           -  connected to ui->btnLogit click event <br />
 * <span style="color: green;">void</span>:slotStNet()           -  connected to ui->btnStNet click event <br />
 * <span style="color: green;">void</span>:slotEndNet()          -  connected to ui->btnEndNet click event <br />
 * <span style="color: green;">void</span>:slotAbortNet()        -  connected to ui->btnAbort click event <br />
 * <span style="color: green;">void</span>:slotReset()           -  connected to ui->btnReset click event <br />
 * <span style="color: green;">void</span>:slotCallChgd(QString txt) - connected to ui->editCall text chg event <br />
 * <span style="color: green;">void</span>:slotCallSelected()    -  connected to ui->CallList sel event <br />
 * <span style="color: green;">void</span>:slotNetChgd()         -  connected to ui->cboNets activated event <br />
 * <span style="color: green;">void</span>:slotSelMARS(bool ck)  -  connected to ui->chkMARS change event <br />
 * <span style="color: green;">void</span>:slotTimeBaseChgd()    -  connected to ui->btnUTC change event <br />
 * <span style="color: green;">void</span>:slotSelLogEntry()     -  connected to ui->tblLog row sel event <br />
 * <span style="color: green;">void</span>:slotEditLogEntry()    -  connected to ui->btnEditLog click event <br />
 * <span style="color: green;">void</span>:slotLogEditCancel()   -  connected to ui->btnCancel click signal <br />
 * <span style="color: green;">void</span>:slotDlgNets()         -  connected to ui->actionNetsMaint activated signal <br />
 * <span style="color: green;">void</span>:slotDlgCalls()        -  connected to ui->actionCallsMaint activated signal <br />
 * <span style="color: green;">void</span>:slotDlgLogFile()      -  connected to ui->actionLogMaint activated signal <br />
 * <span style="color: green;">void</span>:slotDlgBandsMaint()   -  connected to ui->actionBandsMaint triggered signal <br />
 * <span style="color: green;">void</span>:slotAbout()           -  connected to ui->actionAbout triggered signal <br />
 * <span style="color: green;">void</span>:slotImportADIF()      -  connected to ui->actionImportCallsigns triggered signal <br />
 * <span style="color: green;">void</span>:slotSessionLog()      -  connected to ui->actionSessionLog triggered signal<br /><br />
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
    * \brief Application Main Window
    * This window provides the lanch of the application and connection to the
    * database object.
    *
    * \sa dlgNetsMaint
    * \sa dlgCallSigns
    * \sa dlgImportOpts
    * \sa CallSigns
    * \sa NetsRecord
    * \sa globals.hpp
    * \sa ADIF
    * \sa DATABASE                             */
    explicit MainWindow();
    virtual ~MainWindow();

protected:
    void resizeEvent(QResizeEvent *);    //!<
    void moveEvent(QMoveEvent *);        //!<
    void changeEvent(QEvent *e);         //!<
    void showEvent(QShowEvent *);        //!<
    void closeEvent(QCloseEvent *);      //!<

private Q_SLOTS:
    void slotCurTimerTimeout();          //!< connected to tmrCurTime timeout event
    void slotLogIt();                    //!< connected to ui->btnLogit click event
    void slotStNet();                    //!< connected to ui->btnStNet click event
    void slotEndNet();                   //!< connected to ui->btnEndNet click event
    void slotAbortNet();                 //!< connected to ui->btnAbort click event
    void slotReset();                    //!< connected to ui->btnReset click event
    void slotCallChgd(QString txt);      //!< connected to ui->editCall text chg event
    void slotCallSelected();             //!< connected to ui->CallList sel event
    void slotNetChgd();                  //!< connected to ui->cboNets activated event
    void slotSelMARS(bool ck);           //!< connected to ui->chkMARS change event
    void slotTimeBaseChgd();             //!< connected to ui->btnUTC change event
    void slotSelLogEntry();              //!< connected to ui->tblLog row sel event
    void slotEditLogEntry();             //!< connected to ui->btnEditLog click event
    void slotLogEditCancel();            //!< connected to ui->btnCancel click signal
    void slotDlgNets();                  //!< connected to ui->actionNetsMaint activated signal
    void slotDlgCalls();                 //!< connected to ui->actionCallsMaint activated signal
    void slotDlgLogFile();               //!< connected to ui->actionLogMaint activated signal
    void slotDlgBandsMaint();            //!< connected to ui->actionBandsMaint triggered signal
    void slotAbout();                    //!< connected to ui->actionAbout triggered signal
    void slotImportADIF();               //!< connected to ui->actionImportCallsigns triggered signal
    void slotSessionLog();               //!< connected to ui->actionSessionLog triggered signal

private:
    /*! \class mwPrivateData
     * \brief Local Private Data container  */
    class mwPrivateData;
    mwPrivateData *mw_data;              //!< local *d-ptr to local private data

    Ui::MainWindow *ui;                  //!< local *d-ptr to form definition

    void createObjects();                //!<
    void connectSlots();                 //!<
    void createNetHdr();                 //!<
    void removeSessionHdr();             //!<
    void resetSession();                 //!<
    void updtSessionHdr();               //!<
    void saveCall();                     //!<
    void saveSessionDtl();               //!<
    void loadNetsCombo();                //!<
    void loadRefLists();                 //!<
    void calcElapsedTime();              //!<
    void fontPrefs();                    //!<
    void colorPrefs();                   //!<
    void clearForm();                    //!<
    void resize_log_columns(QString cols); //!<
    void displaySettings();               //!<
    void readSettings();                  //!<
    void writeSettings();                 //!<
    void loadNetLog();                    //!<
    void ImportADIF();                    //!<
    void testDB();                        //!<
};
#endif // MAINWINDOW_H
