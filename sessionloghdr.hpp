#ifndef SESSIONLOGHDR_HPP
#define SESSIONLOGHDR_HPP
/*! \file sessionloghdr.hpp   */
#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>

#include "globals.hpp"

class QSqlQuery;

/*! \ingroup QNETLOG
 * \class SessionHeader
 * \details
 * Constructs a database interface to the SessionHeader table.
 * Implementation is the same no matter what database server
 * is used.
 *
 * Database access is provided by public methods:<br>
 * <span style="color: green;">bool</span>:save(LocTypeRec rec)<br />
 * <span style="color: green;">bool</span>:del(LocTypeRec rec)<br />
 * <span style="color: green;">bool</span>:update(LocTypeRec rec)<br />
 * <span style="color: green;">QSqlQuery</span>:findBands()<br />
 * <span style="color: green;">QSqlQuery</span>:findBandsID(int id) and<br />
 * <span style="color: green;">QSqlQuery</span>:findBandsRecord(int id)<br /><br /> */
class SessionHeader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)        //!<
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
    * \brief
     * <span style="color: green;">QObject::Bands(QWidget * parent = 0)</span><br><br>
     * &nbsp;&nbsp;The implementation of the interface to the Bands table in the
     * databaseprovides a wrapper around the SQL functions needed to access and
     * maintainrecords in the bands table of the database.
     *
     * The wrapper class for the sessionloghdr table of the database.
     * It performs all database functions related to the log header
     * record for each session. The ID field acts as a foreign key in
     * the sessionlogdtl file. One header record can have many detail
     * records.
     * \param QObject *parent                     */
    explicit SessionHeader(QObject *parent=0);
    virtual ~SessionHeader();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(SessionHdrRecord rec)</span><br /><br />
     * SQL function to save SessionHeader record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(SessionHdrRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(SessionHdrRecord rec)</span><br /><br />
     * SQL function to delete a specific SessionHeader record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(SessionHdrRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(SessionHdrRecord rec)</span><br /><br />
     * SQL function to update a specific SessionHeader record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(SessionHdrRecord rec);

    /*!
     * &nbsp;&nbsp;int <span style="color: green;">findLastHeader()</span><br /><br />
     * SQL query returning id of last record added to SessionHeader. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    int findLastHeader();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findNets(SessionHdrRec rec)</span><br /><br />
     * SQL query returning all rows in the table where sessionloghdr.id=rec.id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findNets(int id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findHdrRecord(SessionHdrRecord rec)</span><br /><br />
     * SQL query returning all rows with id=rec.id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findHdrRecord(SessionHdrRecord rec);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findHdrRecord(SessionHdrRecord rec)</span><br /><br />
     * SQL query returning all rows with sessionloghdr.id=rec.idx. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findNetHdrs(int idx);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;
};
#endif // SESSIONLOGHDR_HPP
