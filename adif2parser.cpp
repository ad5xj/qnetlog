/*************************************************************************
 *  Impementation of the ADIF 2 Specific XML Parser                      *
 *  interface adifreader.hpp                                             *
 *               -------------------                                     *
 *  begin                : MAY 2014                                      *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 *  GNU General Public License for more details.                         *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QHash>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>

#include "globals.hpp"
#include "callsigns.hpp"
#include "adif2parser.hpp"

class ADIF2Parser::adifPrivateData
{
public:
    bool useMyDBCall;
    bool useMyDBName;
    bool useMyDBCity;
    bool useMyDBSt;
    bool useMyDBGrid;
};

ADIF2Parser::ADIF2Parser(QWidget *parent) : QObject(parent)
{
    adif_data = new adifPrivateData;
    //
}

ADIF2Parser::~ADIF2Parser() { delete adif_data; }

void ADIF2Parser::processADIF2Import(QString fil, QString opts)
{
    QHash<QString,quint8> tokenType;
    QHash<QString,quint8> tagType;
    tokenType.insert("<",1);
    tokenType.insert(">",2);
    tokenType.insert(":",3);
    tagType.insert("eoh",1);
    tagType.insert("EOH",2);
    tagType.insert("eor",3);
    tagType.insert("EOR",4);
    tagType.insert("eof",5);
    tagType.insert("EOF",6);

    bool b_startfield = false;  // true = parsing field name false = parsing data
    bool b_startfldlen = false; // true = parsing len of data str after ":" and before ">"

    quint8  t = 0;          //

    quint16 rownum = 0;     // for-next iterator
    quint16 rowcnt = 0;     // local record count
    quint16 recpos = 0;     // char pos in each row
    quint16 hdrpos = 0;     // utility var to iterate for next
    quint16 st = 0;         // utility var to set start iterator value
    quint16 eohmrk = 0;     // utility var to hold end of header row number
    quint16 varlen = 0;     // len of data value from fieldname
    quint16 crow = 0;       // current row number
    quint16 recseq = 0;     // first field in struct
    quint16 reclen = 0;       // utility var
    quint16 lft = 0;        // utility var
    quint16 curpos = 0;

    QString fieldname = ""; //
    QString fielddata = "";  //
    QString ch = "";        //
    QString adifrec = "";        //
    QString strlen = "";    // length of data string after ":" and before ">"
    QString data = "";      //
    QString header = "";    //

    QStringList rows;       //  string list of adif data split on CRLF

    Fieldnames nam;              // alias for the struct

    Q_UNUSED(varlen)        // to get rid of warnings

    // get the file to parse
    QFile xfile(fil);
    if (!xfile.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox mb;
        mb.setWindowTitle(QApplication::tr("QNetLogger 1073"));
        mb.setText(QApplication::tr("Cannot read file %1:\n%2.").arg(fil).arg(xfile.errorString()));
        mb.exec();
        return;
    }

    // Read all ADIF data into string list for each row ending in CRLF //
    QString msg = tr("Reading...");
    emit signalUpdateStatusMsg(msg);
    do
    {
        data = xfile.readAll();
        rows = data.split("\n",QString::KeepEmptyParts);
    } while ( !xfile.atEnd() );
    xfile.close();

    // read the first few lines to get the version and application that generated it
    // PROGRAMID
    // PROGRAMVERSION
    // <adif_ver:4>1.00 optional version statement only in header
    // EOH  ends the header portion if included (it is optional)
    //
    // TODO: decide whether to save header detail or display it
    //
    rowcnt = rows.count();
    msg = tr("Parsing...");
    emit signalUpdateStatusMsg(msg);
    qDebug() << QString("Parsing %1 rows").arg(rowcnt);

    // Go through each row from the input file to
    // iterate through the header data,
    // if included in adif format (it usually is)
    for ( hdrpos = 0; hdrpos < rowcnt; ++hdrpos )
    {  // for each string in the list of rows - split by CRLF
        if ( rows[hdrpos].contains("<eoh>") )
        {
            eohmrk = rows[hdrpos].lastIndexOf("eoh");
            break;
        }
        else if ( rows[hdrpos].contains("<EOH>") )
        {
            eohmrk = rows[hdrpos].lastIndexOf("EOH");
            break;
        }
        else if ( rows[hdrpos].contains("<Eoh>") )
        {
            eohmrk = rows[hdrpos].lastIndexOf("eoh");
            break;
        }
        else
        {
            header += rows[hdrpos];
        }
    }
    // The position is now at the end of header information rows

    // PARSE DATA ROWS
    recpos = eohmrk + 4; // position past "eoh" tag if supplied
    crow = hdrpos;       // store current row
    curpos = 0;          // initialize vars to use for the parse
    reclen = 0;
    lft = 0;
    st = 0;
    t = 0;
    fieldname = "";
    fielddata = "";
    adifrec = "";
    b_startfield = false;
    b_startfldlen = false;

    // parse each row of ADIF content
    // from the current row to the end of all rows
    for ( rownum = crow; rownum < rowcnt; ++rownum  )
    {
        if ( rownum > rowcnt ) break;

        adifrec = rows[rownum];  // separate current row of data
        reclen = adifrec.length();    // get the length of row data
        if ( recpos > reclen ) recpos = 0;
        lft  = reclen - recpos;  // diff between current char pos to end of row data
        if ( lft == 0 || recpos == reclen )
        {  // left position is zero so go to first char of next record
            recpos = 0;
            continue;
        }
        st = recpos; // starting position in data
        // from start of data in row to the end of data on the row
        // get the tokens, tags, and data
        for ( curpos = st; curpos < reclen; ++curpos )
        {
            ch = adifrec[curpos]; //get the current char

            // look at the character for field token designators
            //   convert to int for switch cases
            t = 10; // set value out of range
            if ( tokenType.contains(ch) )  t = tokenType.value(ch);

            // is it in the hash table?
            switch (t)
            {
            case 1:  // < - start token for beginning a tag
            {
                if ( fieldname.trimmed().length() > 0 )
                {
                    // we have a field name to store
                    //   time to save the field name and data
                    //   fill the struct with data
                    nam.seq  = recseq;
                    nam.name = fieldname;
                    nam.value = fielddata;
                    // insert into list for record
                    recfields.append(nam);
                }
                // save the position in the row
                recpos = curpos;
                b_startfield = true;    // starting field tag detection
                b_startfldlen = false;  // not time for data len yet
                nam.name = "";
                nam.value = "";
                fieldname = "";
                fielddata = "";
                continue;               // break out of this and do next loop
            }

            case 2:  // > - ending token for field tag
            {
                // make sure data len value is saved
                if ( strlen != "" )
                {
                    varlen = strlen.toInt();
                    strlen = "";
                }

                // is fieldname tag in hash list
                quint8 u = tagType.value(fieldname);
                switch (u)
                {
                case 1:  // eoh tag found
                {
                    recpos = curpos;
                    nam.name = "";
                    nam.value = "";
                    fieldname = "";
                    fielddata = "";
                    b_startfield = false;
                    b_startfldlen = false;
                    continue;
                }

                case 2:  // EOH upper case tag found
                {
                    recpos = curpos;
                    nam.name = "";
                    nam.value = "";
                    fieldname = "";
                    fielddata = "";
                    b_startfield = false;
                    b_startfldlen = false;
                    continue;
                }

                case 3:  // eor tag found - have we reached the end of a ADIF data row
                {
                    if ( fieldname.trimmed().length() > 0 )
                    {
                        // we have a field name to store
                        //   time to save the field name and data
                        //   fill the struct with data
                        nam.seq  = recseq;
                        nam.name = fieldname;
                        nam.value = fielddata;
                        // insert into list for record
                        recfields.append(nam);
                    }
                    ++recseq;
                    // save the record with all fields to the DB
                    saveRecord(recfields, opts);
                    // clear out the old data and set up for new data
                    nam.name = "";
                    nam.value = "";
                    fieldname = "";
                    fielddata = "";
                    b_startfield = false;
                    b_startfldlen = false;
                    recfields.clear();
                    continue;
                }

                case 4:  // EOR tag found - have we reached the end of a ADIF data row
                {
                    if ( fieldname.trimmed().length() > 0 )
                    {
                        if ( fieldname.trimmed().toLower() != "eor" )
                        {
                            // we have a field name to store
                            //   time to save the field name and data
                            //   fill the struct with data
                            nam.seq  = recseq;
                            nam.name = fieldname;
                            nam.value = fielddata;
                            // insert into list for record
                            recfields.append(nam);
                        }
                    }
                    ++recseq;
                    // save the record with all fields to the DB
                    saveRecord(recfields, opts);
                    // clear out the old data and set up for new data
                    nam.name = "";
                    nam.value = "";
                    fieldname = "";
                    fielddata = "";
                    b_startfield = false;
                    b_startfldlen = false;
                    recfields.clear();
                    continue;
                }
                // end of eor test

                default:
                {
                    // end of field token encountered but
                    // tag name not in list
                    // so save tag name in field name value
                    nam.name = fieldname;
                    b_startfield = false;
                    b_startfldlen = false;
                    continue;
                }
                }
                // end of tag name test
            }

            case 3:
            {
                // ":" token encountered before ">"
                b_startfldlen = true;
                continue;
            }

            default:
            {
                if ( b_startfield  )
                {
                    // end of tag token has not been reached
                    // read for fieldname not data
                    recpos = curpos;
                    // do we start data len string in fieldname tag
                    if ( !b_startfldlen )
                    {
                        // no still field name tag so append char
                        fieldname += ch; // append to fieldname
                    }
                    else
                    {
                        // yes if this is the data len token skip it
                        // otherwise start the data len string
                        if ( ch != ":" ) strlen += ch;  // append to varlen string
                        varlen = strlen.toInt();        // make string an int
                    }
                }
                else
                {
                    // not in fieldname - this is data
                    // not expecting "<" yet
                    fielddata += ch;
                    recpos = curpos;
                }
                continue;
            }
            }
            // end of token test
        }
        // end of for parsing fields in row
    } // end of all rows
    msg = tr("Importing...Done");
    emit signalUpdateStatusMsg(msg);
    emit signalDone();
}

void ADIF2Parser::saveRecord(QList<Fieldnames> rec, QString o)
{
    static bool    useMyDBCall;
    static bool    useMyDBName;
    static bool    useMyDBCity;
    static bool    useMyDBSt;
    static bool    useMyDBGrid;
    static bool    optionsSet = false;
    static quint16 recont = 0;
    static QList<QString> optlist;

    bool found;
//    bool callexists;

    CallSignsRecord datarec;
    CallSigns *calls;
    Fieldnames nam;
    quint16 j = 0;
    quint16 id = 0;
//    quint16 rownum = 0;
    quint16 bot = 0;
    QString msg = "";

    // parse options from import form if they have not been collected yet
    if ( !optionsSet )
    {
        optlist = o.split("|",QString::KeepEmptyParts);
        useMyDBCall  = optlist[0].toInt(0,10);
        useMyDBName  = optlist[1].toInt(0,10);
        useMyDBCity  = optlist[2].toInt(0,10);
        useMyDBSt    = optlist[3].toInt(0,10);
        useMyDBGrid  = optlist[4].toInt(0,10);
        optionsSet = true;
    }

    bot = rec.count();
    if ( bot <= 0 ) return;
    calls = new CallSigns;

    //qDebug() <<  QString("recfields count: %1").arg(bot);

    msg = tr("Importing...") + QString("%1").arg(recont);
    emit signalUpdateStatusMsg(msg);
    QApplication::processEvents();

    // now put record data into rec struct for storage
    QString callsign = "";
    datarec.ID = 0;
    datarec.Call = "";
    datarec.Name = "";
    datarec.City = "";
    datarec.St = "";
    datarec.Grid = "";
    nam = rec[0];
    ++recont;
    do
    {
        // loop through all the field names then store the record
        if ( nam.name.length() <= 0  )
        {
            // if there is no tag name then skip it  - invalid
            ++j;
            nam = rec[j];
            continue;
        }
        if ( nam.name.toLower().contains("id") ) id = nam.value.toInt();

        if ( nam.name.toLower().contains("call") ) callsign = nam.value.toUpper();
        if ( callsign == "" )
        {
            if ( bot > 1 )
            {
                ++j;
                if ( j < bot )
                    nam = rec[j];
                continue;
            }
        }

        if ( j < bot )
        {
            // test for pre-existing record in the database
            found = false;
            if ( callsign != "" ) found = calls->findACallSign(callsign);
            if ( found && useMyDBCall )
            {
                // the callsign is in the database and we will use it
                datarec.ID = id;
                datarec.Call = callsign;
                if ( callsign == "" ) continue;
                QSqlQuery qry = calls->findID(id);
                qry.first();
                datarec.Name = qry.record().field("operator").value().toString();
                datarec.City = qry.record().field("city").value().toString();
                datarec.St   = qry.record().field("st").value().toString();
                datarec.Grid = qry.record().field("grid").value().toString();
                if ( nam.name.toLower().contains("name") && !useMyDBName )
                    datarec.Name = nam.value;
                if ( nam.name.toLower().contains("qth")  && !useMyDBCity )
                    datarec.City = nam.value;
                if ( nam.name.toLower().contains("state") && !useMyDBSt )
                    datarec.St = nam .value;
                if ( nam.name.toLower().contains("gridsquare") && !useMyDBGrid)
                    datarec.Grid = nam .value;
                continue;
            }
            else
            {
                if ( useMyDBCall )
                {
                    // by option the callsign is not in the database and we will use it
                    datarec.ID = 0;
                    datarec.Call = callsign;
                    if ( nam.name.toLower().contains("name") )  datarec.Name = nam.value;
                    if ( nam.name.toLower().contains("qth") )   datarec.City = nam.value;
                    if ( nam.name.toLower().contains("state") ) datarec.St = nam.value;
                    if ( nam.name.toLower().contains("gridsquare") ) datarec.Grid = nam.value;
                }
            }
            // not sure what to do if it is not in the db and option !useMYDBCall
        }
        ++j;
        if ( j >= bot ) break;
        nam = rec[j];
    } while ( j < rec.count() );
    emit signalImported();
    if ( datarec.ID > 0 )
    {
        calls->update(datarec);
        emit signalImportUpdt();
    }
    else
    {
        if ( datarec.Call != "" ) calls->save(datarec);
        emit signalImportAdd();
    }
    ++recont;
    datarec.ID = 0;
    datarec.Call = "";
    datarec.Name = "";
    datarec.City = "";
    datarec.St = "";
    datarec.Grid = "";
    // record saved so return
}


    /* these fields have not been implemented in the adif input
    if ( nam.name.toLower().contains("age") ) continue;
    if ( nam.name.toLower().contains("adif_ver") ) continue;
    if ( nam.name.toLower().contains("a_index") ) continue;
    if ( nam.name.toLower().contains("ant_az") ) continue;
    if ( nam.name.toLower().contains("ant_el") ) continue;
    if ( nam.name.toLower().contains("ant_path") ) continue;
    if ( nam.name.toLower().contains("band_rx") ) continue;
    if ( nam.name.toLower().contains("freq_rx") ) continue;
    if ( nam.name.toLower().contains("check") ) continue;
    if ( nam.name.toLower().contains("class") ) continue;
    if ( nam.name.toLower().contains("contacted_op") ) continue;
    if ( nam.name.toLower().contains("credit_submitted") ) continue;
    if ( nam.name.toLower().contains("credit_granted") ) continue;
    if ( nam.name.toLower().contains("distance") ) continue;
    if ( nam.name.toLower().contains("email") ) continue;
    if ( nam.name.toLower().contains("eqsl_rcvd") ) continue;
    if ( nam.name.toLower().contains("eqsl_sent") ) continue;
    if ( nam.name.toLower().contains("force_init") ) continue;
    if ( nam.name.toLower().contains("guest_op") ) continue;
    if ( nam.name.toLower().contains("iota_island_id") ) continue;
    if ( nam.name.toLower().contains("k_index") ) continue;
    if ( nam.name.toLower().contains("lat") ) continue;
    if ( nam.name.toLower().contains("lon") ) continue;
    if ( nam.name.toLower().contains("lotw_qsl_rcvd") ) continue;
    if ( nam.name.toLower().contains("lotw_qsl_sent") ) continue;
    if ( nam.name.toLower().contains("max_burst") ) continue;
    if ( nam.name.toLower().contains("ms_shower") ) continue;
    if ( nam.name.toLower().contains("my_city") ) continue;
    if ( nam.name.toLower().contains("my_cnty") ) continue;
    if ( nam.name.toLower().contains("my_country") ) continue;
    if ( nam.name.toLower().contains("my_cq_zone") ) continue;
    if ( nam.name.toLower().contains("my_gridsquare") ) continue;
    if ( nam.name.toLower().contains("my_iota") ) continue;
    if ( nam.name.toLower().contains("my_iota_island_id") ) continue;
    if ( nam.name.toLower().contains("my_itu_zone") ) continue;
    if ( nam.name.toLower().contains("my_lat") ) continue;
    if ( nam.name.toLower().contains("my_lon") ) continue;
    if ( nam.name.toLower().contains("my_name") ) continue;
    if ( nam.name.toLower().contains("my_postal_code") ) continue;
    if ( nam.name.toLower().contains("my_rig") ) continue;
    if ( nam.name.toLower().contains("my_sig") ) continue;
    if ( nam.name.toLower().contains("my_sig_info") ) continue;
    if ( nam.name.toLower().contains("my_state") ) continue;
    if ( nam.name.toLower().contains("my_street") ) continue;
    if ( nam.name.toLower().contains("nr_bursts") ) continue;
    if ( nam.name.toLower().contains("nr_pings") ) continue;
    if ( nam.name.toLower().contains("owner") ) continue;
    if ( nam.name.toLower().contains("precedence") ) continue;
    if ( nam.name.toLower().contains("programid") ) continue;
    if ( nam.name.toLower().contains("programversion") ) continue;
    if ( nam.name.toLower().contains("prop_mode") ) continue;
    if ( nam.name.toLower().contains("public_key") ) continue;
    if ( nam.name.toLower().contains("qslrdate") ) continue;
    if ( nam.name.toLower().contains("qslsdate") ) continue;
    if ( nam.name.toLower().contains("qslrcvd") ) continue;
    if ( nam.name.toLower().contains("qsl_rvcd_via") ) continue;
    if ( nam.name.toLower().contains("qslsent") ) continue;
    if ( nam.name.toLower().contains("qsl_sent_via") ) continue;
    if ( nam.name.toLower().contains("qso_complete") ) continue;
    if ( nam.name.toLower().contains("qso_date_off") ) continue;
    if ( nam.name.toLower().contains("qso_random") ) continue;
    if ( nam.name.toLower().contains("qth") ) continue;
    if ( nam.name.toLower().contains("rig") ) continue;
    if ( nam.name.toLower().contains("rx_pwr") ) continue;
    if ( nam.name.toLower().contains("sat_mode") ) continue;
    if ( nam.name.toLower().contains("sat_name") ) continue;
    if ( nam.name.toLower().contains("sfi") ) continue;
    if ( nam.name.toLower().contains("sig") ) continue;
    if ( nam.name.toLower().contains("sig_info") ) continue;
    if ( nam.name.toLower().contains("srx") ) continue;
    if ( nam.name.toLower().contains("srx_string") ) continue;
    if ( nam.name.toLower().contains("station_callsign") ) continue;
    if ( nam.name.toLower().contains("stx") ) continue;
    if ( nam.name.toLower().contains("stx_string") ) continue;
    if ( nam.name.toLower().contains("swl") ) continue;
    if ( nam.name.toLower().contains("ve_prov") ) continue;
    if ( nam.name.toLower().contains("web") ) continue;
    */
