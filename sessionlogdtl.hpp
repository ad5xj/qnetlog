#ifndef SESSIONLOGDTL_HPP
#define SESSIONLOGDTL_HPP
/*! \file sessionlogdtl.hpp   */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>

#include "globals.hpp"

/*! \ingroup QNETLOG
 * \details
 * This is the database interface to the sessionlogdtl table.
 * Implementation is the same no matter what database server
 * is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">bool</span>:save(SessionDtlRecord rec)<br />
 * <span style="color: green;">bool</span>:del(SessionDtlRecord rec)<br />
 * <span style="color: green;">bool</span>:update(SessionDtlRecord rec)<br />
 * <span style="color: green;">bool</span>:findLogRecord(SessionDtlRecord rec)<br />
 * <span style="color: green;">QString</span>:findLastLogged(SessionDtlRecord rec) and<br />
 * <span style="color: green;">QSqlQueryModel</span>:getLogEntry(int id) and<br />
 * <span style="color: green;">QSqlQuery</span>:getCurSession(int ndx) and<br />
 * <span style="color: green;">QSqlQuery</span>:getNetLog(int ndx) and<br />
 * <span style="color: green;">QSqlQuery</span>:findLogCallSign(SessionDtlRecord rec)<br /><br /> */
class SessionDetail : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)        //!<
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
    * \brief The SessionDetail class
    * This class is a wrapper for the sessionlogdtl class in the database.
    * It is used to perform all necessary database functions for
    * the session detail records of the log. Contains a foreign key
    * to the sessionloghdr table in a many to 1 relationship.    */
    explicit SessionDetail(QObject *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;SessionDetail::<span style="color: green;">~SessionDetail(QWidget * parent = 0)</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~SessionDetail();

    quint32 getLastErr()    { return err; }        //!< property return definition
    QString getLastErrMsg() { return errmsg; }     //!< property return definition

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(SessionDtlRecord rec)</span><br /><br />
     * SQL function to save sessionlogdtl record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(SessionDtlRecord);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(SessionDtlRecord rec)</span><br /><br />
     * SQL function to delete a specific sessionlogdtl record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(SessionDtlRecord);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(SessionDtlRecord rec)</span><br /><br />
     * SQL function to update a specific sessionlogdtl record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(SessionDtlRecord);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">findLogRecord(SessionDtlRecord rec)</span><br /><br />
     * SQL query validating record exists in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    bool findLogRecord(SessionDtlRecord);

    /*!
     * &nbsp;&nbsp;QString <span style="color: green;">findLastLogged(SessionDtlRecord rec)</span><br /><br />
     * SQL query returning all rows in the table where timestamp is the last logged.
     * This is an interface function to perform SQL database functions regardless of
     * what type of database is used.
     * \return QString Returns a QString date result to the caller     */
    QString   findLastLogged(SessionDtlRecord);

    /*!
     * &nbsp;&nbsp;QSqlQueryModel* <span style="color: green;">getCurSession(int ndx)</span><br /><br />
     * SQL query returning pointer to the model of records in the table that have the rec.id=ndx. This
     * is an interface function to perform SQL database functions regardless of what
     * type of database is used.
     * \return QSqlQuery Returns a pointer to a QSqlQueryModel to the caller     */
    QSqlQueryModel* getCurSession(int ndx);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">getLogEntry(int id)</span><br /><br />
     * SQL query returning query result of records in the table that have the rec.id=id. This
     * is an interface function to perform SQL database functions regardless of what
     * type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery getLogEntry(int id); // get a specific record with id

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">getNetLog(int ndx)</span><br /><br />
     * SQL query returning query result of records in the table that have the rec.id=ndx. This
     * is an interface function to perform SQL database functions regardless of what
     * type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery getNetLog(int ndx);  // get all records for current net using Net header index

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">getLogEntry(SessionDtlRecord rec)</span><br /><br />
     * SQL query returning query result of records in the table that have the sessionlogdtl.callsign=rec.callsign. This
     * is an interface function to perform SQL database functions regardless of what
     * type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findLogCallSign(SessionDtlRecord);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;
};
#endif // SESSIONLOGDTL_HPP
