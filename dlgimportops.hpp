#ifndef DLGIMPORTOPS_HPP
#define DLGIMPORTOPS_HPP
/*! \file dlgimportops.hpp  */

#include "ui_dlgimportoptions.h"

namespace Ui {
class dlgImportOpts;
}

/*! \ingroup QNETLOG
 * \class dlgImportOps
 * \details
 * This dialog provides import options for the ADIF import of external data to the database.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotImportADIF()<br />
 * <span style="color: green;">void</span>:slotDone()<br />
 * <span style="color: green;">void</span>:slotUpdateStatusMsg() */
class dlgImportOpts : public QDialog
{
    Q_OBJECT

public:
    /*!
    * \brief ADIF file import options dialog
    * \param QWidget parent
    * \sa ADIF                           */
    explicit dlgImportOpts(QWidget *parent = 0);

    /*!
     * \brief
     * &nbsp;&nbsp;dlgImportOpts::<span style="color: green;">~dlgImportOpts()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~dlgImportOpts();

private slots:
    void slotImportADIF();
    void slotDone();
    void slotUpdateStatusMsg(QString msg);

private:
    /*! \class importPrivateData
     * \brief Local private data definitions   */
    class importPrivateData;
    importPrivateData    *imp_data;  //!< *d-ptr to local private data values

    Ui::dlgImportOptions *ui;        //!< *d-ptr to GUI form

    void connectSlots();
};

#endif // DLGIMPORTOPS_HPP

