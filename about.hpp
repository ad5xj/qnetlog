#ifndef ABOUT_HPP
#define ABOUT_HPP
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>

#include "ui_about.h"

namespace Ui {
class dlgAbout;
}

class dlgAbout : public QDialog
{
    Q_OBJECT

public:
    explicit dlgAbout(QWidget *parent = 0);
    virtual ~dlgAbout();

private:
    Ui::dlgAbout *ui;
};

#endif // ABOUT_HPP

