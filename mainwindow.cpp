#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QList>
#include <QtCore/QDir>
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QDialog>
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QFontDialog>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QHeaderView>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlRelationalTableModel>

#include "adif2exporter.hpp"
#include "adif2parser.hpp"
#include "adif2reader.hpp"
#include "adifmodel.hpp"
#include "nets.hpp"
#include "bands.hpp"
#include "modes.hpp"
#include "status.hpp"
#include "loctyp.hpp"
#include "comboboxdelegate.hpp"
#include "yesnodelegate.hpp"
#include "chkboxdelegate.hpp"
#include "editsessionlogmodel.hpp"
#include "callsigns.hpp"
#include "sessionlogdtl.hpp"
#include "sessionloghdr.hpp"
#include "dlgbandsmaint.hpp"
#include "dlgnetsmaint.hpp"
#include "dlgcallsigns.hpp"
#include "dlglogmaint.hpp"
#include "dlglogedit.hpp"
#include "dlgimportops.hpp"
#include "about.hpp"

#include "mainwindow.hpp"

#define FORM_TITLE "QNetLog "

class MainWindow::mwPrivateData
{
public:
    bool formloaded;
    bool headerloaded;
    bool m_utc;
    bool goodCallSign;
    bool netStarted;
    bool font_bold;
    bool font_italic;
    bool font_underline;

    // using int here to be compatible with SQLITE database types
    int  m_netoccur;
    int  m_netdays;
    int  m_netmode;
    int  m_netband;
    int  m_hdrndx;
    int  m_loggedcnt;
    int  netsIndex;

    // needed for QColor and QFont data types
    int  color_r, color_g, color_b, color_a;
    int font_size;

    QPoint pos;
    QSize  size;

    QString m_oper;
    QString m_netname;
    QString m_netdate;
    QString m_netfreq;
    QString m_netbandname;
    QString m_netmodename;
    QString m_netnotes;
    QString colhdrs;
    QString colwidths;
    QString font_family;
    QString fontSelected; // to be stored in prefs
    QString altRowColor;  // to be stored in prefs
    QString currentCall;

    QVector<QString>    bands;
    QVector<QString>    modes;
    QVector<NetsRecord> nets;
    QVector<QString>    status;  // string is text that describes check in status
    QVector<QString>    loctyp;  // string to display which radio button was selected

    QColor altColor;      // color to be used

    QFont  font;
    QFont  altFont;       // font to be used

    QTime netStTime;
    QTime netEndTime;

    QTimer *tmrCurTime;

    QStandardItemModel  *session_model;
    QTableWidgetItem    *item_model;
    QItemSelectionModel *itemsel_model;
    QSqlQueryModel      *sqlmodel;

    SessionDtlRecord dtlrec;
    SessionHdrRecord hdrrec;
    SessionHeader *hdr;      // database header record
    SessionDetail *dtl;

    dlgNetsMaint  *dlgNets;
    dlgCallSigns  *dlgCalls;
    DlgLogMaint   *dlgLog;
    DlgLogEdit    *dlgLogEdit;
    dlgAbout      *dlgAbt;
    dlgImportOpts *dlgOpts;
    DlgBandsMaint *dlgBands;
};

MainWindow::MainWindow() : QMainWindow(), ui(new Ui::MainWindow)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    mw_data = new mwPrivateData;
    mw_data->formloaded = false;
    mw_data->headerloaded = false;

    ui->setupUi(this);

    createObjects();

    connectSlots();

    readSettings();
    displaySettings();

    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mw_data;
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

// ============================================================= //
// ===================  Event Handlers  ======================== //
// ============================================================= //
void MainWindow::resizeEvent(QResizeEvent *)
{
    // TODO: we may make this a fixed size window
    ui->grpNetSel->move(2,2);
    ui->grpNetStats->move(4,40);
    ui->grpLogEntry->move(2,170);
    quint32 h = height();
    quint32 w = this->width() - 6;
    ui->tblCurSession->setGeometry(2, 380, w, h - 480);
    mw_data->pos = pos();
    mw_data->size = size();
    writeSettings();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    // if form is loaded and has moved we save the position
    if ( mw_data->formloaded )
    {
        mw_data->pos = pos();
        mw_data->size = size();
        writeSettings();
    }
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::closeEvent(QCloseEvent *)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    // save the log columns to stored string
    int z;
    mw_data->colwidths = "";
    for ( int yy = 0; yy < 12; ++yy )
    {
        if ( yy > 0 ) mw_data->colwidths += "|";
        z = ui->tblCurSession->columnWidth(yy);
        mw_data->colwidths += QString("%1").arg(z);
    }
    mw_data->pos = pos();
    mw_data->size = size();
    writeSettings();
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::showEvent(QShowEvent *)
{
    if ( !mw_data->formloaded )
    {
        testDB();
        // set default state for some widgets
        clearForm();
        mw_data->m_loggedcnt = 0;
        mw_data->netStarted = false;
        mw_data->tmrCurTime->start();
        mw_data->formloaded = true;
    }
    // these must appear here to give time for database to be
    // created if it does not exist
    displaySettings();
    loadNetsCombo();
    // find bands and modes from db
    Bands    *band = new Bands(this);
    Modes    *mode = new Modes(this);

    QSqlQuery b = band->findBands();
    b.first();
    // load bands list
    while ( b.isValid() )
    {
        mw_data->bands.append(b.record().field("Label").value().toString());
        b.next();
    }
    b.finish();

    // load modes list
    QSqlQuery m = mode->findModes();
    m.first();
    mw_data->modes.append("unknown");
    while ( m.isValid() )
    {
        mw_data->modes.append(m.record().field("Lable").value().toString());
        m.next();
    }
}

// ============================================================= //
// ===================  Slot Handlers  ========================= //
// ============================================================= //
void MainWindow::slotCurTimerTimeout()
{
    QString title = "Net Stats - ";
    title += QDate::currentDate().toString("yyyy-MM-dd");
    ui->lblStatsBox->setText(title);
    // called for each timer tick
    if ( mw_data->m_utc )
        ui->lblCurTime->setText(QDateTime::currentDateTimeUtc().toString("hh:mm:ss"));
    else
        ui->lblCurTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
    if ( mw_data->netStarted ) calcElapsedTime();
}

void MainWindow::slotStNet()
{
    // make sure we have started the net
    if ( mw_data->netStarted ) return;
    if ( ui->editNCS->text() == "" )
    {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        ui->statusBar->showMessage("Enter An NCS Callsign");
        QMessageBox msgBox;
        msgBox.setText(tr("Insufficient Data"));
        QString msg = tr("The Database requires a NCS callsign before saving the header record. Enter all required data first.");
        msgBox.setInformativeText(msg);
        msgBox.setStandardButtons(QMessageBox::Ok);
        int ret = msgBox.exec();
        if ( ret == QMessageBox::Accepted )
        {
            mw_data->netStarted = false;
            mw_data->m_hdrndx = 0;
            mw_data->goodCallSign = false;
            // enable actions valid for idle mode
            clearForm();
            ui->statusBar->showMessage("");
            if ( mw_data->m_utc )
            {
                ui->txtNetEndTime->setText(QDateTime::currentDateTimeUtc().toString("hh:mm:ss"));
            }
            else
            {
                ui->txtNetEndTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
            }
            ui->cboNetName->setCurrentIndex(0);
            ui->actionEnd_Net->setEnabled(false);
            ui->actionCallSignsMaint->setVisible(true);
            ui->actionLogMaint->setVisible(true);
            ui->actionNetsMaint->setVisible(true);
            ui->menuHelp->setEnabled(true);
            ui->actionContents->setEnabled(true);
            ui->statusBar->showMessage("");
            return;
        }
        return;
    }
    QString strMsg = ui->cboNetName->currentText() + "Net Logging Started ...";
    ui->statusBar->showMessage(strMsg);
    QString t = FORM_TITLE;
    t += " - ";
    t += ui->cboNetName->currentText();
    t += " STARTED";
    setWindowTitle(t);
    // update display to show stats
    if ( mw_data->m_utc )
        ui->txtNetStTime->setText(QDateTime::currentDateTimeUtc().toString("hh:mm:ss"));
    else
        ui->txtNetStTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
    QApplication::processEvents();

    // set proper button display status
    ui->btnStNet->setEnabled(false);
    ui->btnEndNet->setEnabled(true);
    ui->btnAbort->setEnabled(true);
    ui->btnLogIt->setEnabled(true);
    // disable actions not valid for this mode
    ui->actionStart_Net->setEnabled(false);
    ui->actionEnd_Net->setEnabled(true);
    ui->actionCallSignsMaint->setVisible(false);
    ui->actionNetsMaint->setVisible(false);
    ui->actionLogMaint->setVisible(false);
    ui->actionLogIt->setEnabled(true);
    ui->editCallSign->setEnabled(true);
    ui->editCity->setEnabled(true);
    ui->editGrid->setEnabled(true);
    ui->editName->setEnabled(true);
    ui->editState->setEnabled(true);
    ui->editNotes->setEnabled(true);
    ui->editCallSign->setFocus();
    ui->tblCurSession->setEnabled(true);

    // turn on net timer and display elapsed time
    int h,m,s;
    QString stTime;
    if ( mw_data->m_utc )
        stTime = QDateTime::currentDateTimeUtc().toString("MM/dd/YYYY hh:mm:ss");
    else
        stTime = QDateTime::currentDateTime().toString("MM/dd/YYYY hh:mm:ss");
    s = stTime.right(2).toInt();
    h = stTime.mid(12,2).toInt();
    m = stTime.mid(15,2).toInt();
    mw_data->netStTime = QTime(h,m,s);
    mw_data->netStTime.start();
    mw_data->m_loggedcnt = 0;
    mw_data->netStarted = true;

    createNetHdr();
}

void MainWindow::slotLogIt()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    ui->btnLogIt->setEnabled(false);
    // if callsign is not previously stored do it now
    if ( !mw_data->goodCallSign ) saveCall();
    saveSessionDtl();
    // clear form and get ready for another contact
    clearForm();
    mw_data->goodCallSign = false;
    // display contacts for this net session
    ui->tblCurSession->reset();
    mw_data->headerloaded = false;
    loadNetLog();
    ui->tblCurSession->resizeColumnsToContents();
    // Make the combo boxes always displayed.
    ComboBoxDelegate* delegate = new ComboBoxDelegate(this);
    YesNoDelegate* yesno = new YesNoDelegate(this);
    CheckBoxDelegate* chkd = new CheckBoxDelegate(this);
    ui->tblCurSession->setItemDelegateForColumn(3, delegate);
    ui->tblCurSession->setItemDelegateForColumn(9, delegate);
    ui->tblCurSession->setItemDelegateForColumn(10,yesno);
    ui->tblCurSession->setItemDelegateForColumn(11,chkd);
    ui->tblCurSession->setItemDelegateForColumn(12,chkd);
    ui->tblCurSession->setItemDelegateForColumn(13,chkd);
    ui->btnLogIt->setEnabled(true);
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::slotTimeBaseChgd()
{
    mw_data->m_utc = ui->btnUTC->isChecked();
}

void MainWindow::slotSelMARS(bool ck)
{
    ui->editMarsCall->setVisible(ck);
}

void MainWindow::slotSelLogEntry()
{
    // using item delegate edit now
}

void MainWindow::slotLogEditCancel()
{
    /*
    ui->btnEditCancel->setEnabled(false);
    ui->btnEditLog->setEnabled(true);
    ui->grpEdit->setVisible(false);
    */
}

void MainWindow::slotEditLogEntry()
{
    // re-constructing to use item delegate

}

void MainWindow::slotDlgNets()
{
    mw_data->dlgNets = new dlgNetsMaint(this);
    mw_data->dlgNets->exec();
}

void MainWindow::slotDlgBandsMaint()
{
    mw_data->dlgBands = new DlgBandsMaint(this);
    mw_data->dlgBands->exec();
}

void MainWindow::slotDlgLogFile()
{
    mw_data->dlgLog = new DlgLogMaint(this);
    mw_data->dlgLog->show();
}

void MainWindow::slotAbortNet()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Abort Net Session"));
    QString msg = tr("You have pressed the ABORT button. ");
    msg += tr("Aborting the net entry at this point will destroy the record for this net session. ");
    msg += tr("If this is really what you wish to do answer YES. ");
    msg += tr("Otherwise answer NO to continue.");
    msgBox.setInformativeText(msg);
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int ret = msgBox.exec();
    switch ( ret )
    {
    case QMessageBox::Yes:
        removeSessionHdr();
        resetSession();
        break;
    case QMessageBox::No:
        break;
    }
}

void MainWindow::slotEndNet()
{
    //qDebug() << "Updating header for key " << m_hdrndx;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    // turn off net timer

    QString endtime;
    endtime = QDateTime::currentDateTime().toString("hh:mm:ss");
    ui->txtNetEndTime->setText(endtime);
    updtSessionHdr();

    mw_data->netStarted = false;
    mw_data->m_hdrndx = 0;
    mw_data->goodCallSign = false;
    // enable actions valid for idle mode
    clearForm();
    setWindowTitle(FORM_TITLE);
    if ( mw_data->m_utc )
    {
        ui->txtNetEndTime->setText(QDateTime::currentDateTimeUtc().toString("hh:mm:ss"));
    }
    else
    {
        ui->txtNetEndTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
    }
    ui->cboNetName->setCurrentIndex(0);
    ui->actionEnd_Net->setEnabled(false);
    ui->actionCallSignsMaint->setVisible(true);
    ui->actionLogMaint->setVisible(true);
    ui->actionNetsMaint->setVisible(true);
    ui->menuHelp->setEnabled(true);
    ui->actionContents->setEnabled(true);
    ui->statusBar->showMessage("");
    ui->btnStNet->setEnabled(true);
    ui->btnEndNet->setEnabled(false);
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::slotSessionLog()
{
    // not yet
}

void MainWindow::slotAbout()
{
    mw_data->dlgAbt = new dlgAbout();
    mw_data->dlgAbt->exec();
}

void MainWindow::slotDlgCalls()
{
    mw_data->dlgCalls = new dlgCallSigns(this);
    mw_data->dlgCalls->exec();
}

void MainWindow::slotCallChgd(QString txt)
{
    QString c;
    CallSigns calls;

    c = txt.trimmed().toUpper();
    bool mobile = c.contains("/");
    if (mobile)
    {
        int x = c.indexOf("/",0);
        c = c.left(x);
    }
    // clear the list if any items shown
    ui->lstViewCalls->clear();
    QString strRow = "";
    ui->lstViewCalls->clear();
    if ( c == "" ) return;
    // find the records
    QSqlQuery n = calls.findCallSigns(c);
    n.first();
    if (!n.isValid())
    {
        ui->editCallSign->setText(c);
        return;
    }
    // good records so start by adding first record
    // add first one found
    bool valid = n.first();
    if (valid)
    {
        do
        {
          // insert the rest of the rows from the data
          strRow = n.value(1).toString();
          if ( strRow.left(c.length()) == c )
              new QListWidgetItem(strRow, ui->lstViewCalls);
          valid = n.next();
        } while(valid);
    }
    ui->lstViewCalls->setEnabled(true);
//    ui->lstViewCalls->show();
    if ( mw_data->goodCallSign )
    {
        CallSignsRecord rec;
        rec.Call = mw_data->currentCall;
        CallSigns calls;
        QSqlQuery qry = calls.findCallSigns(mw_data->currentCall);
        qry.first();
        if ( qry.isValid() )
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Abort Net Session"));
            QString msg = tr("You have pressed the ABORT button. ");
            msg += tr("Aborting the net entry at this point will destroy the record for this net session. ");
            msg += tr("If this is really what you wish to do answer YES. ");
            msg += tr("Otherwise answer NO to continue.");
            msgBox.setInformativeText(msg);
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            int ret = msgBox.exec();
            switch ( ret )
            {
            case QMessageBox::Yes:
                removeSessionHdr();
                resetSession();
                break;
            case QMessageBox::No:
                break;
            }

            ui->editName->setText(qry.value(2).toString());
            ui->editCity->setText(qry.value(3).toString());
            ui->editState->setText(qry.value(4).toString());
            ui->editGrid->setText(qry.value(5).toString());
        }
        else
            return;
        //qDebug() << "Checking Log for callsign..." << currentCall;
        SessionDtlRecord logrec;
        logrec.callsign = mw_data->currentCall;
        logrec.hdrndx = mw_data->m_hdrndx;
        SessionDetail log;

        logrec.callsign = mw_data->currentCall;
        // begin
        ui->txtLstLoggedDt->setText(log.findLastLogged(logrec));
    }
    ui->editCallSign->setText(c);
}

void MainWindow::slotCallSelected()
{
    mw_data->currentCall = ui->lstViewCalls->item(ui->lstViewCalls->currentRow())->text();
    mw_data->m_netname = ui->cboNetName->currentIndex();
    mw_data->goodCallSign = true;
    ui->editCallSign->setText(mw_data->currentCall);
    QApplication::processEvents();
}

void MainWindow::slotNetChgd()
{
    if ( mw_data->netStarted )
    {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        QMessageBox msgBox;
        msgBox.setText("Procedural Error - The net has been started. You must stop the current net to change nets.");
        msgBox.exec();
        return;
    }
    // get ID from cbobox
    int id;
    if ( ui->cboNetName->currentIndex() <= 0 ) return;
    id = ui->cboNetName->itemData(ui->cboNetName->currentIndex()).toInt(); // save the key for the session hdr record;
    mw_data->m_hdrndx = id;
    int j = 0;
    int k = mw_data->nets.count();
    int l = 0;
    // find net record in net vector list
    do
    {
        l = mw_data->nets[j].id;
        if ( l == id ) break;
        ++j;
    } while (j < k);

    QStringList bandslist;
    for ( quint16 a = 0; a < mw_data->bands.count(); ++a )
    {
        bandslist << mw_data->bands[a];
    }
    //modeslist << QString("SSB|Digital|AM|FM").split("|", QString::KeepEmptyParts);
    QStringList modeslist;
    for ( quint16 b = 0; b < mw_data->modes.count(); ++b )
    {
        modeslist << mw_data->modes[b];
    }
    QString nn;
    // now that we know which net is selected we can get the record with all the data
    mw_data->m_hdrndx = mw_data->nets[j].id;
    mw_data->m_utc = false;
    if ( mw_data->nets[j].TimeBase == 0 ) mw_data->m_utc = true;
    mw_data->m_netoccur = mw_data->nets[j].Occurs;
    mw_data->m_netdays = mw_data->nets[j].DayOfWeek;
    mw_data->m_loggedcnt = 0;
    mw_data->m_netband = mw_data->nets[j].NetBand;
    mw_data->m_netbandname = bandslist[mw_data->nets[j].NetBand];
    mw_data->m_netmode = mw_data->nets[j].NetMode;
    mw_data->m_netmodename = modeslist[mw_data->nets[j].NetMode];
    mw_data->m_netfreq = mw_data->nets[j].NetFrequency;
    mw_data->netStTime = mw_data->nets[j].NetStTime;
    mw_data->m_netname = mw_data->nets[j].NetName;
    mw_data->m_netnotes = mw_data->nets[j].NetNotes;
    QString t = FORM_TITLE;
    t += " - ";
    t += mw_data->m_netname;
    setWindowTitle(t);
    ui->grpNetStats->setEnabled(true);

    if ( mw_data->m_utc )
        ui->btnUTC->setChecked(true);
    else
        ui->btnLocalTime->setChecked(true);
    ui->editNetStTime->setText(mw_data->netStTime.toString("hh:mm AP"));
    ui->txtNetFreq->setText(mw_data->m_netfreq);
    ui->editRealFreq->setText(mw_data->m_netfreq); // default to sched freq
    ui->editNetNotes->setEnabled(true);
    ui->lblNetBand->setText(mw_data->m_netbandname);
    ui->lblNetMode->setText(mw_data->m_netmodename);
    ui->actionStart_Net->setEnabled(true);
    ui->editNCS->setFocus();  // where to go after choosing net
}

void MainWindow::slotReset()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if ( mw_data->netStarted ) slotEndNet();
    clearForm();
    ui->tblCurSession->reset();
    ui->cboNetName->setCurrentIndex(0);
    ui->actionStart_Net->setEnabled(false);
    ui->grpNetStats->setEnabled(false);
    ui->txtElapsedTime->setText("00:00:00");
    ui->txtNetEndTime->setText("00:00:00");
    ui->txtLstLoggedDt->setText("");
    ui->txtNetStTime->setText("00:00:00");
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::slotImportADIF()
{
    mw_data->dlgOpts = new dlgImportOpts(this);
    mw_data->dlgOpts->exec();
}

// ============================================================= //
// =================  Friendly Functions  ====================== //
// ============================================================= //
void MainWindow::calcElapsedTime()
{
    // calc elapsed time
    int tms = mw_data->netStTime.elapsed();
    int h,m,s;
    int hh,hms,mms,mmm,sms; //,ms,ss;

    hms = 60 * 60 * 1000;   // number of ms in one hour
    h  = int(tms / hms);    // calc number of hours
    hh = h * hms;           // number of hours in ms

    mms = 60 * 1000;        // number of ms in one min
    mmm = tms - hh;         // calc min ms from totalms - hrsms
    m  = int(mmm / mms);    // calc number of min

    sms = 1000;              // mumber of ms in one second
    sms = tms - (hh + (m * mms));       // calc secms from hrsms + minms
    s  = int(sms /1000);
    QTime tim = QTime(h,m,s);
    QString etime = tim.toString("hh:mm:ss");
    // display elapsed time
    ui->txtElapsedTime->setText(etime);
}

void MainWindow::createNetHdr()
{
    // create the header record to attach details
    SessionHdrRecord rec;
    rec.id = 0; // default for new record
    rec.netid = mw_data->m_hdrndx;
    if ( !mw_data->m_utc )
    {  // local timebase
        rec.timebase = 0;
        rec.sessiondate = QDateTime::currentDateTime().toString("yyyy-MM-dd");
    }
    else
    {  // UTC timebase
        rec.timebase = 1;
        rec.sessiondate = QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd");
    }
    rec.sessionmode     = mw_data->m_netmode;
    rec.sessionoccur    = mw_data->m_netoccur;
    rec.sessiondays     = mw_data->m_netdays;
    rec.sessionband     = mw_data->m_netband;
    rec.stationcnt      = 0;                   // set to zero before logging contacts
    rec.sessionfreq     = ui->editRealFreq->text().trimmed(); // scheduled freq in net record
    rec.sessionncs      = ui->editNCS->text().trimmed();
    rec.sessionst       = ui->editNetStTime->text().trimmed();
    rec.sessionend      = "00:00";
    rec.sessionnotes    = ui->editNetNotes->document()->toPlainText(); // net session notes - not contact notes


    SessionHeader hdr;                  // save the default header data
    hdr.save(rec);

    int hdridx = hdr.findLastHeader();  // remember the new index for attachment to the details

    if ( hdridx <= 0 )
    {  // error on return of zero or minus value
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        ui->statusBar->showMessage("Database Error");
        QMessageBox msgBox;
        msgBox.setText(tr("Database Error."));
        QString msg = tr("The Database has reported an error while saving the header record. Please contact the program author.");
        msg += tr("\n\nThe error: ");
        msg += QString("%1").arg(hdr.getLastErr());
        msg += tr(" ");
        msg += hdr.getLastErrMsg();
        msgBox.setInformativeText(msg);
        msgBox.setStandardButtons(QMessageBox::Ok);
        int ret = msgBox.exec();
        if ( ret == QMessageBox::Accepted )
        {
            mw_data->netStarted = false;
            mw_data->m_hdrndx = 0;
            mw_data->goodCallSign = false;
            // enable actions valid for idle mode
            clearForm();
            ui->statusBar->showMessage("");
            if ( mw_data->m_utc )
            {
                ui->txtNetEndTime->setText(QDateTime::currentDateTimeUtc().toString("hh:mm:ss"));
            }
            else
            {
                ui->txtNetEndTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
            }
            ui->cboNetName->setCurrentIndex(0);
            ui->actionEnd_Net->setEnabled(false);
            ui->actionCallSignsMaint->setVisible(true);
            ui->actionLogMaint->setVisible(true);
            ui->actionNetsMaint->setVisible(true);
            ui->menuHelp->setEnabled(true);
            ui->actionContents->setEnabled(true);
            ui->statusBar->showMessage("");
            return;
        }
        return;
    }
    mw_data->m_hdrndx = hdridx;
}

void MainWindow::saveSessionDtl()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    quint32 err = 0;
    quint32 n   = 0; // to save the seq value after increment
    int ret = 0;
    QDateTime tim;
    SessionDetail dtl;
    SessionDtlRecord rec;

    // save the session detail record with hdr key
    // scrape the screen form and load a record struct
    rec.id         = 0; // must be zero on add
    rec.hdrndx     = mw_data->m_hdrndx;
    rec.rectime    = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:MM:ss");
    n              = mw_data->m_loggedcnt + 1;
    rec.dtlseq     = n;
    rec.callsign   = ui->editCallSign->text().trimmed().toUpper();
    tim            = QDateTime::fromString(ui->txtNetStTime->text());
    //Status Items << "Contacted"  << "Early Ck In"
    //         << "Reg Chk in" << "Check Out"
    //         << "Comments"   << "Short time"
    //         << "Traffic"    << "Re-Check"
    //         << "Radio Check";
    if ( mw_data->m_utc )
    {
        if ( tim < QDateTime::currentDateTimeUtc() )
            rec.status = "Early Ck In";
        else
            rec.status = "Reg Chk In";
    }
    if ( ui->btnFixed->isChecked() )    rec.stationtype = 1;
    if ( ui->btnMobile->isChecked() )   rec.stationtype = 2;
    if ( ui->btnPortable->isChecked() ) rec.stationtype = 3;
    rec.marscall   = ui->editMarsCall->text().trimmed().toUpper();
    rec.firstname  = ui->editName->text().trimmed();
    rec.city       = ui->editCity->text().trimmed();
    rec.st         = ui->editState->text().trimmed().toUpper();
    rec.gridsquare = ui->editGrid->text().trimmed();
    rec.notes      = ui->editNotes->text().trimmed();
    rec.ares       = 0;
    rec.mars       = 0;
    rec.satern     = 0;
    rec.traffic    = 0;

    if ( ui->chkARES->isChecked() )    rec.ares    = 1;
    if ( ui->chkMARS->isChecked() )    rec.mars    = 1;
    if ( ui->chkSatern->isChecked() )  rec.satern  = 1;
    if ( ui->chkTraffic->isChecked() ) rec.traffic = 1;

    rec.notes = "";

    // call database function with record data and save
    ret = dtl.save(rec);
    if ( ret <= 0 )
    {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        err = dtl.getLastErr();
        QString msg = dtl.getLastErrMsg();
        qDebug() << QString("Database Error on Update - %1 :").arg(err) << msg;
        QMessageBox msgBox;
        msgBox.setText("Database Error" + msg);
        msgBox.exec();
        slotReset();
        return;
    }
    else
    {
        mw_data->m_loggedcnt = n;
        ui->txtStLogged->setText(QString("%1").arg(n));
    }
    // reset for next log entry
    clearForm();
    mw_data->goodCallSign = false;
//    loadNetLog();
    ui->tblCurSession->resizeColumnsToContents();
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::updtSessionHdr()
{
    // set up net session header record for updt
    // NOTE: This recored has already been saved so not all fields get update.
    SessionHdrRecord rec;

    rec.id = mw_data->m_hdrndx;                     // set to current hdr index
    rec.netid = ui->cboNetName->currentIndex();
    if ( ui->btnUTC->isChecked() )
        rec.timebase = 1;                          // set to 1 if UTC
    else
        rec.timebase = 0;                          // default to localtime

    rec.sessionfreq = ui->editRealFreq->text();    // actual freq used for net
    rec.sessionst   = ui->txtNetStTime->text();    // header always has actual st time
    rec.sessionend  = ui->txtNetEndTime->text();   // header always has actual end time
    rec.sessionlen  = ui->txtElapsedTime->text();  // to be calc'd on net end
    rec.stationcnt  = mw_data->m_loggedcnt;        // count of stations logged

    mw_data->hdr = new SessionHeader(this);        // only needed here destroy later
    mw_data->m_hdrndx = mw_data->hdr->update(rec); // returns last inserted header id

    delete mw_data->hdr;                           // now destroy hdr - don't leave it hanging around
}

void MainWindow::removeSessionHdr()
{
    SessionHeader hdr;
    SessionHdrRecord rec;

    rec.id = mw_data->m_hdrndx;

    if ( !hdr.del(rec) )
    {
        QMessageBox msgBox;
        msgBox.setText(tr("Database Error."));
        QString msg = tr("The Database has reported an error while removing the header record.");
        msg += tr("\n\nThe error: ");
        msg += QString("%1").arg(hdr.getLastErr());
        msg += tr(" ");
        msg += hdr.getLastErrMsg();
        msgBox.setInformativeText(msg);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return;
    }
}

void MainWindow::resetSession()
{
    clearForm();
    QString t = FORM_TITLE;
    setWindowTitle(t);
    ui->statusBar->showMessage("");
    ui->txtNetEndTime->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
    ui->actionStart_Net->setEnabled(true);
    ui->actionEnd_Net->setEnabled(false);
    ui->actionCallSignsMaint->setVisible(true);
    ui->actionLogMaint->setVisible(true);
    ui->actionNetsMaint->setVisible(true);
    ui->menuHelp->setEnabled(true);
    ui->actionContents->setEnabled(true);
    ui->statusBar->showMessage(tr("Session Header Removed..."));
    QApplication::processEvents();

    ui->btnStNet->setEnabled(true);
    ui->btnEndNet->setEnabled(false);
    ui->btnAbort->setEnabled(false);
    ui->btnLogIt->setEnabled(false);
    ui->editCity->setEnabled(false);
    ui->editGrid->setEnabled(false);
    ui->editName->setEnabled(false);
    ui->editState->setEnabled(false);
    ui->editNotes->setEnabled(false);
    ui->editCallSign->setFocus();
    ui->tblCurSession->setEnabled(false);
    // turn on net timer and display elapsed time
    ui->txtNetStTime->setText("00:00:00");
    ui->txtElapsedTime->setText("00:00:00");
    ui->txtNetEndTime->setText("00:00:00");
    mw_data->netStTime.setHMS(0,0,0);
    mw_data->netStarted = false;
    mw_data->m_hdrndx = 0;
    mw_data->goodCallSign = false;
    ui->cboNetName->setCurrentIndex(0); // we can only reset it after netStarted is false
}

void MainWindow::saveCall()
{
    int x = 0;
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QString callsign;
    CallSignsRecord rec;
    CallSigns call;
    QApplication::processEvents();

    callsign = ui->editCallSign->text();
    bool mobile = callsign.contains("/");
    if (mobile)
    {
        x = callsign.indexOf("/",0);
        callsign = callsign.left(x);
    }
    // define parameters for net log query
    rec.Call  = callsign;
    rec.Name  = ui->editName->text().trimmed();
    rec.City  = ui->editCity->text();
    rec.St    = ui->editState->text().trimmed().toUpper();
    rec.Grid  = ui->editGrid->text();

    call.save(rec);
    mw_data->goodCallSign = true;
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void MainWindow::loadNetLog()
{
    EditSessionLogModel *model;

    ui->tblCurSession->reset();
    mw_data->m_loggedcnt = 0;
    // model/view display
    // editing in the table done with EditSessionLogModel delegate
    model = new EditSessionLogModel(this);

    model->initModel(mw_data->m_hdrndx);

    ui->tblCurSession->setModel(model);
    ui->tblCurSession->show();
    ui->tblCurSession->setRowHeight(0,22);
    ui->tblCurSession->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked);

    ui->lstViewCalls->clear();
    mw_data->m_loggedcnt = model->rowCount(QModelIndex());
    ui->txtStLogged->setText(QString("%1").arg(mw_data->m_loggedcnt));
}

void MainWindow::loadRefLists()
{
    Status st;
    LocType lt;
    QSqlQuery s;
    QSqlQuery t;

    mw_data->status.clear();  // vector list of all status labels
    mw_data->loctyp.clear();  // vector list of all location types

    s = st.findStats();
    do
    {
        mw_data->status.append(s.record().field("Label").value().toString());
        s.next();
    } while ( s.isValid());
    s.finish();

    t = lt.findLocTypes();
    do
    {
        mw_data->loctyp.append(t.record().field("Label").value().toString());
        t.next();
    } while ( t.isValid());
    t.finish();
}

void MainWindow::loadNetsCombo()
{
    int x = 0;
    int y = 0;
    Nets nets;              // database table
    NetsRecord rec;         // record stucture
    QSqlQuery n;            // query on table

    mw_data->nets.clear();  // vector list of all nets

    n = nets.findNets();

    n.first(); // reset to first record
    if ( n.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "Nets combo load fail - invalid query";
        qDebug() << "Err: " << QString("%1").arg(n.lastError().type()) << n.lastError().text();
        return;
    }
    n.last();
    y = n.record().count();
    // add the first non-selection position now
    n.first();
    rec.id = 0;
    rec.TimeBase = 0;
    rec.Occurs = 0;
    rec.DayOfWeek = 0x0;
    rec.NetBand = 0;
    rec.NetMode = 0;
    rec.NetFrequency = "00.000 Mhz";
    rec.NetName = "Unknown";
    rec.NetStTime = QTime(00,00,00);
    rec.NetNotes = "";
    // add to vector list
    mw_data->nets.append(rec);
    // also add to cboBox
    ui->cboNetName->addItem("Select Net",0);
    // now get the rest
    for ( x = 1; x < y; ++x )
    {
        //create a record for the nets vector list
        rec.id = n.record().field("id").value().toInt();
        rec.TimeBase = n.record().field("timebase").value().toInt();
        rec.Occurs = n.record().field("occurs").value().toInt();
        rec.DayOfWeek = n.record().field("dayofweek").value().toInt();
        rec.NetBand = n.record().field("netband").value().toInt();
        rec.NetFrequency = n.record().field("netfreq").value().toString();
        rec.NetMode = n.record().field("netmode").value().toInt();
        rec.NetName = n.record().field("netname").value().toString();
        rec.NetStTime = QTime().fromString(n.record().field("netsttime").value().toString(),"hh:mm AP");
        rec.NetNotes = n.record().field("netnotes").value().toString();
        // add rec to vector list
        mw_data->nets.append(rec);
        // also add to cboBox
        ui->cboNetName->addItem(rec.NetName,rec.id);
        n.next();
        if ( !n.isValid() ) break;
    }
    n.finish();
}

void MainWindow::createObjects()
{
    QPalette palette;
    QBrush altbrush;

    // create local objects for use with the QDesigner form
    mw_data->tmrCurTime = new QTimer(this);
    mw_data->tmrCurTime->setInterval(1000);
    mw_data->m_loggedcnt = 0;
    mw_data->m_hdrndx = 0;
    mw_data->currentCall = "";
    mw_data->goodCallSign = false;
    mw_data->netStarted = false;
    if ( ui->btnUTC->isChecked() ) mw_data->m_utc = true;

    // set up color for alternate rows
    altbrush.setColor(mw_data->altColor);
    palette.setBrush(QPalette::AlternateBase, altbrush);
    // force header font to following
    mw_data->font.setFamily(mw_data->font_family);
    mw_data->font.setPointSize(8);
    mw_data->font.setBold(true);
    mw_data->font.setItalic(false);
    mw_data->font.setUnderline(false);
    mw_data->font.setFamily(mw_data->font_family);
    if ( mw_data->font_size < 8 ) mw_data->font_size = 10;
    mw_data->font.setPointSize(mw_data->font_size);
    mw_data->font.setBold(mw_data->font_bold);
    mw_data->font.setItalic(mw_data->font_italic);
    mw_data->font.setUnderline(mw_data->font_underline);


    ui->tblCurSession->reset();
    ui->tblCurSession->setPalette(palette);
    ui->tblCurSession->setAlternatingRowColors(true);
    ui->tblCurSession->setFont(mw_data->font);
    ui->tblCurSession->setMouseTracking(true);
    ui->cboNetName->setCurrentIndex(0);
    ui->actionStart_Net->setEnabled(false);
}

void MainWindow::connectSlots()
{
    // connect menu actions
    connect(ui->actionExit,SIGNAL(triggered()),this,SLOT(close()));
    connect(ui->actionLogIt,SIGNAL(triggered()),this,SLOT(slotLogIt()));
    connect(ui->actionStart_Net,SIGNAL(triggered()),this,SLOT(slotStNet()));
    connect(ui->actionEnd_Net,SIGNAL(triggered()),this,SLOT(slotEndNet()));
    connect(ui->actionReset,SIGNAL(triggered()),this,SLOT(slotReset()));
    connect(ui->actionCallSignsMaint,SIGNAL(triggered()),this,SLOT(slotDlgCalls()));
    connect(ui->actionLogMaint,SIGNAL(triggered()),this,SLOT(slotDlgLogFile()));
    connect(ui->actionNetsMaint,SIGNAL(triggered()),this,SLOT(slotDlgNets()));
    connect(ui->actionBandsMaint,SIGNAL(triggered(bool)),this,SLOT(slotDlgBandsMaint()));
    connect(ui->actionImportCalSigns, SIGNAL(triggered()),this,SLOT(slotImportADIF()));
    connect(ui->actionSessionLog,SIGNAL(triggered(bool)),this,SLOT(slotSessionLog()));
    connect(ui->actionAbout,SIGNAL(triggered()),this,SLOT(slotAbout()));
    // connect GUI buttons
    connect(ui->btnUTC,SIGNAL(toggled(bool)),this,SLOT(slotTimeBaseChgd()));
    connect(ui->btnLocalTime,SIGNAL(toggled(bool)),this,SLOT(slotTimeBaseChgd()));
    connect(ui->chkMARS,SIGNAL(toggled(bool)),this,SLOT(slotSelMARS(bool)));
    connect(ui->btnStNet,SIGNAL(clicked()),this,SLOT(slotStNet()));
    connect(ui->btnEndNet,SIGNAL(clicked()),this,SLOT(slotEndNet()));
    connect(ui->btnAbort,SIGNAL(clicked()),this,SLOT(slotAbortNet()));
    connect(ui->btnLogIt,SIGNAL(clicked()),this,SLOT(slotLogIt()));
    // connect session contact list actions
    connect(ui->tblCurSession,SIGNAL(clicked(QModelIndex)),this,SLOT(slotSelLogEntry()));
    connect(ui->tblCurSession,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(slotEditLogEntry()));
    // connect interactivity for editing
    connect(ui->editCallSign,SIGNAL(textChanged(QString)),this,SLOT(slotCallChgd(QString)));
    connect(ui->lstViewCalls,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(slotCallSelected()));
    connect(ui->cboNetName,SIGNAL(currentIndexChanged(int)),this,SLOT(slotNetChgd()));
    // connect timer timeout signal
    connect(mw_data->tmrCurTime,SIGNAL(timeout()),this,SLOT(slotCurTimerTimeout()));


    QMetaObject::connectSlotsByName(this);
}

void MainWindow::clearForm()
{
    ui->editCallSign->setText("");
    ui->editCity->setText("");
    ui->editGrid->setText("");
    ui->editName->setText("");
    ui->editState->setText("");
    ui->editNotes->setText("");
    ui->chkARES->setChecked(false);
    ui->chkSatern->setChecked(false);
    ui->chkTraffic->setChecked(false);
    ui->btnFixed->setChecked(true);
    if ( !mw_data->netStarted )
    {
        ui->btnLogIt->setEnabled(false);
        ui->editCallSign->setEnabled(false);
        ui->editCity->setEnabled(false);
        ui->editGrid->setEnabled(false);
        ui->editName->setEnabled(false);
        ui->editState->setEnabled(false);
        ui->editNotes->setEnabled(false);
        ui->actionLogIt->setEnabled(false);
        ui->actionStart_Net->setEnabled(false);
        ui->actionEnd_Net->setEnabled(false);
        ui->actionLogIt->setEnabled(false);
        ui->grpNetStats->setEnabled(false);
        ui->tblCurSession->setEnabled(false);
    }
    mw_data->goodCallSign = false;
    ui->editMarsCall->setVisible(false);
    ui->editCallSign->setFocus();
    ui->editCallSign->setSelection(0,ui->editCallSign->text().size());
}

void MainWindow::colorPrefs()
{
    QColor color = QColorDialog::getColor(Qt::green, this);
    if (color.isValid())
    {
        // save color locally
        mw_data->altColor = color;
        mw_data->color_r = color.red();
        mw_data->color_g = color.green();
        mw_data->color_b = color.blue();
        mw_data->color_a = color.saturation();
        QSettings settings("AD5XJ","QNetLog");
        settings.beginGroup("LogList");
         settings.setValue("altRowColorR", QString("%1").arg(mw_data->color_r));
         settings.setValue("altRowColorG", QString("%1").arg(mw_data->color_g));
         settings.setValue("altRowColorB", QString("%1").arg(mw_data->color_b));
         settings.setValue("altRowColorA", QString("%1").arg(mw_data->color_a));
        settings.endGroup();
    }
}

void MainWindow::fontPrefs()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, QFont("DejaVu Sans", 10), this);
    if (ok)
    {
         // the user clicked OK and font is set to the font the user selected
        mw_data->altFont = font;
        mw_data->font_family = font.family();
        mw_data->font_size   = font.pointSize();
        mw_data->font_italic = font.italic();
        mw_data->font_bold   = font.bold();
        mw_data->font_underline = font.underline();
        QSettings settings("AD5XJ","QNetLog");
        settings.beginGroup("ListLog");
         settings.setValue("font_family", mw_data->font_family);
         settings.setValue("font_point", mw_data->font_size);
         if ( mw_data->font_italic )
            settings.setValue("font_italic", "1");
         else
            settings.setValue("font_italic", "0");
         if ( mw_data->font_bold )
            settings.setValue("font_bold", "1");
         else
            settings.setValue("font_bold", "0");
         if ( mw_data->font_underline )
            settings.setValue("font_underline", "1");
         else
            settings.setValue("font_underline", "0");
    }
}

void MainWindow::resize_log_columns(QString cols)
{
    if ( mw_data->headerloaded ) return;
    int x = 0;
    int y = 0;
    QStringList col_list;

    // only headers are bold here
    mw_data->font.setBold(false);
    QHeaderView *headerView = ui->tblCurSession->horizontalHeader();
    headerView->setStretchLastSection(true);
    mw_data->sqlmodel->setHeaderData(0, Qt::Horizontal, "ID");
    mw_data->sqlmodel->setHeaderData(1, Qt::Horizontal, tr("Seq"));
    mw_data->sqlmodel->setHeaderData(2, Qt::Horizontal, tr("Time"));
    mw_data->sqlmodel->setHeaderData(3, Qt::Horizontal, tr("Status"));
    mw_data->sqlmodel->setHeaderData(4, Qt::Horizontal, tr("  Call\n   Sign "));
    mw_data->sqlmodel->setHeaderData(5, Qt::Horizontal, tr("     Name      "));
    mw_data->sqlmodel->setHeaderData(6, Qt::Horizontal, tr("City"));
    mw_data->sqlmodel->setHeaderData(7, Qt::Horizontal, tr("St/Pr"));
    mw_data->sqlmodel->setHeaderData(8, Qt::Horizontal, tr("Grid"));
    mw_data->sqlmodel->setHeaderData(9, Qt::Horizontal, tr("Loc \n Type  "));
    mw_data->sqlmodel->setHeaderData(10, Qt::Horizontal, tr("Tr"));
    mw_data->sqlmodel->setHeaderData(11, Qt::Horizontal, tr("ARES"));
    mw_data->sqlmodel->setHeaderData(12,Qt::Horizontal, tr("SATERN"));
    mw_data->sqlmodel->setHeaderData(13,Qt::Horizontal, tr("MARS"));
    mw_data->sqlmodel->setHeaderData(14, Qt::Horizontal, tr("         N O T E S         "));
    // TODO: add status column with edit delegate combo box
    //       chk in,ck out, traffic, short time,etc.
    // 30|30|166|70|69|102|34|55|60|33|80|200
    mw_data->font.setBold(false);
    // resize the log columns by stored string
    col_list = cols.split("|",QString::KeepEmptyParts);
    x = col_list.count();
    for ( y = 0; y < x; ++y )
    {
        ui->tblCurSession->setColumnWidth(y,col_list.at(y).toInt());
    }
}

void MainWindow::displaySettings()
{
    if ( mw_data->font_size <8 ) mw_data->font_size = 10;

    QFont font;
    font.setFamily(mw_data->font_family);
    font.setPointSize(mw_data->font_size);
    font.setBold(mw_data->font_bold);
    font.setItalic(mw_data->font_italic);
    font.setUnderline(mw_data->font_underline);
    mw_data->altFont = font;

    QByteArray a = FORM_TITLE;
    QString b = a + APP_VERSION;
    this->setWindowTitle(b);
    this->move(mw_data->pos);

}

void MainWindow::readSettings()
{
    int p = 0;

    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("Main");
     mw_data->pos  = settings.value("pos", QPoint(100,200)).toPoint();
     mw_data->size = settings.value("size", QSize(652,683)).toSize();
     //ID|SEQ|Time|Status|Call\n  Sign  |     Name      |City|St|Grid| Loc \n  Type  |Tr|Affiliation|       N O T E S
     mw_data->colwidths = settings.value("ColWidths","30|30|166|70|69|102|34|55|60|33|80|80|80|80|200").toString();
    settings.endGroup();

    settings.beginGroup("LogList");
     mw_data->color_r = settings.value("altRowColorR", "100").toInt();
     mw_data->color_g = settings.value("altRowColorG", "100").toInt();
     mw_data->color_b = settings.value("altRowColorB", "255").toInt();
     mw_data->color_a = settings.value("altRowColorA", "100").toInt();
     mw_data->altColor = QColor(mw_data->color_r,mw_data->color_g,mw_data->color_b,mw_data->color_a);
     mw_data->font_family = "Arial";
     mw_data->font_family = settings.value("font_family", "DejaVu Sans Mono").toString();
     p = settings.value("font_point", 10).toInt();
     if ( p < 8 || p > 54 ) p = 10;
     mw_data->font_size  = p;
     mw_data->font_bold = false;
     if (settings.value("font_bold", "0").toString() == "1") mw_data->font_bold = true;
     mw_data->font_italic = false;
     if (settings.value("font_italic", "0").toString() == "1") mw_data->font_italic = true;
     mw_data->font_underline = false;
     if (settings.value("font_underline", "0").toString() == "1") mw_data->font_underline = true;
    settings.endGroup();

    settings.beginGroup("MyPrefs");
     mw_data->m_oper = settings.value("callsign", "").toString();
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("Main");
     settings.setValue("pos", mw_data->pos);
     settings.setValue("size", mw_data->size);
     settings.setValue("ColWidths", mw_data->colwidths);
    settings.endGroup();

    settings.beginGroup("LogList");
     settings.setValue("altRowColorR", mw_data->color_r);
     settings.setValue("altRowColorG", mw_data->color_g);
     settings.setValue("altRowColorB", mw_data->color_b);
     settings.setValue("altRowColorA", mw_data->color_a);
     settings.setValue("font_family",  mw_data->font_family);
     settings.setValue("font_point",   mw_data->font_size);
     settings.setValue("font_bold",    mw_data->font_bold);
     settings.setValue("font_italic",  mw_data->font_italic);
     settings.setValue("font_underline", mw_data->font_underline);
    settings.endGroup();
    settings.sync();
}

void MainWindow::testDB()
{  // find out if db exists, if not create it with tables
    bool success;
    QString path;
    QDir dir;
    QSqlDatabase db;

    path = dir.absolutePath();
    path.append(QDir::separator()).append("logfile.db");
    path = QDir::toNativeSeparators(path);

    // open a database even if it does not exist
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("localhost");
    db.setDatabaseName("./logfile.db");
    if ( db.open("admin","netlogger") )
    {
//        qDebug() << "Database open successful";
    }
    else
    {
        QString msg = tr("Unable to establish a database connection.\n"
                         "Check to see if the logfile.db is in the same folder "
                         "as the application executable.....\n\nClick Cancel to exit.");
        QMessageBox::critical(0, tr("Cannot open database"), msg, QMessageBox::Cancel);
        close();
    }
    // see if it exists with tables created
    int t = db.database().tables().count();
    if ( t > 3 ) return;

//    qDebug() << "Tables count <= 3";
    QString sqlStr = "";
    dir.cdUp();

    success = false;
    path = dir.absolutePath();
    path.append(QDir::separator()).append("logfile.sql");
    path = QDir::toNativeSeparators(path);

    bool yesno = false;
    QFile sqlfile;
    yesno = sqlfile.exists(path);
    if ( !yesno ) return;

    sqlfile.setFileName(path);
    if ( sqlfile.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
//        qDebug() << "Reading DML";
        sqlStr = sqlfile.readAll();
        sqlfile.close();

        path = dir.currentPath();
        path = QDir::toNativeSeparators(path);
        dir.cd(path);
        if ( sqlStr.length() > 0 )
        {
//            qDebug() << "Executing DML";
            QSqlQuery query(db);
            // The SQLite driver executes only a single (the first) query
            // in the QSqlQuery DML script. If the script contains more
            // queries, it needs to be split into separate statements and
            // executed  individually. This for/next loop does just that.
            // The DML script is demarkated with a ';' to separate each
            // statement to executed separately each time through the loop.
            QStringList scriptQueries = QTextStream(&sqlStr).readAll().split(';');
            foreach ( QString queryTxt, scriptQueries )
            {
                if ( queryTxt.trimmed().isEmpty() ) continue;
                qDebug() << queryTxt;
                query.exec(queryTxt);
                if ( query.lastError().type() != QSqlError::NoError )
                {
                    QString msg = QApplication::tr("FATAL ERROR - One of the querys failed to execute. Error detail: ");
                    msg += query.lastError().text();
//                    qDebug() << msg;
                    success = false;
                }
                query.finish();
            }
            if ( query.lastError().type() == QSqlError::NoError )
            {
//                qDebug() << "Query success...";
                success = true;
            }
            query.finish();
        }

//        qDebug() << "New database created";
        if ( !success )
        {
            QMessageBox::critical(0, qApp->tr("Cannot open database"),
                qApp->tr("Unable to establish a database connection.\n"
                         "This example needs SQLite support. Please read "
                         "the Qt SQL driver documentation for information how "
                         "to build it.\n\n"
                         "Click Cancel to exit."), QMessageBox::Cancel);
        }
    }
}
