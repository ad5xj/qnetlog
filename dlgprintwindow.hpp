#ifndef DLGPRINTWINDOW_HPP
#define DLGPRINTWINDOW_HPP
/*! \file dlgprintwindow.hpp  */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMainWindow>

#include "ui_dlgprintwindow.h"

class QPrinter;
class QPainter;

namespace Ui {
class DlgPrintWindow;
}

/*! \ingroup QNETLOG
 * \brief The DlgPrintWindow class       */
class DlgPrintWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DlgPrintWindow(QWidget *parent = 0);
    virtual ~DlgPrintWindow();

protected:
    void changeEvent(QEvent *e);

private slots:
    void slotPrinterInfo();
    void slotRefreshUi();
    void slotBrowse();
    void slotSessionLog();

private:
    class dpPrivateData;
    dpPrivateData *dp_data;

    Ui::DlgPrintWindow *ui;

    void loadPrintersCombo();
    void printSessionLog();
    void printLogPage(int index, QPainter *painter, QPrinter *printer, bool fstPage);
};

#endif // DLGPRINTWINDOW_HPP
