#ifndef EDITSESSIONLOGMODEL_HPP
#define EDITSESSIONLOGMODEL_HPP
/*! \file editsessionlogmodel.hpp  */
#include <QtSql/QSqlQueryModel>

/*! \ingroup QNETLOG
 * \class EditSessionLogModel */
class EditSessionLogModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    /*!
     * \brief
     * This class is an edit delegate for the session log table.
     * Methods provided here customize the edit process in the
     * display grid rather than a separate form.
     * \param QObject parent
     * \sa MainWindow                                */
    EditSessionLogModel(QObject *parent = 0);

    void initModel(int key);
    bool setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

private:
    int foreignKey;

    bool setStatus(int logId, const QString &status);
    bool setCallsign(int logId, const QString &call);
    bool setFirstName(int logId, const QString &firstName);
    bool setCity(int logId, const QString &city);
    bool setSt(int logId, const QString &st);
    bool setLocTyp(int logId, const QString &type);
    bool setGrid(int logId, const QString &grid);
    bool setTraffic(int logId, QVariant yesno);
    bool setARES(int logId, QVariant yesno);
    bool setSATERN(int logId, QVariant yesno);
    bool setMARS(int logId, QVariant yesno);
    bool setNotes(int logId, const QString &notes);
    void refresh();
};
#endif // EDITSESSIONLOGMODEL_HPP

