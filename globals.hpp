#ifndef GLOABALS_HPP
#define GLOABALS_HPP
/*! \file globals.hpp
 * \defgroup QNETLOG     */
#include <QtCore/QString>
#include <QtCore/QDateTime>

/*! \ingroup QNETLOG
 * \def APP_NAME */
#define APP_NAME "QNetLogger"
/*! \def APP_Version  */
#define APP_VERSION "v0.97.1"

/*! \typedef struct DB_bands_rec
 * \brief Alias BandsRec insures proper data types are used for database record */
typedef struct DB_bands_rec
{
    int     id;
    int     ndx;
    QString Label;
}  BandsRec;

/*! \typedef struct DB_modes_rec
 * \brief Alias ModesRec insures proper data types are used for database record  */
typedef struct DB_modes_rec
{
    int     id;
    int     ndx;
    QString Label;
} ModesRec;

/*! \typedef struct DB_log_file_header
 * \brief Alias SessionHdrRecord insures proper data types are used for database record */
typedef struct DB_log_file_header
{              // one header for each net session started   //
    int id;                // header primary key
    int netid;             // key to nets definition table many->one nets record
    int timebase;          // utc / local definition
    int sessionoccur;      // int depending on radio button on net maint entry
    int sessiondays;       // bit matrix of days of the week
    int sessionmode;       // key to modes
    int sessionband;       // band from nets record
    int stationcnt;        // total no of stations logged for session
    QString sessiondate;   // actual date net session was logged
    QString sessionfreq;   // actual operating freq for session
    QString sessionst;     // net start time hh:mm:ss
    QString sessionend;    // net ending time hh:mm:ss
    QString sessionlen;    // total elapsed time for session hh:mm:ss
    QString sessionncs;    // NCS operator for this session
    QString sessionnotes;  // default value is from net definition table - editable
} SessionHdrRecord;

/*! \typedef struct DB_log_file_details
 * \brief Alias SessionDtlRecord insures proper data types are used for database record  */
typedef struct DB_log_file_details
{    // one record for each checkin of a session many->one session header //
    int     id;
    int     hdrndx;         // foreign key to header record
    int     dtlseq;         // log sequence of entry for session
    int     stationtype;    // index depending on radio button selected
    int     timebase;       // 0 or 1 for UTC or Local
    int     traffic;        // 1 for traffic item passed
    int     ares;           // 1 for ares affiliation
    int     satern;         // 1 for satern affiliation
    int     mars;           // 1 for MARS affiliation
    QString rectime;        // timestamp of log entry
    QString status;         // contacted,check in, short time, chk out, traffic,etc.
    QString callsign;       // always necessary
    QString marscall;       // only for MARS affiliation
    QString firstname;      //   these should be obvious
    QString city;           //
    QString st;             //
    QString gridsquare;     //
    QString notes;          //
} SessionDtlRecord;

/*! \typedef struct DB_nets_record_type
 * \brief Alias NetsRecord insures proper data types are used for database record   */
typedef struct DB_nets_record_type
{        //  one net definition for each unique net (includes time of day ) //
    int id;                 // Primary Key indexed
    bool    TimeBase;       // false-localtime true-UTC
    quint8  Occurs;         // 0-daily 1-weekdays 2-weekend 3-biweekly
    quint8  DayOfWeek;      // Each bit represents a day of the week
    quint8  NetMode;        // 0-AM 1-FM 2-SSB 3-Digital
    int     TimeFormat;     // time string format
    int     NetBand;        // indx from bands combo box selection index data
    QTime   NetStTime;      // Time that net starts with timebase TimeBase
    QString NetName;        // Net Identifier   indexed with timestamp
    QString NetFrequency;   // Radio frequency for the net in Khz
    QString NetNotes;       //
} NetsRecord;

/*! \typedef struct DB_callsigns_record_type
 * \brief Alias CallSignsRecord insures proper data types are used for database record   */
typedef struct DB_callsigns_record_type
{        // one record for each unique callsign from any net //
    int ID;          // Primary Key indexed
    QString Call;    // FCC / IARU call sign max 6 char indexed unique
    QString Name;
    QString City;
    QString St;
    QString Grid;        // Maidenhead Grid Square
} CallSignsRecord;

/*! \typedef struct DB_status_record_type
 * \brief Alias CallSignsRecord insures proper data types are used for database record   */
typedef struct DB_status_record_type
{
    int ID;
    QString Label;
} StatusRec;

/*! \typedef struct DB_loctype_record_type
 * \brief Alias CallSignsRecord insures proper data types are used for database record   */
typedef struct DB_loctype_record_type
{
    int ID;
    QString Label;
} LocTypeRec;

enum FieldType
{
    PlainText,
    RichText,
    BitImage,
    SvgImage
};

#endif // GLOABALS_HPP

