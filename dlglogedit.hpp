#ifndef DLGLOGEDIT_HPP
#define DLGLOGEDIT_HPP
/*! \file dlglogedit.hpp   */
#include <QtWidgets/QDialog>
#include <QtSql/QSqlQuery>

#include "globals.hpp"

#include "ui_dlglogedit.h"

namespace Ui {
class DlgLogEdit;
}

/*! \ingroup QNETLOG
 * \class DlgLogEdit
 * \brief The Database Session Log Edit dialog */
class DlgLogEdit : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(quint32 m_hdrndx READ getHdrNdx WRITE setHdrNdx)
    Q_PROPERTY(quint32 m_dtlndx READ getDtlNdx WRITE setDtlNdx)

public:
    /*!
     * \details
     * This fixed size dialog provides database maintenance functions for the
     * session log in the database. The property is set before the
     * form is displayed for valid use.
     *
     * Private Slots connected to widgets in this form include:<br />
     * <span style="color: green;">bool</span>:slotSaveLogEntry()<br />
     * \param QWidget parent
     * \sa SessionHdr
     * \sa SessionDtl
     * \sa DATABASE                           */
    explicit DlgLogEdit(QWidget *parent = 0);

    /*!
     * \brief
     * &nbsp;&nbsp;DlgLogEdit::<span style="color: green;">~DlgLogEdit()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~DlgLogEdit();

    void setHdrNdx(quint32 ndx);  //!< property method to set value into local var
    void setDtlNdx(quint32 ndx);  //!< property method to set value into local var

    quint32 getDtlNdx()  { return m_nethdrndx; } //!< Not really useful here but provided to avoid the warnings from the compiler
    quint32 getHdrNdx()  { return m_netdtlndx; } //!< Not really useful here but provided to avoid the warnings from the compiler

    void displayDtlRec();

protected:
    void changeEvent(QEvent *e);

private slots:
    void slotMARSchgd();
    void slotCallSelected();
    void slotCallChgd(QString txt);
    bool slotSaveLogEntry();

private:
    // property vars
    quint32 m_nethdrndx;          //!< local property value for session hdr index
    quint32 m_netdtlndx;          //!< local property value for session dtl index
    // local private definitions
    class dtlPrivateData;
    dtlPrivateData *dtl_data;
    // local GUI definitions
    Ui::DlgLogEdit *ui;        //!< *d-ptr to GUI form
    // private methods
    void clearForm();
    void findDtlRec();
    QSqlQuery getNetLog();
};

#endif // DLGLOGEDIT_HPP
