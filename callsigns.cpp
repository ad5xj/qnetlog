#include <QtCore/QDebug>

#include "callsigns.hpp"

CallSigns::CallSigns(QObject *parent) : QObject(parent)
{
    //
}

CallSigns::~CallSigns()
{
    //
}

bool CallSigns::del(CallSignsRecord rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    if ( rec.ID <= 0 )
    {
        qDebug() << "DB Error no record ID specified: err - " << err << " " << errmsg;
        return success;
    }

    strDML  = "DELETE FROM callsigns ";
    strDML += "WHERE id=" + QString("%1").arg(rec.ID);
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error deleting Callsigns data: err - " << err << " " << errmsg;
        qDebug() << "SQL:" << strDML;
        success = false;
    }
    return success;
}

bool CallSigns::save(CallSignsRecord rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    strDML  = "INSERT INTO callsigns ";
    strDML += "(";
    strDML += " call,";
    strDML += " operator,";
    strDML += " city,";
    strDML += " st,";
    strDML += " grid)";
    strDML += " VALUES (";
    strDML += " '" + rec.Call + "', ";
    strDML += " '" + rec.Name + "', ";
    strDML += " '" + rec.City + "', ";
    strDML += " '" + rec.St   + "', ";
    strDML += " '" + rec.Grid + "') ";
    //qDebug() << "preparing ADD SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        success = true;
    }
    else
    {
        success = false;
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error saving CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return success;
}

bool CallSigns::update(CallSignsRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    strDML  = "UPDATE callsigns SET ";
    strDML += "call=";
    strDML += " '" + rec.Call + "', ";
    strDML += "operator=";
    strDML += " '" + rec.Name + "', ";
    strDML += "city=";
    strDML += " '" + rec.City + "', ";
    strDML += "st=";
    strDML += " '" + rec.St + "', ";
    strDML += "grid=";
    strDML += "'" + rec.Grid + "' ";
    strDML += " WHERE id = ";
    strDML += QString("%1").arg(rec.ID) + "; ";
    //qDebug() <<  "preparing ADD SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
        return false;
    }
    return true;
}

bool CallSigns::findCallSignsRecord(CallSignsRecord rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    strDML  = "SELECT DISTINCT id,";
    strDML += " call,";
    strDML += " name,";
    strDML += " city,";
    strDML += " st,";
    strDML += " grid";
    strDML += " FROM CallSigns ";
    strDML += " WHERE id = ";
    strDML += QString("%1").arg(rec.ID);
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    //qDebug() << "DB Error executing query: err-" << err << " " << errmsg;
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        success = true;
    }
    else
    {
        success = false;
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    qry.first();
    if ( qry.isValid() )
        success = true;
    else
        success = false;

    return success;
}

bool CallSigns::findACallSign(QString strCall)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    if ( strCall == "" ) return false; // we don't need all callsigns so return

    strDML  = "SELECT id,";
    strDML += " call, operator, city, st, grid, notes";
    strDML += " FROM callsigns ";
    strDML += " WHERE call=";
    strDML += "'" + strCall.trimmed().toUpper() + "%' ";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    qry.first();
    if ( qry.isValid() ) return true;
    return false;
}

QSqlQuery CallSigns::findCallSigns(QString strCall)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    if ( strCall == "" ) return qry; // we don't need all callsigns so return

    strDML  = "SELECT id,";
    strDML += " call CALLSIGN, operator OPERATOR, city CITY, st ST, grid GRID, notes NOTES";
    strDML += " FROM callsigns ";
    strDML += " WHERE call LIKE ";
    strDML += "'" + strCall + "%' ";
    strDML += " ORDER BY call";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery CallSigns::findAll()
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT id, call, operator, city, st, grid, marscall, notes";
    strDML += " FROM callsigns ORDER BY call";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery CallSigns::findID(quint32 id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT id, call, operator, city, st, grid, marscall, notes";
    strDML += " FROM callsigns where id = ";
    strDML += QString("%1").arg(id);
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "DB Error updating CallSigns data: err " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}
