#ifndef YESNODELEGATE_HPP
#define YESNODELEGATE_HPP
/*! \file yesnodelegate.hpp  */
#include <QtCore/QObject>
#include <QtCore/QModelIndex>
#include <QtCore/QAbstractItemModel>
#include <QtWidgets/QStyledItemDelegate>
#include <QtWidgets/QStyleOptionViewItem>
#include <QtWidgets/QAbstractItemDelegate>

/*! \ingroup QNETLOG
 * \brief The YesNoDelegate class
 * This delegate class aids in editing providing a yes / no choice in some fields */
class YesNoDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    YesNoDelegate(QWidget *parent=0);
    virtual ~YesNoDelegate();

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const Q_DECL_OVERRIDE;

    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const Q_DECL_OVERRIDE;
    void setEditorData(QWidget *editor, const QModelIndex &index) const Q_DECL_OVERRIDE;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;

private:
};


#endif // YESNODELEGATE_HPP

