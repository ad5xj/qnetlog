#include <QtCore/QDebug>
#include <QtSql/QSqlError>

#include "bands.hpp"

Bands::Bands(QObject *parent) : QObject(parent)
{
    // Bands database maintenance implementation
}

Bands::~Bands()
{
    //
}

bool Bands::del(BandsRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    if ( rec.id <= 0 )
    {
        qDebug() << "DB Error no record ID specified: err - " << err << " " << errmsg;
        return success;
    }

    sqlStr  = "DELETE FROM bands ";
    sqlStr += "WHERE id=" + QString("%1").arg(rec.id) + ", ');";
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error deleting Bands data: err - " << err << " " << errmsg;
        success = false;
    }
    return success;
}

bool Bands::save(BandsRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    sqlStr  = "INSERT INTO bands ( ndx, Label ) ";
    sqlStr += "VALUES (" + QString("%1").arg(rec.ndx) + ", ');";
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error saving Bands data: err - " << err << " " << errmsg;
        success = false;
    }
    return success;
}

bool Bands::update(BandsRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr = "";

    sqlStr  = "UPDATE bands SET ";
    sqlStr += "ndx=" + QString("%1").arg(rec.ndx) + ", ";
    sqlStr += "Label='" + rec.Label + "' ";
    sqlStr += " WHERE id = ";
    sqlStr += QString("%1").arg(rec.id) + "; ";
    //qDebug() <<  "preparing UPDT SQL:" << sqlStr;
    qry.prepare(sqlStr);

    //qDebug() << "executing...";
    qry.exec();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        success = true;
    }
    else
    {
        success = false;
        err = qry.lastError().number();
        errmsg = qry.lastError().text();
        qDebug() << "UPDATE query err: " << QString("%1").arg(err) << " - " << errmsg;
    }
    return success;
}

QSqlQuery Bands::findBandsRecord(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString   sqlStr;

    sqlStr  = "SELECT (id, ndx, Label) FROM bands WHERE id = " + QString("%1").arg(id);
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if ( qry.isValid() )
    {
        //qDebug() << "Specific net query success ";
    }
    else
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "Specific net query invalid " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery Bands::findBandsID(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    //qDebug() << "selecting records from table Bands...";
    sqlStr  = "SELECT (id, ndx, Label) FROM bands WHERE id = " + QString("%1").arg(id);
    sqlStr += " ORDER BY ndx";
    qry.prepare(sqlStr);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}

QSqlQuery Bands::findBands()
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    sqlStr  = "SELECT id, ndx, Label FROM bands ORDER BY ndx";
    qry.prepare(sqlStr);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}

