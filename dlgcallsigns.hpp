#ifndef DLGCALLSIGNS_HPP
#define DLGCALLSIGNS_HPP
/*! \file dlgcallsigns.hpp  */
#include <QtCore/QObject>
#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QShowEvent>
#include <QtWidgets/QDialog>

#include "globals.hpp"
#include "ui_dlgcallsigns.h"

namespace Ui {
class dlgCallSigns;
}

/*! \ingroup QNETLOG
 * \class dlgCallSigns
 * \details
 * This dialog provides database maintenance functions for the Callsigns table
 * in the database.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotAddRec()<br />
 * <span style="color: green;">void</span>:slotUpdtRecRec()<br />
 * <span style="color: green;">void</span>:slotRowSel()<br />
 * <span style="color: green;">void</span>:slotCancel()<br />
 * <span style="color: green;">void</span>:slotReset()<br />
 * <span style="color: green;">void</span>:slotExit()<br />        */
class dlgCallSigns : public QDialog
{
    Q_OBJECT

public:
    /*!
     * \brief The CallSigns Database Table Maintenance Dialog */
    explicit dlgCallSigns(QWidget *parent = 0);
    virtual ~dlgCallSigns();

protected:
    void changeEvent(QEvent *e);                //!<
    void closeEvent(QCloseEvent *);             //!<
    void resizeEvent(QResizeEvent *);           //!<
    void showEvent(QShowEvent *);               //!<

private slots:
    void slotAddRec();                          //!<
    void slotUpdtRec();                         //!<
    void slotDelRec();                          //!<
    void slotRowSel();                          //!<
    void slotCancel();                          //!<
    void slotReset();                           //!<
    void slotExit();                            //!<

private:
    /*! \class cmPrivateData
     * \brief Local definition of private data  */
    class cmPrivateData;
    cmPrivateData *cm_data;                     //!< *d-ptr to local private data values

    Ui::dlgCallSigns *ui;                       //!< *d-ptr to form definition

    void clearForm();                           //!<
    void resetButtons();                        //!<
    void loadCallsignsTable();                  //!<
    void createObjects();                       //!<
    void connectSlots();                        //!<
    void resizeTblCols();                       //!<
    void saveDataFromForm();                    //!<
    void updtDataFromForm();                    //!<
    void saveColSizes();                        //!<
    void displaySettings();                     //!<
    void readSettings();                        //!<
    void saveSettings();                        //!<
};

#endif // DLGCALLSIGNS_HPP
