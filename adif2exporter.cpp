/*************************************************************************
 *  Impementation of the ADIF 2 Specific XML Exporter                    *
 *  interface adifexporter.hpp                                             *
 *               -------------------                                     *
 *  begin                : JUNE 2014                                      *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 *  GNU General Public License for more details.                         *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#include <QtCore/QFile>
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QTableView>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

#include "tablehdrs.hpp"
#include "adif2exporter.hpp"

ADIF2Exporter::ADIF2Exporter(QWidget *parent) : QObject(parent)
{
    //
}

void ADIF2Exporter::processADIF2Export(QString fil, QStringList indata)
{
    if ( indata.count() == 0 ) return;
    if ( fil.length()   == 0 ) return;
    if ( fil.right(4) != ".adi" ) fil = fil + ".adi";

    QStringList TblHdrs;
    QStringList TblFields;
    QStringList months;       //  string list of adif data split on CRLF
    months << ""    << "Jan" << "Feb" << "Mar" << "Apr"
           << "May" << "Jun" << "Jul" << "Aug" << "Sep"
           << "Oct" << "Nov" << "Dec";

    // get the file to parse
    QFile xfile(fil);
    if (!xfile.open(QFile::ReadWrite | QFile::Text))
    {
        QMessageBox mb;
        mb.setWindowTitle(QApplication::tr("QNetLogger 1073"));
        mb.setText(QApplication::tr("Cannot read file %1:\n%2.").arg(fil).arg(xfile.errorString()));
        mb.exec();
        return;
    }

    // create the table headers db object
    TableHdrs hdrs;    // this is the db table not string list as above
    // get the list of fields in the contacts table
    QSqlQuery qry = hdrs.readHeadersSeq();
    qry.first();
    do
    {

        TblHdrs.append(qry.value("hdrtxt").toString());
        QString fldname = qry.value("adifnam").toString();
        TblFields.append(fldname);
        qry.next();
    } while ( qry.isValid() );
    qry.finish();

    // format header ending in <eoh>
    QByteArray outrow = "";
    QByteArray a = APP_NAME;
    QByteArray b = APP_VERSION;
    QByteArray c = a;
               c += " ";
               c += b;
    outrow += "ADIF 2 Export from ";
    outrow += c;
    outrow += "\n";
    xfile.write(outrow);
    int mo, da, yr;
    QString utc = "local";
    da = QDateTime::currentDateTime().date().day();
    mo = QDateTime::currentDateTime().date().month();
    yr = QDateTime::currentDateTime().date().year();
    //if ( main.b_utc ) utc = "UTC";
    outrow  = "Logs generated on ";
    outrow += QString("%1").arg(da);
    outrow += " ";
    outrow += months[mo];
    outrow += " ";
    outrow += QString("%1").arg(yr);
    outrow += "\n";
    xfile.write(outrow,outrow.length());
    outrow = "File output is NOT restricted to only QSOs with : All operators on All Bands - All Modes\n";
    xfile.write(outrow,outrow.length());
    outrow  = "<programid:10>QNetLogger\n";
    xfile.write(outrow,outrow.length());
    outrow  = "<programversion:>";
    outrow += APP_VERSION;
    outrow += "\n";
    xfile.write(outrow,outrow.length());
    outrow = "<eoh>";
    outrow += "\n\n";
    xfile.write(outrow,outrow.length());
    // done with file header //

    // field order is customized by user so must be collected from database
    // use seq no in db and adif name to create strings from here


    int x = 0;
    for ( x = 0; x < indata.count(); ++x )
    { // for every row coming in
        QString dataitem = indata[x];
        QStringList fields = dataitem.split("|",QString::KeepEmptyParts);
        for ( int y = 0; y < fields.count(); ++y )
        {  // for each field in sequence
            // use adif_name to create outrow data
            outrow += "<";
            outrow += TblFields[y];
            outrow += ">";
            outrow += fields[y];
            outrow += " ";
        }
        // all fields formatted
        // so put the end of record marker
        outrow += "<eor>\n\n";
        // and write it to the file
        xfile.write(outrow,outrow.length());
        QString msg = "Exporting ";
        msg += QString("%1").arg(x);
        msg += " Records";
        emit signalUpdateStatusMsg(msg);
        outrow = "";
    }
    xfile.close();
    QString msg = QString("%1").arg(x);
    msg += " Records exported...";
    emit signalUpdateStatusMsg(msg);
}
