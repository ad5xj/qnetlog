#include <QtCore/QPoint>
#include <QtCore/QSize>
#include <QtGui/QFont>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QSplitter>
#include <QtSql>

#include "globals.hpp"
#include "nets.hpp"
#include "dlgnetsmaint.hpp"

/* Each define represents a single bit */
#define DAY_SUN (1 << 0)  // 0000001
#define DAY_SAT (1 << 1)  // 0000010
#define DAY_FRI (1 << 2)  // 0000100
#define DAY_THU (1 << 3)  // 0001000
#define DAY_WED (1 << 4)  // 0010000
#define DAY_TUE (1 << 5)  // 0100000
#define DAY_MON (1 << 6)  // 1000000
#define FORM_TITLE QApplication::tr("Nets Database Maintenance")

class dlgNetsMaint::nmPrivateData
{
public:
    // local vars and objects go here
    bool formLoaded;
    bool needsSaving;
    bool rowselected;
    bool m_utc;
    bool AddMode;
    bool ChgMode;
    bool DelMode;
    bool font_bold;
    bool font_underline;
    bool font_italic;

    quint16 font_size;
    quint16 color_r;
    quint16 color_g;
    quint16 color_b;
    quint16 color_a;

    QFont font;
    QFont altFont;

    QColor altColor;

    QPoint pos;
    QSize  size;

    QString colwidths;
    QString colhdrs;
    QString font_family;

    QByteArray splt;
    QByteArray wgeo;

    QSqlTableModel *dbNetsModel;
};

dlgNetsMaint::dlgNetsMaint(QWidget *parent) : QDialog(parent)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    nm_data = new nmPrivateData;
    nm_data->formLoaded = false;

    ui = new Ui::dlgNetsMaint;
    ui->setupUi(this);
    ui->retranslateUi(this);

    setModal(true);

    createObjects();

    readSettings();

    displaySettings();

    createConnections();

    ui->tblViewNets->clearContents();
    resizeTblCols();
    loadNetsList();

    clearForm();
    resetButtons();

    ui->editNetName->setFocus();
    nm_data->formLoaded = true;
    nm_data->rowselected = false;
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

dlgNetsMaint::~dlgNetsMaint()
{
    delete ui;
    delete nm_data;
}

void dlgNetsMaint::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch ( e->type() )
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void dlgNetsMaint::showEvent(QShowEvent *)
{
    restoreGeometry(nm_data->wgeo);
    if ( width() < 100 ) resize(650,545);
}

void dlgNetsMaint::moveEvent(QMoveEvent *)
{
    nm_data->wgeo = saveGeometry();
}

void dlgNetsMaint::resizeEvent(QResizeEvent *)
{
    ui->splitWindow->setGeometry(3,3,width() - 6, height() - 30);
    nm_data->splt = ui->splitWindow->saveGeometry();
    nm_data->wgeo = saveGeometry();
}

void dlgNetsMaint::closeEvent(QCloseEvent *)
{
    nm_data->splt = ui->splitWindow->saveGeometry();
    nm_data->wgeo = saveGeometry();
    saveColWidths();
    saveSettings();
}

void dlgNetsMaint::slotAddNet()
{
    if ( !nm_data->AddMode )
    {
        QString t = FORM_TITLE + tr(" - ADD MODE");
        this->setWindowTitle(t);
        ui->editNetID->setEnabled(true);
        ui->editNetTime->setEnabled(true);
        ui->editNetFreq->setEnabled(true);
        ui->editNetName->setEnabled(true);
        ui->editNetNotes->setEnabled(true);


        ui->editNetID->setText("0");
        ui->editNetFreq->setText("");
        ui->editNetName->setText("");
        ui->editNetNotes->setText("");
        ui->editNetTime->setTime(QTime::currentTime());
        ui->btnUTC->setChecked(true);
        ui->btnLSB->setChecked(false);
        ui->btnUSB->setChecked(true);
        ui->chkSun->setChecked(false);
        ui->chkSat->setChecked(true);
        ui->chkFri->setChecked(false);
        ui->chkThu->setChecked(false);
        ui->chkWed->setChecked(false);
        ui->chkTue->setChecked(false);
        ui->chkMon->setChecked(false);
        ui->editNetNotes->setText("");
        enableForm(true);
        // disable buttons until done
        ui->btnAddNet->setText(tr("SAVE"));
        ui->btnAddNet->setEnabled(false);
        ui->btnDelNet->setEnabled(false);
        ui->btnUpdtNet->setEnabled(false);
        ui->btnExit->setEnabled(false);
        ui->btnCancel->setEnabled(true);
        nm_data->AddMode = true;
    }
    else
    {
        saveNetRec(); // save the data from the form
        clearForm();
        ui->tblViewNets->clear();
        ui->tblViewNets->setRowCount(0);
        loadNetsList();
        nm_data->AddMode = false;
    }
}

void dlgNetsMaint::slotChgNet()
{
    if ( nm_data->ChgMode )
    {
        nm_data->ChgMode = false;

        updtNetRec();
        clearForm();
        resetButtons();

        ui->tblViewNets->clearContents();
        resizeTblCols();
        loadNetsList();
        QString t = FORM_TITLE + " " + APP_VERSION;
        this->setWindowTitle(t);
    }
    else
    {
        QString t = FORM_TITLE + tr(" - EDIT MODE ");
        this->setWindowTitle(t);
        enableForm(true);
        // disable buttons until done
        ui->btnUpdtNet->setText(tr("SAVE"));
        ui->btnAddNet->setEnabled(false);
        ui->btnUpdtNet->setEnabled(true);
        ui->btnDelNet->setEnabled(false);
        ui->btnCancel->setEnabled(true);
        ui->btnReset->setEnabled(true);
        ui->btnExit->setEnabled(false);
        nm_data->ChgMode = true;
    }
}

void dlgNetsMaint::slotDelNet()
{
    if ( !nm_data->DelMode )
    {
        QString t = FORM_TITLE + tr(" - CAUTION DELETING! ! !");
        this->setWindowTitle(t);
        // disable buttons until done
        ui->btnDelNet->setEnabled(false);
        ui->btnUpdtNet->setEnabled(true);
        ui->btnExit->setEnabled(false);
        ui->btnCancel->setVisible(true);
        ui->btnUpdtNet->setText(tr("COMMIT"));
        nm_data->DelMode = true;
    }
    else
    {
        nm_data->DelMode = false;

        QMessageBox msgBox;
        msgBox.setText("You are about to permanently delete this record.");
        msgBox.setInformativeText("Is this what you really want to do?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::No);
        int ret = msgBox.exec();

        switch ( ret  )
        {
        case QMessageBox::Yes:
        {
            Nets nets;
            NetsRecord rec;

            rec.id = ui->editNetID->text().toInt();
            nets.del(rec); // save the data from the form
        }
        case QMessageBox::No:
            break;
        case QMessageBox::Cancel:
            slotReset();
            break;
        }
        clearForm();
    }
}

void dlgNetsMaint::slotCancel()
{
    this->setWindowTitle(FORM_TITLE);
    nm_data->ChgMode = false;
    nm_data->AddMode = false;
    nm_data->DelMode = false;
    enableForm(false);
    resetButtons();
}

void dlgNetsMaint::slotReset()
{
    nm_data->AddMode = false;
    nm_data->ChgMode = false;
    nm_data->DelMode = false;
    nm_data->rowselected = false;

    ui->tblViewNets->clear();
    ui->tblViewNets->setRowCount(0);
    loadNetsList();

    clearForm();
    resetButtons();

    this->setWindowTitle(FORM_TITLE);
}

void dlgNetsMaint::slotRowSel()
{
    // get which row is selected
    int x = ui->tblViewNets->currentRow();
    if ( x < 0 ) return;  // nothing selected so return
    // display fields and set buttons to edit mode
    ui->editNetID->setText(ui->tblViewNets->item(x,0)->text());
    ui->editNetName->setText(ui->tblViewNets->item(x,1)->text());
    ui->editNetFreq->setText(ui->tblViewNets->item(x,2)->text());
    QString m = ui->tblViewNets->item(x,3)->text();
    if ( m == "LSB" )
        ui->btnLSB->setChecked(true);
    else if ( m == "USB" )
        ui->btnLSB->setChecked(true);
    else if ( m == "Digital" )
        ui->btnDigital->setChecked(true);
    else if ( m == "AM" )
        ui->btnAM->setChecked(true);
    else if ( m == "FM" )
        ui->btnFM->setChecked(true);
    else
        ui->btnUSB->setChecked(true);
    int j = 0;
    for ( j = 0; j < ui->cboBand->count(); ++j )
    {
        if ( ui->tblViewNets->item(x,4)->text() == ui->cboBand->itemText(j) )break;
    }
    ui->cboBand->setCurrentIndex(j);
    QString tstr = ui->tblViewNets->item(x,5)->text();
    QTime newtime = QTime::fromString(tstr,"hh:mm AP");
    ui->editNetTime->setTime(newtime);
    if ( ui->tblViewNets->item(x,6)->text() == "UTC" )
        ui->btnUTC->setChecked(true);
    else if ( ui->tblViewNets->item(x,6)->text() == "Local" )
        ui->btnLocal->setChecked(true);
    else
        ui->btnLocal->setChecked(true);
    ui->editNetNotes->setText(ui->tblViewNets->item(x,7)->text());
    QString occrs = ui->tblViewNets->item(x,8)->text();
    ui->btnSessionDly->setChecked(false);
    ui->btnSessionWkDays->setChecked(false);
    ui->btnSessionWkly->setChecked(true);
    ui->btnSessionBiWkly->setChecked(false);
    ui->btnMthly->setChecked(false);
    if ( occrs == "Daily" )
        ui->btnSessionDly->setChecked(true);
    else if ( occrs == "Weekdays" )
        ui->btnSessionWkDays->setChecked(true);
    else if ( occrs == "Weekly" )
        ui->btnSessionWkly->setChecked(true);
    else if ( occrs == "Bi-Weekly" )
        ui->btnSessionBiWkly->setChecked(true);
    else if ( occrs == "Monthly" )
        ui->btnMthly->setChecked(true);
    else
        ui->btnSessionWkly->setChecked(true);

    QStringList days = ui->tblViewNets->item(x,9)->text().split(" ");
    ui->chkSun->setChecked(false);
    ui->chkMon->setChecked(false);
    ui->chkTue->setChecked(false);
    ui->chkWed->setChecked(false);
    ui->chkThu->setChecked(false);
    ui->chkFri->setChecked(false);
    ui->chkSat->setChecked(false);
    for ( int k = 0; k < days.count(); ++k )
    {
        if ( days.at(k) == "" ) break;
        if ( days.at(k) == "Su" ) ui->chkSun->setChecked(true);
        if ( days.at(k) == "M" ) ui->chkMon->setChecked(true);
        if ( days.at(k) == "Tu" ) ui->chkTue->setChecked(true);
        if ( days.at(k) == "W" ) ui->chkWed->setChecked(true);
        if ( days.at(k) == "Th" ) ui->chkThu->setChecked(true);
        if ( days.at(k) == "F" ) ui->chkFri->setChecked(true);
        if ( days.at(k) == "Sa" ) ui->chkSat->setChecked(true);
    }
    ui->btnAddNet->setEnabled(false);
    ui->btnUpdtNet->setEnabled(true);
    ui->btnDelNet->setEnabled(true);
    ui->btnCancel->setEnabled(false);
    ui->btnReset->setEnabled(true);
    ui->btnExit->setEnabled(true);

    nm_data->rowselected = true;
}

void dlgNetsMaint::saveNetRec()
{
    if ( !nm_data->AddMode ) return;
    quint8 daybits = 0x0;
    quint32 err = 0;
    Nets nets;
    NetsRecord rec;

//    qDebug() << "Saving Net Record from data entered on form";

    rec.id = 0;
    rec.NetName = ui->editNetName->text();
    rec.NetBand = ui->cboBand->currentIndex();

    if ( ui->btnUTC->isChecked() )
        rec.TimeBase = 1;
    else if ( ui->btnLocal->isChecked() )
        rec.TimeBase = 2;

    rec.NetStTime = ui->editNetTime->time();

    if ( ui->btnSessionDly->isChecked() )
        rec.Occurs = 1;
    else if ( ui->btnSessionWkDays->isChecked() )
        rec.Occurs = 2;
    else if ( ui->btnSessionWkly->isChecked() )
        rec.Occurs = 3;
    else if ( ui->btnSessionBiWkly->isChecked() )
        rec.Occurs = 4;
    else if ( ui->btnMthly->isChecked() )
        rec.Occurs = 5;
    else
        rec.Occurs = 3;  // default to Weekly if not one of the above
    if ( ui->btnSessionDly->isChecked() )
        daybits = 0x1111111;
    else
    {
        if ( ui->chkSun->isChecked()) daybits |= DAY_SUN;
        if ( ui->chkMon->isChecked()) daybits |= DAY_MON;
        if ( ui->chkTue->isChecked()) daybits |= DAY_TUE;
        if ( ui->chkWed->isChecked()) daybits |= DAY_WED;
        if ( ui->chkThu->isChecked()) daybits |= DAY_THU;
        if ( ui->chkFri->isChecked()) daybits |= DAY_FRI;
        if ( ui->chkSat->isChecked()) daybits |= DAY_SAT;
    }
    rec.DayOfWeek = daybits;
    if ( ui->btnLSB->isChecked())
        rec.NetMode = 1;
    else if ( ui->btnUSB->isChecked())
        rec.NetMode = 2;
    else if ( ui->btnDigital->isChecked())
        rec.NetMode = 3;
    else if ( ui->btnFM->isChecked())
        rec.NetMode = 4;
    else if ( ui->btnAM->isChecked())
        rec.NetMode = 5;
    rec.NetFrequency =  ui->editNetFreq->text();
    rec.NetNotes =  ui->editNetNotes->document()->toPlainText();

    if ( nets.save(rec) )
    {
         ui->btnAddNet->setEnabled(true);
         ui->btnDelNet->setEnabled(false);
         ui->btnUpdtNet->setEnabled(false);
         ui->btnExit->setEnabled(true);
         clearForm();
    }
    else
    {
         ui->btnAddNet->setEnabled(true);
         ui->btnDelNet->setEnabled(false);
         ui->btnUpdtNet->setEnabled(false);
         ui->btnExit->setEnabled(true);
         err = nets.getLastErr();
         QString msg = nets.getLastErrMsg();
         qDebug() << QString("Database Error %1 :").arg(err) << msg;
         QMessageBox msgBox;
         msgBox.setText("Database Error" + msg);
         msgBox.exec();
    }
}

void dlgNetsMaint::updtNetRec()
{
    if ( !nm_data->ChgMode ) return;

    quint8 daybits = 0x0;
    quint32 err = 0;
    Nets nets;
    NetsRecord rec;

    rec.id = ui->editNetID->text().toInt();
    rec.NetName = ui->editNetName->text();
    rec.NetBand = ui->cboBand->currentIndex();

    if ( ui->btnUTC->isChecked() )
        rec.TimeBase = 1;
    else if ( ui->btnLocal->isChecked() )
        rec.TimeBase = 2;

    rec.NetStTime = ui->editNetTime->time();

    if ( ui->btnSessionDly->isChecked() )
        rec.Occurs = 1;
    else if ( ui->btnSessionWkDays->isChecked() )
        rec.Occurs = 2;
    else if ( ui->btnSessionWkly->isChecked() )
        rec.Occurs = 3;
    else if ( ui->btnSessionBiWkly->isChecked() )
        rec.Occurs = 4;
    else if ( ui->btnMthly->isChecked() )
        rec.Occurs = 5;
    else
        rec.Occurs = 3;  // default to Weekly if not one of the above
    if ( ui->btnSessionDly->isChecked() )
        daybits = 0x1111111;
    else
    {
        if ( ui->chkSun->isChecked()) daybits |= DAY_SUN;
        if ( ui->chkMon->isChecked()) daybits |= DAY_MON;
        if ( ui->chkTue->isChecked()) daybits |= DAY_TUE;
        if ( ui->chkWed->isChecked()) daybits |= DAY_WED;
        if ( ui->chkThu->isChecked()) daybits |= DAY_THU;
        if ( ui->chkFri->isChecked()) daybits |= DAY_FRI;
        if ( ui->chkSat->isChecked()) daybits |= DAY_SAT;
    }
    rec.DayOfWeek = daybits;
    if ( ui->btnLSB->isChecked())
        rec.NetMode = 1;
    else if ( ui->btnUSB->isChecked())
        rec.NetMode = 2;
    else if ( ui->btnDigital->isChecked())
        rec.NetMode = 3;
    if ( ui->btnFM->isChecked())
        rec.NetMode = 4;
    else if ( ui->btnAM->isChecked())
        rec.NetMode = 5;
    rec.NetFrequency =  ui->editNetFreq->text();
    rec.NetNotes =  ui->editNetNotes->document()->toPlainText();

    if ( nets.update(rec) )
    {
         ui->btnAddNet->setEnabled(true);
         ui->btnDelNet->setEnabled(false);
         ui->btnUpdtNet->setEnabled(false);
         ui->btnUpdtNet->setText(tr("Update"));
         ui->btnExit->setEnabled(true);
         slotCancel();
         slotReset();
    }
    else
    {
         ui->btnAddNet->setEnabled(true);
         ui->btnDelNet->setEnabled(false);
         ui->btnUpdtNet->setEnabled(false);
         ui->btnUpdtNet->setText(tr("Update"));
         ui->btnExit->setEnabled(true);
         err = nets.getLastErr();
         QString msg = nets.getLastErrMsg();
         if ( err != QSqlError::NoError )
         {
             qDebug() << QString("Database Error on Update - %1 :").arg(err) << msg;
             QMessageBox msgBox;
             msgBox.setText("Database Error" + msg);
             msgBox.exec();
             return;
         }
         slotCancel();
         clearForm();
    }
}

void dlgNetsMaint::enableForm(bool b)
{
    ui->cboBand->setCurrentIndex(0);

    ui->editNetID->setEnabled(b);
    ui->editNetTime->setEnabled(b);
    ui->editNetFreq->setEnabled(b);
    ui->editNetName->setEnabled(b);
    ui->editNetNotes->setEnabled(b);
    ui->cboBand->setEnabled(b);
    ui->btnUTC->setEnabled(b);
    ui->btnLocal->setEnabled(b);
    ui->btnDigital->setEnabled(b);
    ui->btnAM->setEnabled(true);
    ui->btnFM->setEnabled(true);
    ui->btnLSB->setEnabled(b);
    ui->btnUSB->setEnabled(b);
    ui->btnSessionDly->setEnabled(b);
    ui->btnSessionWkDays->setEnabled(b);
    ui->btnSessionWkly->setEnabled(b);
    ui->btnSessionBiWkly->setEnabled(b);
    ui->btnMthly->setEnabled(b);
    ui->chkSun->setEnabled(b);
    ui->chkSat->setEnabled(b);
    ui->chkFri->setEnabled(b);
    ui->chkThu->setEnabled(b);
    ui->chkWed->setEnabled(b);
    ui->chkTue->setEnabled(b);
    ui->chkMon->setEnabled(b);

    ui->cboBand->setEnabled(b);
    ui->btnUTC->setEnabled(b);
    ui->btnLocal->setEnabled(b);
    ui->btnDigital->setEnabled(b);
    ui->btnAM->setEnabled(b);
    ui->btnFM->setEnabled(b);
    ui->btnLSB->setEnabled(b);
    ui->btnUSB->setEnabled(b);
}

void dlgNetsMaint::clearForm()
{
    ui->editNetID->setText("0");
    ui->editNetFreq->setText("");
    ui->editNetName->setText("");
    ui->editNetNotes->setText("");
    ui->editNetTime->setTime(QTime::currentTime());
    ui->cboBand->setCurrentIndex(0);
    enableForm(false);
    setWindowTitle(FORM_TITLE);
}

void dlgNetsMaint::resetButtons()
{
    // clear the check boxes
    ui->chkSun->setChecked(false);
    ui->chkSat->setChecked(true);
    ui->chkFri->setChecked(false);
    ui->chkThu->setChecked(false);
    ui->chkWed->setChecked(false);
    ui->chkTue->setChecked(false);
    ui->chkMon->setChecked(false);
    // set the time base
    ui->btnUTC->setChecked(true);
    ui->btnUSB->setChecked(true);
    // change button text
    ui->btnAddNet->setText(tr("Add Net"));
    ui->btnUpdtNet->setText(tr("Update"));
    // reset button states
    ui->btnAddNet->setEnabled(true);
    ui->btnUpdtNet->setEnabled(false);
    ui->btnDelNet->setEnabled(false);
    ui->btnCancel->setEnabled(false);
    ui->btnReset->setEnabled(false);
    ui->btnExit->setEnabled(true);
}

void dlgNetsMaint::loadNetsList()
{
    Nets nets;
    QSqlQuery qry;

    qry =  nets.findNets();   // generalize query of all nets in order by name for display

    // check for valid query
    qry.first();
    if ( !qry.isValid() ) { return; }

    // we have results to show
    int r = 0;
    QTableWidgetItem *idItem;
    QTableWidgetItem *nameItem;
    QTableWidgetItem *freqItem;
    QTableWidgetItem *modeItem;
    QTableWidgetItem *bandItem;
    QTableWidgetItem *sttimeItem;
    QTableWidgetItem *baseItem;
    QTableWidgetItem *notesItem;
    QTableWidgetItem *occrItem;
    QTableWidgetItem *daysItem;

    // now load the display with results - store first item in table
    ui->tblViewNets->insertRow(r);

    // fill in blanks in new row
    int id = qry.record().field("id").value().toInt();
    idItem   = new QTableWidgetItem(QString("%1").arg(id));
    nameItem = new QTableWidgetItem(qry.record().field("netname").value().toString());
    freqItem = new QTableWidgetItem(qry.record().field("netfreq").value().toString());
    int modeidx = qry.record().field("netmode").value().toInt();
    QString s1 = "";
    switch ( modeidx )
    {
    case 1:
        s1 = "LSB";
        break;
    case 2:
        s1 = "USB";
        break;
    case 3:
        s1 = "Digital";
        break;
    case 4:
        s1 = "FM";
        break;
    case 5:
        s1 = "AM";
        break;
    }
    modeItem = new QTableWidgetItem(s1);
    int x = qry.record().field("netband").value().toInt();
    QString s = ui->cboBand->itemText(x);
    bandItem = new QTableWidgetItem(s);
    sttimeItem = new QTableWidgetItem(qry.value(5).toString());
    int tmbase = qry.record().field("timebase").value().toInt();
    QString t1 = "";
    switch ( tmbase )
    {
    case 1:
        t1 = "UTC";
        break;
    case 2:
        t1 = "Local";
        break;
    default:
        t1 = "Local";
        break;
    }
    baseItem = new QTableWidgetItem(t1);

    notesItem = new QTableWidgetItem(qry.record().field("netnotes").value().toString());
    int occ = qry.record().field("occurs").value().toInt();
    QString s2 = "";
    switch ( occ )
    {
    case 1:
        s2 = "Daily";
        break;
    case 2:
        s2 = "Weekdays";
        break;
    case 3:
        s2 = "Weekly";
        break;
    case 4:
        s2 = "Bi-Weekly";
        break;
    case 5:
        s2 = "Monthly";
        break;
    }
    occrItem = new QTableWidgetItem(s2);

    quint16 daybits = qry.record().field("dayofweek").value().toInt();
    QString s3 = "";
    if ( !(daybits ^ DAY_SUN) ) s3 += "Su ";
    if ( !(daybits ^ DAY_MON) ) s3 += "M ";
    if ( !(daybits ^ DAY_TUE) ) s3 += "T ";
    if ( !(daybits ^ DAY_WED) ) s3 += "W ";
    if ( !(daybits ^ DAY_THU) ) s3 += "Th ";
    if ( !(daybits ^ DAY_FRI) ) s3 += "F ";
    if ( !(daybits ^ DAY_SAT) ) s3 += "Sa ";
    daysItem = new QTableWidgetItem(s3);

    idItem->setTextAlignment(Qt::AlignHCenter);
    baseItem->setTextAlignment(Qt::AlignHCenter);

    ui->tblViewNets->setItem(r,0,idItem);
    ui->tblViewNets->setItem(r,1,nameItem);
    ui->tblViewNets->setItem(r,2,freqItem);
    ui->tblViewNets->setItem(r,3,modeItem);
    ui->tblViewNets->setItem(r,4,bandItem);
    ui->tblViewNets->setItem(r,5,sttimeItem);
    ui->tblViewNets->setItem(r,6,baseItem);
    ui->tblViewNets->setItem(r,7,notesItem);
    ui->tblViewNets->setItem(r,8,occrItem);
    ui->tblViewNets->setItem(r,9,daysItem);
    ui->tblViewNets->setRowHeight(r,28);

    qry.next();

    do
    {
        ++r;

        ui->tblViewNets->insertRow(r);
        ui->tblViewNets->setRowHeight(r,28);
        // fill in blanks in new row
        int id = qry.record().field("id").value().toInt();
        idItem   = new QTableWidgetItem(QString("%1").arg(id),0);
        idItem->setTextAlignment(Qt::AlignHCenter);
        nameItem = new QTableWidgetItem(qry.record().field("netname").value().toString(),0);
        freqItem = new QTableWidgetItem(qry.record().field("netfreq").value().toString(),0);
        int modeidx = qry.record().field("netmode").value().toInt();
        switch ( modeidx )
        {
        case 1:
            s1 = "LSB";
            break;
        case 2:
            s1 = "USB";
            break;

        case 3:
            s1 = "Digital";
            break;
        case 4:
            s1 = "FM";
            break;
        case 5:
            s1 = "AM";
            break;
        }
        modeItem = new QTableWidgetItem(s1,0);
        x = qry.record().field("netband").value().toInt();
        s = ui->cboBand->itemText(x);
        bandItem = new QTableWidgetItem(s,0);
        sttimeItem = new QTableWidgetItem(qry.value(5).toString(),0);
        int tmbase = qry.record().field("timebase").value().toInt();
        switch ( tmbase )
        {
        case 1:
            t1 = "UTC";
            break;
        case 2:
            t1 = "Local";
            break;
        default:
            t1 = "Local";
            break;
        }
        baseItem = new QTableWidgetItem(t1,0);
        baseItem->setTextAlignment(Qt::AlignHCenter);

        notesItem = new QTableWidgetItem(qry.record().field("netnotes").value().toString(),0);
        int occ = qry.record().field("occurs").value().toInt();
        switch ( occ )
        {
        case 1:
            s2 = "Daily";
            break;
        case 2:
            s2 = "Weekdays";
            break;
        case 3:
            s2 = "Weekly";
            break;
        case 4:
            s2 = "Bi-Weekly";
            break;
        case 5:
            s2 = "Monthly";
            break;
        }
        occrItem = new QTableWidgetItem(s2,0);

        daybits = qry.record().field("dayofweek").value().toInt();
        s3 = "";
        if ( !(daybits ^ DAY_SUN) ) s3 += "Su ";
        if ( !(daybits ^ DAY_MON) ) s3 += "M ";
        if ( !(daybits ^ DAY_TUE) ) s3 += "T ";
        if ( !(daybits ^ DAY_WED) ) s3 += "W ";
        if ( !(daybits ^ DAY_THU) ) s3 += "Th ";
        if ( !(daybits ^ DAY_FRI) ) s3 += "F ";
        if ( !(daybits ^ DAY_SAT) ) s3 += "Sa ";
        daysItem = new QTableWidgetItem(s3,0);

        ui->tblViewNets->setItem(r,0,idItem);
        ui->tblViewNets->setItem(r,1,nameItem);
        ui->tblViewNets->setItem(r,2,freqItem);
        ui->tblViewNets->setItem(r,3,modeItem);
        ui->tblViewNets->setItem(r,4,bandItem);
        ui->tblViewNets->setItem(r,5,sttimeItem);
        ui->tblViewNets->setItem(r,6,baseItem);
        ui->tblViewNets->setItem(r,7,notesItem);
        ui->tblViewNets->setItem(r,8,occrItem);
        ui->tblViewNets->setItem(r,9,daysItem);
        qry.next();
    } while ( qry.isValid() );

    qry.finish();
    ui->tblViewNets->setCornerButtonEnabled(false);
    ui->tblViewNets->repaint();
}

void dlgNetsMaint::createObjects()
{
    // not really creating objects here but it is provided for consistency
    ui->tblViewNets->setWindowTitle("Valid Nets Recorded");
    ui->tblViewNets->setAlternatingRowColors(true);
    ui->tblViewNets->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tblViewNets->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tblViewNets->setFont(nm_data->font);

    ui->btnAddNet->setEnabled(true);
    ui->btnUpdtNet->setEnabled(false);
    ui->btnDelNet->setEnabled(false);

    ui->editNetName->setText("");

    nm_data->colhdrs = "ID|Net Name|Default Freq.|Mode|Band|Net St Time| TZ |  N o t e s   | Occurs|  On Days  ";
    nm_data->m_utc = false;
    nm_data->needsSaving = false;
}

void dlgNetsMaint::createConnections()
{
    connect(ui->btnAddNet,  SIGNAL(clicked()),this,SLOT(slotAddNet()));
    connect(ui->btnUpdtNet, SIGNAL(clicked()),this,SLOT(slotChgNet()));
    connect(ui->btnDelNet,  SIGNAL(clicked()),this,SLOT(slotDelNet()));
    connect(ui->btnCancel,  SIGNAL(clicked()),this,SLOT(slotCancel()));
    connect(ui->btnReset,   SIGNAL(clicked()),this,SLOT(slotReset()));
    connect(ui->tblViewNets,SIGNAL(itemSelectionChanged()),this,SLOT(slotRowSel()));

    QMetaObject::connectSlotsByName(this);
}

void dlgNetsMaint::resizeTblCols()
{ // resize the log columns by string definition colhdrs
    int x;

    QList<QString> lbls = nm_data->colhdrs.split("|",QString::KeepEmptyParts);
    ui->tblViewNets->setHorizontalHeaderLabels(lbls);

    QString cols = nm_data->colwidths;

    QPalette palette;
    QBrush altbrush;

    altbrush.setColor(nm_data->altColor);
    palette.setBrush(QPalette::AlternateBase, altbrush);
    // force header font to following
    nm_data->font.setFamily(nm_data->font_family);
    nm_data->font.setPointSize(8);
    nm_data->font.setBold(true);
    nm_data->font.setItalic(false);
    nm_data->font.setUnderline(false);
    nm_data->font.setFamily(nm_data->font_family);
    nm_data->font.setPointSize(nm_data->font_size);
    nm_data->font.setBold(true);
    nm_data->font.setItalic(nm_data->font_italic);
    nm_data->font.setUnderline(nm_data->font_underline);

    ui->tblViewNets->clear();
    ui->tblViewNets->setColumnCount(11);
    ui->tblViewNets->setPalette(palette);
    ui->tblViewNets->setAlternatingRowColors(true);
    ui->tblViewNets->setFont(nm_data->font);

    nm_data->font.setBold(true);

    QStringList col_list = cols.split("|",QString::KeepEmptyParts);
    x = col_list.count();
    for ( int y = 0; y < x; ++y )
    {
        ui->tblViewNets->setColumnWidth(y,col_list.at(y).toInt());
    }
    nm_data->font.setBold(false);
    ui->tblViewNets->setFont(nm_data->font);
}

void dlgNetsMaint::saveColWidths()
{
    // save the log columns to stored string
    quint16 y = 0;
    quint16 z = 0;

    // saving current column widths
    nm_data->colwidths = "";

    for ( y = 0; y < 12; ++y )
    {
        z = ui->tblViewNets->columnWidth(y);
        if (z == 0) break;
        nm_data->colwidths += QString("%1").arg(z);
        if ( y != 11 ) nm_data->colwidths += "|";
    }
}

void dlgNetsMaint::displaySettings()
{
    QFont font;
    font.setFamily(nm_data->font_family);
    font.setPointSize(nm_data->font_size);
    font.setBold(nm_data->font_bold);
    font.setItalic(nm_data->font_italic);
    font.setUnderline(nm_data->font_underline);
    nm_data->altFont = font;
    ui->tblViewNets->setFont(font);

//    restoreGeometry(nm_data->wgeo);
    ui->splitWindow->restoreGeometry(nm_data->splt);
}

void dlgNetsMaint::readSettings()
{
    quint16 p;
    QSettings settings("AD5XJ", "QNetLog");

    settings.beginGroup("NetMaint");
//     nm_data->pos       = settings.value("pos", QPoint(100,200)).toPoint();
//     nm_data->size      = settings.value("size", QSize(556,683)).toSize();
     nm_data->wgeo = settings.value("WGeo","").toByteArray();
     nm_data->splt = settings.value("SplitGeo","").toByteArray();
     nm_data->colwidths = settings.value("ColWidths","29|262|107|73|70|99|59|770|186|100|100").toString();
     nm_data->color_r = settings.value("altRowColorR", "150").toInt();
     nm_data->color_g = settings.value("altRowColorG", "150").toInt();
     nm_data->color_b = settings.value("altRowColorB", "150").toInt();
     nm_data->color_a = settings.value("altRowColorA", "255").toInt();
     nm_data->altColor = QColor(nm_data->color_r,nm_data->color_g,nm_data->color_b,nm_data->color_a);
     nm_data->font_family = "Arial";
     nm_data->font_family = settings.value("font_family", "DejaVu Sans Mono").toString();
     p = settings.value("font_point", 10).toInt();
     if ( p < 8 || p > 54 ) p = 10;
     nm_data->font_size  = p;
     nm_data->font_bold = false;
     if (settings.value("font_bold", "0").toString() == "1") nm_data->font_bold = true;
     nm_data->font_italic = false;
     if (settings.value("font_italic", "0").toString() == "1") nm_data->font_italic = true;
     nm_data->font_underline = false;
     if (settings.value("font_underline", "0").toString() == "1") nm_data->font_underline = true;
    settings.endGroup();
}

void dlgNetsMaint::saveSettings()
{
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("NetMaint");
//     settings.setValue("pos", this->pos());
//     settings.setValue("size", size());
     settings.setValue("WGeo",nm_data->wgeo);
     settings.setValue("SplitSize",nm_data->splt);
     settings.setValue("ColWidths", nm_data->colwidths);
     settings.setValue("altRowColorR", nm_data->color_r);
     settings.setValue("altRowColorG", nm_data->color_g);
     settings.setValue("altRowColorB", nm_data->color_b);
     settings.setValue("altRowColorA", nm_data->color_a);
     settings.setValue("font_family", nm_data->font_family);
     settings.setValue("font_point", nm_data->font_size);
     settings.setValue("font_bold", nm_data->font_bold);
     settings.setValue("font_italic", nm_data->font_italic);
     settings.setValue("font_underline", nm_data->font_underline);
    settings.endGroup();
    settings.sync();
}
