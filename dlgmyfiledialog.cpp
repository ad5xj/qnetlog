#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtGui/QDesktopServices>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QProgressDialog>

#include "dlgmyfiledialog.hpp"

class dlgMyFileDialog::dmfPrivateData
{
public:
    QString myTitle;
    QString currentFile;
    QDir    currentDir;
};

dlgMyFileDialog::dlgMyFileDialog(QWidget *parent) : QDialog(parent)
{
    dmf_data = new dmfPrivateData;

    ui = new Ui::dlgMyFileDialog;
    ui->setupUi(this);
    ui->retranslateUi(this);

    ui->cboInDir->setCurrentText(QDir::currentPath());
    ui->tblFiles->setSelectionBehavior(QAbstractItemView::SelectRows);

    QStringList labels;
    labels << tr("Filename") << tr("Size");
    ui->tblFiles->setHorizontalHeaderLabels(labels);
    ui->tblFiles->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tblFiles->verticalHeader()->hide();
    ui->tblFiles->setShowGrid(true);

    connect(ui->btnQuit,   SIGNAL(clicked(bool)),         this, SLOT(close()));
    connect(ui->btnBrowse, SIGNAL(clicked(bool)),         this, SLOT(slotBrowse()));
    connect(ui->btnFind,   SIGNAL(clicked(bool)),         this, SLOT(slotFind()));
    connect(ui->editName,  SIGNAL(editingFinished()),     this, SLOT(slotKeepName()));
    connect(ui->tblFiles,  SIGNAL(cellActivated(int,int)),this, SLOT(slotOpenFileOfItem(int,int)));
}

dlgMyFileDialog::~dlgMyFileDialog()
{
    delete ui;
}

void dlgMyFileDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void dlgMyFileDialog::setTitle(QString title)
{
    ui = new Ui::dlgMyFileDialog();
    ui->setupUi(this);
    ui->retranslateUi(this);

    dmf_data->myTitle = this->windowTitle();
    QString t = dmf_data->myTitle.trimmed() + " - " + title.trimmed();
    this->setWindowTitle(t);
}

void dlgMyFileDialog::setPath(QString path)
{
    dmf_data->currentDir = path;
}

void dlgMyFileDialog::setPattern(QString pattern)
{
    ui->cboFileType->setCurrentText(pattern);
}

void dlgMyFileDialog::slotBrowse()
{
    qDebug() << "Browse button pressed...";
    QString path = QDir::currentPath();
    path += "/";
    path += ui->cboFindText->currentText();
    QString directory = QFileDialog::getExistingDirectory(this,tr("Find Files"), path);

    qDebug() << "Browsing dir " << directory;
    if ( !directory.isEmpty() )
    {
        if ( ui->cboInDir->findText(directory) == -1 ) ui->cboInDir->addItem(directory);
        ui->cboInDir->setCurrentIndex(ui->cboInDir->findText(directory));
    }
}

void dlgMyFileDialog::slotFind()
{
    ui->tblFiles->setRowCount(0);

    QString fileName = ui->cboFileType->currentText(); // "*.pdf"
    QString srchtext = ui->cboFindText->currentText(); // "*.pdf"
    QString path = ui->cboInDir->currentText();        // /home/ken/Downloads

    dmf_data->currentDir = path;
    QStringList files, filters;
    if ( fileName.isEmpty() ) fileName = "*";
    filters << "*.pdf" << "*.txt";
    dmf_data->currentDir.setNameFilters(filters);
    files = dmf_data->currentDir.entryList(filters,QDir::Files | QDir::NoSymLinks);
    if ( !srchtext.isEmpty() ) files = findFiles(files, ui->cboFindText->currentText());
    showFiles(files);
}

QStringList dlgMyFileDialog::findFiles(const QStringList &files, const QString &text)
{

    int i = 0;
    QStringList foundFiles;

    QProgressDialog progressDialog(this);

    progressDialog.setCancelButtonText(tr("&Cancel"));
    progressDialog.setRange(0, files.size());
    progressDialog.setWindowTitle(tr("Find Files"));

    for ( i = 0; i < files.size(); ++i )
    {
       progressDialog.setValue(i);
       progressDialog.setLabelText(tr("Searching file number %1 of %2...").arg(i).arg(files.size()));
       qApp->processEvents();
       QFile file(dmf_data->currentDir.absoluteFilePath(files[i]));

       if ( file.open(QIODevice::ReadOnly) )
       {
           QString line;
           QTextStream in(&file);
           while (!in.atEnd())
           {
               if ( progressDialog.wasCanceled() )  break;
               line = in.readLine();
               if (line.contains(text))
               {
                   foundFiles << files[i];
                   break;
               }
           }
       }
    }
    return foundFiles;
}

void dlgMyFileDialog::slotOpenFileOfItem(int row, int col)
{
    Q_UNUSED(col);
    QTableWidgetItem *item = ui->tblFiles->item(row, 0);
    dmf_data->currentFile = QUrl::fromLocalFile(dmf_data->currentDir.absoluteFilePath(item->text())).toString();
    emit signalFileSelected();
//    QDesktopServices::openUrl(QUrl::fromLocalFile(dmf_data->currentDir.absoluteFilePath(item->text())));
}

void dlgMyFileDialog::showFiles(const QStringList &files)
{
    int i = 0;

    for ( i = 0; i < files.size(); ++i )
    {
        qDebug() << files[i];
        QFile file(dmf_data->currentDir.absoluteFilePath(files[i]));
        qint64 size = QFileInfo(file).size();

        QTableWidgetItem *fileNameItem = new QTableWidgetItem(files[i]);
//        fileNameItem->setFlags(fileNameItem->flags() ^ Qt::ItemIsEditable);
        QTableWidgetItem *sizeItem = new QTableWidgetItem(tr("%1 KB").arg(int((size + 1023) / 1024)));
        sizeItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
//        sizeItem->setFlags(sizeItem->flags() ^ Qt::ItemIsEditable);

        int row = ui->tblFiles->rowCount();
        ui->tblFiles->insertRow(row);
        ui->tblFiles->setItem(row, 0, fileNameItem);
        ui->tblFiles->setItem(row, 1, sizeItem);
    }
    ui->lblFilesFound->setText(tr("%1 file(s) found").arg(files.size()) + (" (Double click on a file to open it)"));
    ui->lblFilesFound->setWordWrap(false);
}

QString dlgMyFileDialog::getOpenFileName()
{
    QString ret = dmf_data->currentDir.canonicalPath().toLocal8Bit();
    ret += dmf_data->currentFile;
    return ret;
}

