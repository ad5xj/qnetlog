/*************************************************************************
 *  Interface adif2parser.hpp for QNetLog ADIF 2 parse function          *
 *  implemented by adif2parser.cpp                                       *
 *               -------------------                                     *
 *  begin                : JUNE 2014                                     *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied       *
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     *
 *  THE AUTHOR NOR SUPPLIER OF THIS SOFTWARE ARE NOT LIBABLE FOR ANY     *
 *  CONSEQUENCE OF USE OF THIS PRODUCT EITHER IMPLIED OR ACTUAL. See     *
 *  the GNU General Public License for more details.                     *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#ifndef ADIFPARSER_HPP
#define ADIFPARSER_HPP
/*! \file  adifparser.hpp */

#include <QtCore/QObject>
#include <QtCore/QIODevice>
#include <QtWidgets/QWidget>

#include "globals.hpp"
#include "callsigns.hpp"
#include "mainwindow.hpp"

/*! \ingroup ADIF
 * \class ADIF2Parser
 * \brief  XML Parser Object
 *
 * To parse ADIF data from file
 *
 * Current code base is for ADIF 2.2.7 .adi file
 * format should follow adif.org specs as of 11 Jun 2011
 *
 * \param QObject parent                  */
class ADIF2Parser : public QObject
{
    Q_OBJECT

public:
    explicit ADIF2Parser(QWidget *parent=0);
    virtual ~ADIF2Parser();

    /*!
     * \brief
     * Helper function to process ADIF import data file. This function has been
     * highly modified to accept our selection options for import function to
     * fit our selection scheme. The input param-opts is a string that gets
     * split and used to set the bools locally.
     *
     * Itterate through initial rows until eoh or EOH token encountered.
     *
     * With current row number saved where the header ends,
     * move on to the rest of the data.
     *
     * The field name token is of type fieldname,n,d
     * The name and data length are separated by ":" and an optional
     *    data type char may be included, separated by ":"
     * ADIF v2 needs to use all after the ":" in the field name to get field value length
     * [optional data type single char may be supplied as:<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"s" for string<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"m" for multi-line string (rarely used)<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"n" for numeric<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"b" for bool - "Y" = true and "N" = false<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"d" for date in ISO format UTC yyyymmdd<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"t" for time in UTC ISO format of HHMMSS or HHMM using leading zero<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where HH > 0 <= 23 and<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MM > 0 <= 59<br />
     * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SS > 0 <= 59<br />
     * ]<br />
     * fieldname ends with ">"<br />
     *
     * The first char "<" after the <eoh> begins the first field
     *   of the first data record or command token like <eor> .<br /><br />
     *
     * The first char "<" after the <eor> or the <eor>/<EOR> token
     *   signals the end of an adif data row and the need to store data
     *   starts the first fieldname of a record - ">" ends field name and
     *   starts the data until the starting "<" of the next record or a <eor><br /><br />
     *
     *
     * User applications may create fields designated by "APP_{PROGID}_{FIELDNAME}" prefix
     * example: "<APP_MONOLOG_BIRTHDAY:8:d>"<br /><br />
     *
     * data is in the format of:<br />
     * \code
     * QList
     * {
     *    int     seq;   "record sequence in order of occurance"
     *    QString name;  "like qso_date:8 or call:6"
     *    QString value; "like yyyymmdd for date or WB8BZH for call"
     * } fields;
     * \endcode
     *
     * \note
     * Not all fields may be in all records -- each may be a unique list of fields
     *
     * \param QString fil
     * \param QString opts
     */
    void processADIF2Import(QString fil, QString opts);

signals:
    void    signalImported();
    void    signalImportAdd();
    void    signalImportUpdt();
    void    signalDone();                       //!< emited when all processing is finished
    QString signalUpdateStatusMsg(QString msg); //!< emited when updating the parent

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errorString;

    typedef struct field_names_type
    {
        int     seq;
        QString name;
        QString value;
    } Fieldnames;

    QList<Fieldnames> recfields; //!< a list of all fields in the record

    /*! \class adifPrivateData
     * \brief
     * Local definition of private data     */
    class adifPrivateData;
    adifPrivateData *adif_data;                 //!< *d-ptr to local private data

    void saveRecord(QList<Fieldnames> rec, QString o); //!< utility function to save all fields and  data to the database
};
#endif // ADIFPARSER_HPP
