#ifndef NETS_HPP
#define NETS_HPP
/*! \file nets.hpp   */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>

#include "globals.hpp"

/*! \ingroup DATABASE
 * \class Nets
 * \details
 * Constructs a database interface to the Nets table. Implementation is the same
 * no matter what database server is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">bool</span>:save(NetsRec rec)<br />
 * <span style="color: green;">bool</span>:del(NetsRec rec)<br />
 * <span style="color: green;">bool</span>:update(NetsRec rec)<br />
 * <span style="color: green;">QSqlQuery</span>:findNets()<br />
 * <span style="color: green;">QSqlQuery</span>:findNetID(int id) and<br />
 * <span style="color: green;">QSqlQuery</span>:findNetsRecord(int id)<br /><br /> */
class Nets : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)        //!<
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
     * <span style="color: green;">QObject::Nets(QWidget * parent = 0)</span><br><br>
     * &nbsp;&nbsp;The implementation of the interface to the Nets table in the database
     * provides a wrapper around the SQL functions needed to access and maintain
     * records in the nets table of the database.
     * \param QObject *parent                     */
    explicit Nets(QObject *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;Nets::<span style="color: green;">~Nets(QWidget * parent = 0)</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~Nets();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(NetsRec rec)</span><br /><br />
     * SQL function to save nets record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(NetsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(NetsRec rec)</span><br /><br />
     * SQL function to delete a specific nets record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(NetsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(NetsRec rec)</span><br /><br />
     * SQL function to update a specific nets record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(NetsRecord rec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">findNetsCallSign(NetsRec rec)</span><br /><br />
     * SQL function to validate the existence of a specific nets record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool findNetsCallSign(NetsRecord rec);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findNets(NetsRec rec)</span><br /><br />
     * SQL query returning all rows in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findNets();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findNetsID(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findNetID(int id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findNetsID(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findNetsRecord(int id);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;
};

#endif // NETS_HPP
