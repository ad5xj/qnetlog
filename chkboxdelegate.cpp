#include <QtCore/QDebug>
#include <QtGui/QPainter>
#include <QtWidgets/QCheckBox>

#include "chkboxdelegate.hpp"

CheckBoxDelegate::CheckBoxDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{
    //
}

CheckBoxDelegate::~CheckBoxDelegate()
{
    //
}

QWidget* CheckBoxDelegate::createEditor(QWidget *parent,
                                        const QStyleOptionViewItem &option,
                                        const QModelIndex &index
                                       ) const
{
    Q_UNUSED(index)
    Q_UNUSED(option)
    QCheckBox *chkbx = new QCheckBox(parent);
    return chkbx;
}

void CheckBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    bool newval;

    newval = index.model()->data(index, Qt::EditRole).toInt(0);

    QCheckBox *chkbx = static_cast<QCheckBox*>(editor);
    chkbx->setChecked(newval);
}

void CheckBoxDelegate::setModelData(QWidget *selector, QAbstractItemModel *model, const QModelIndex &index) const
{
    // find the item in the model and update it
    QCheckBox *chkBox = static_cast<QCheckBox*>(selector);
    model->setData(index, chkBox->isChecked(), Qt::EditRole);
}

void CheckBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // resize to container - not the normal action - use fixed size of 20
    // provied here for compatibility
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}

//QSize CheckBoxDelegate::sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const
//{
//    return QStyledItemDelegate::sizeHint(option, index);
//}

