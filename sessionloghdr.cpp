#include <QtCore/QDebug>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlTableModel>

#include "sessionloghdr.hpp"

SessionHeader::SessionHeader(QObject *parent) : QObject(parent)
{
    // NetHeader database maintenance implementation
}

SessionHeader::~SessionHeader()
{
    //
}

bool SessionHeader::save(SessionHdrRecord rec)
{   // save the header record
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    // the primary key id column is autoincrement so we do not supply a value
    strDML  = "INSERT INTO sessionloghdr ";
    strDML += "(";
    strDML += "netid, ";            // foreign key to nets definition table
    strDML += "timebase, ";         // 0=UTC 1=localtime
    strDML += "sessionmode, ";      // scheduled mode for net
    strDML += "sessionoccr,";       // index of selected radio button on maint dialog
    strDML += "sessiondays, ";      // actual days as packed bits
    strDML += "sessiondate,";       // actual date net was held
    strDML += "sessionncs, ";       // NCS operator for this net session
    strDML += "sessionband, ";      // band for this net
    strDML += "sessionfreq, ";      // actual net freq for session
    strDML += "sessionst, ";        // actual net start time
    strDML += "sessionend, ";       // actual net ending time
    strDML += "sessionlen, ";       // actual net length in hh:mm:ss
    strDML += "stationcnt, ";       // number of contacts logged
    strDML += "sessionnotes";       // NCS notes for this net
    strDML += ") ";
    strDML += " VALUES (";
    strDML += QString("%1").arg(rec.netid) + ", ";        // 1
    if (rec.timebase)                                     // 2
        strDML += "1, ";
    else
        strDML += "0, ";
    strDML += QString("%1").arg(rec.sessionmode) + ", ";      // 3
    strDML += QString("%1").arg(rec.sessionoccur) + ", ";    // 5
    strDML += QString("%1").arg(rec.sessiondays) + ", ";      // 6
    strDML += "'" + rec.sessiondate + "', ";                  // 7
    strDML += "'" + rec.sessionncs + "', ";
    strDML += QString("%1").arg(rec.sessionband) + ", ";      // 8
    strDML += "'" + rec.sessionfreq + "', ";                  // 4
    strDML += "'" + rec.sessionst + "', ";                // 9
    strDML += "'" + rec.sessionend + "', ";               // 9
    strDML += "'" + rec.sessionlen + "', ";               // 7
    strDML += QString("%1").arg(rec.stationcnt)  + ", ";
    strDML +=  "'" + rec.sessionnotes + "');";   // 11
    //qDebug() << "preparing ADD SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    if ( !qry.lastError().type() == QSqlError::NoError )
    {
        err = qry.lastError().number();
        errmsg = qry.lastError().text();
        errmsg += "SQL:" + strDML;
        success = false;
    }
    else
    {
        err = QSqlError::NoError;
        errmsg = "";
        success = true;
    }
    return success;
}

bool SessionHeader::update(SessionHdrRecord rec)
{
    //
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    // note that not all fields are updated -  only what should change is update
    strDML  = "UPDATE netlogheader SET ";
    strDML += "netid="         + QString("%1").arg(rec.netid) + ", ";
    strDML += "sessionfreq='"  + rec.sessionfreq + "', ";
    strDML += "stationcnt="    + QString("%1").arg(rec.stationcnt) + ", ";
    strDML += "sessionst='"    + rec.sessionst + "', ";
    strDML += "sessionend='"   + rec.sessionend + "', ";
    strDML += "sessionlen="    + rec.sessionlen + ", ";
    strDML += "sessionnotes='" + rec.sessionnotes + "' ";
    strDML += " WHERE id = ";
    strDML += QString("%1").arg(rec.id) + "; ";
    //qDebug() <<  "preparing Log header UPDATE SQL:" << strDML;
    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err  == QSqlError::NoError ) success = true;
    //qDebug() << "Log Header UPDATE query err " << err << errmsg;
    return success;
}

bool SessionHeader::del(SessionHdrRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    strDML  = "DELETE FROM sessionloghdr WHERE id=" + QString("%1").arg(rec.netid);        // 1
    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    if ( !qry.lastError().type() == QSqlError::NoError )
    {
        err = qry.lastError().number();
        errmsg = "Delete on ID=" + QString("%1").arg(rec.netid) + " ";
        errmsg += qry.lastError().text();
        errmsg += "SQL:" + strDML;
        return false;
    }
    else
    {
        err = QSqlError::NoError;
        errmsg = "";
    }
    return true;
}

int SessionHeader::findLastHeader()
{
    int id = 0;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry3(db);
    QString strDML = "";
    // now find last id added - SQL below is for SQLITE only
    // other databases (like MYSQL) will have to adjust sytax
    // acccording to that database SQL limitations and function
    // availability
    strDML = "SELECT max(id) id from sessionloghdr";
    //qDebug() << "preparing LASTID() SQL:" << strDML;
    qry3.exec(strDML);
    qry3.first();
    id = qry3.record().field("id").value().toInt();
    //qDebug() << "Returning last id: " << QString("%1").arg(id);
    return id;
}

QSqlQuery SessionHeader::findHdrRecord(SessionHdrRecord rec)
{
    QString strDML = "";
    QSqlDatabase db = QSqlDatabase::database();

    //qDebug() << "declaring select query ...";
    QSqlQuery qry(db);

    //qDebug() << "selecting record from net log header...";

    strDML  = "SELECT id,";
    strDML += "netid,";
    strDML += "timebase,";
    strDML += "sessionmode,";
    strDML += "sessionoccr,";
    strDML += "sessiondays,";
    strDML += "stationcnt,";
    strDML += "sessiondate,";
    strDML += "sessionncs,";
    strDML += "sessionband,";
    strDML += "sessionfreq,";
    strDML += "sessionst,";
    strDML += "sessionend,";
    strDML += "sessionlen,";
    strDML += "sessionlgr,";
    strDML += "sessionnotes ";
    strDML += " FROM netlogheader ";
    strDML += " WHERE id = ";
    strDML += QString("%1").arg(rec.id);
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if (qry.isValid())
    {
        //qDebug() << "Log header query success ";
    }
    else
    {
        //qDebug() << "Log header query invalid " << qry.lastError().text();
        return qry;
    }
    return qry;
}

QSqlQuery SessionHeader::findNets(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT DISTINCT id,";
    strDML += "netid,";
    strDML += "timebase,";
    strDML += "sessiondate,";
    strDML += "sessionmode,";
    strDML += "sessionoccr,";
    strDML += "sessiondays,";
    strDML += "stationcnt,";
    strDML += "sessionncs,";
    strDML += "sessionband,";
    strDML += "sessionfreq,";
    strDML += "sessionst,";
    strDML += "sessionend,";
    strDML += "sessionlen,";
    strDML += "sessionnotes ";
    strDML += "FROM sessionloghdr ";
    strDML += "WHERE netid=" + QString("%1").arg(id) + " ";
    strDML += "ORDER BY sessiondate, sessionst ";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}

QSqlQuery SessionHeader::findNetHdrs(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT DISTINCT ";
    strDML += "nets.netname, ";
    strDML += "nets.netband, ";
    strDML += "nets.netfreq, ";
    strDML += "nets.netsttime, ";
    strDML += "nets.netnotes, ";
    strDML += "sessionloghdr.sessiondate, ";
    strDML += "sessionloghdr.sessionst, ";
    strDML += "sessionloghdr.sessionend, ";
    strDML += "sessionloghdr.stationcnt, ";
    strDML += "sessionloghdr.sessionnotes ";
    strDML += "FROM nets, sessionloghdr ";
    strDML += "WHERE ";
    strDML += "sessionloghdr.id = ";
    strDML += QString("%1").arg(id);
    strDML += " and ";
    strDML += "nets.id = sessionloghdr.netid and ";
    qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}
