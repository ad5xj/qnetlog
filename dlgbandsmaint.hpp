#ifndef DLGBANDSMAINT_HPP
#define DLGBANDSMAINT_HPP
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>
#include <QtWidgets/QTableView>
#include <QtSql/QSqlTableModel>

#include "globals.hpp"

#include "ui_dlgbandsmaint.h"

class Bands;

namespace Ui {
class DlgBandsMaint;
}

/*! \ingroup QNETLOG
 * \class DlgBandsMaint
 * \details
 * This dialog provides database maintenance functions for the Bands table
 * in the database.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotAddRec()<br />
 * <span style="color: green;">void</span>:slotUpdtRecRec()<br />
 * <span style="color: green;">void</span>:slotRowSel()<br />
 * <span style="color: green;">void</span>:slotCancel()<br />
 * <span style="color: green;">void</span>:slotReset()<br />
 * <span style="color: green;">void</span>:slotExit()<br />        */
class DlgBandsMaint : public QDialog
{
    Q_OBJECT

public:
    /*!
     * \brief
     * The Bands Database Table Maintenance Dialog
     * \param QWidget parent
     * \sa Bands
     * \sa DATABASE                           */
    explicit DlgBandsMaint(QWidget *parent = 0);

    /*!
     * \brief
     * &nbsp;&nbsp;DlgBandsMaint::<span style="color: green;">~DlgBandsMaint()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~DlgBandsMaint();

protected:
    void changeEvent(QEvent *e);                //!<
    void closeEvent(QCloseEvent *);             //!<
    void resizeEvent(QResizeEvent *);           //!<
    void showEvent(QShowEvent *);               //!<

private Q_SLOTS:
    void slotAddRec();                          //!<
    void slotUpdtRec();                         //!<
    void slotRowSel();                          //!<
    void slotCancel();                          //!<
    void slotReset();                           //!<
    void slotExit();                            //!<

private:
    /*! \class cmPrivateData
     * \brief Local definition of private data  */
    class BandsPrivateData;
    BandsPrivateData *bm_data;                  //!< *d-ptr to local private data values

    Ui::DlgBandsMaint *ui;                      //!< *d-ptr to GUI form

    void clearForm();                           //!<
    void resetButtons();                        //!<
    void loadBandsTable();                      //!<
    void createObjects();                       //!<
    void connectSlots();                        //!<
    void resizeTblCols();                       //!<
    void saveDataFromForm();                    //!<
    void updtDataFromForm();                    //!<
    void saveColSizes();                        //!<
    void displaySettings();                     //!<
    void readSettings();                        //!<
    void saveSettings();                        //!<
};

#endif // DLGBANDSMAINT_HPP
