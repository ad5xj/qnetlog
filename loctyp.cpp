#include <QtCore/QDebug>

#include "loctyp.hpp"

LocType::LocType(QObject *parent) : QObject(parent)
{
    // LocType database maintenance implementation
}

LocType::~LocType()
{
    //
}

bool LocType::del(LocTypeRec)
{
    //
    return false;
}

bool LocType::save(LocTypeRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    sqlStr  = "INSERT INTO loctype ( id, Label ) ";
    sqlStr += "VALUES ( ";
    sqlStr += QString("%1").arg(rec.ID);
    sqlStr += ", '";
    sqlStr += rec.Label;
    sqlStr += "' );";
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error saving Bands data: err - " << err << " " << errmsg;
        success = false;
    }
    return success;
}

bool LocType::update(LocTypeRec rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr = "";

    sqlStr  = "UPDATE loctype SET ";
    sqlStr += "id=" + QString("%1").arg(rec.ID) + ", ";
    sqlStr += "Label='" + rec.Label + "' ";
    sqlStr += " WHERE id = ";
    sqlStr += QString("%1").arg(rec.ID) + "; ";
    //qDebug() <<  "preparing UPDT SQL:" << sqlStr;
    qry.prepare(sqlStr);

    //qDebug() << "executing...";
    qry.exec();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        success = true;
    }
    else
    {
        success = false;
        err = qry.lastError().number();
        errmsg = qry.lastError().text();
        qDebug() << "UPDATE query err: " << QString("%1").arg(err) << " - " << errmsg;
    }
    return success;
}

QSqlQuery LocType::findLocTypeRecord(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString   sqlStr;

    sqlStr  = "SELECT (id, Label) FROM loctype WHERE id = " + QString("%1").arg(id);
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    qry.first();
    if ( qry.isValid() )
    {
        //qDebug() << "Specific net query success ";
    }
    else
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "Specific net query invalid " << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery LocType::findLocTypeID(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    //qDebug() << "selecting records from table Bands...";
    sqlStr  = "SELECT (id, Label) FROM loctype " + QString("%1").arg(id);
    qry.prepare(sqlStr);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}

QSqlQuery LocType::findLocTypes()
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    sqlStr  = "SELECT id, Label FROM loctype ORDER BY id";
    qry.prepare(sqlStr);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() != QSqlError::NoError )
    {
        qDebug() << "DB Error executing query: " << sqlStr
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        return qry;
    }
    return qry;
}
