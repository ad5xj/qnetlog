#ifndef LOCTYP_HPP
#define LOCTYP_HPP
/*! \file loctyp.hpp */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>

#include "globals.hpp"

/*! \ingroup DATABASE
 * \class LocType
 * \details
 * Constructs a database interface to the LocType table. Implementation is the same
 * no matter what database server is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">bool</span>:save(BandsRec rec)<br />
 * <span style="color: green;">bool</span>:del(BandsRec rec)<br />
 * <span style="color: green;">bool</span>:update(BandsRec rec)<br />
 * <span style="color: green;">QSqlQuery</span>:findLocTypes()<br />
 * <span style="color: green;">QSqlQuery</span>:findLocTypeID(int id) and<br />
 * <span style="color: green;">QSqlQuery</span>:findLocTypeRecord(int id)<br /><br /> */
class LocType : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)
    Q_PROPERTY(QString errmsg READ getLastErrMsg)

public:
    /*! \brief
     * <span style="color: green;">QObject::LocType(QWidget * parent = 0)</span><br><br>
     * &nbsp;&nbsp;The implementation of the interface to the loctype table in the database
     * provides a wrapper around the SQL functions needed to access and maintain
     * records in the bands table of the database.
     * \param QObject *parent                     */
    explicit LocType(QObject *parent=0);
    /*!
     * \brief
     * &nbsp;&nbsp;LocType::<span style="color: green;">~LocType(QWidget * parent = 0)</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~LocType();

    // property read-only methods
    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }
    // DML Methods
    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(LocTypeRec rec)</span><br /><br />
     * SQL function to save LocType record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(LocTypeRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(LocTypeRec rec)</span><br /><br />
     * SQL function to delete a specific loctype record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(LocTypeRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(LocTypeRec rec)</span><br /><br />
     * SQL function to update a specific loctype record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(LocTypeRec);
    // aggregate methods
    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findLocTypes()</span><br /><br />
     * SQL query returning all rows in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findLocTypes();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findLocTypeID(int id)</span><br /><br />
     * SQL query returning rows in the table with loctype.id=id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findLocTypeID(int id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findLocTypeRecord(int id)</span><br /><br />
     * SQL query returning all rows in the table where loctype.id=id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findLocTypeRecord(int id);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;
};

#endif // LOCTYP_HPP

