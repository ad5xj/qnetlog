#include "about.hpp"

dlgAbout::dlgAbout(QWidget *parent) : QDialog(parent)
{
    ui = new Ui::dlgAbout;
    ui->setupUi(this);
    ui->retranslateUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    move(400,200);

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(close()));
}

dlgAbout::~dlgAbout()
{
    delete ui;
}
