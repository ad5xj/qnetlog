/*! \mainpage Radio Net Logging Application (Preliminary)
 * \tableofcontents
 *
 * \section copy Copyright
 * \copybrief &copy; Copyright 2015 AD5XJ Ken Standard ad5xj@arrl.net<br /><br />
 * An original project for Windows, Linux, and Mac OS X<br /><br />
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation in any format for non-commercial purposes is
 * hereby granted, provided that the above copyright notice and this
 * disclaimer appear in all copies and supporting documentation.<br /><br />
 *
 * The software <strong>MUST NOT BE SOLD</strong> or used as part of a any commercial
 * or "non-free" product. When we speak of free software, we are
 * referring to freedom, not price. Our General Public License is
 * designed to make sure that you have the freedom to distribute copies
 * of free software, that you receive source code or can get it if
 * you want it, that you can change the software or use pieces of it
 * in new free programs, and that you know you can do these things. The use
 * of this code must conform to GPL 3 or higher license restrictions.<br /><br />
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
 * BUT WITHOUT ANY WARRANTY&nbsp;&ndash;&nbsp;WITHOUT EVEN THE IMPLIED
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE
 * GNU GENERAL PUBLIC LICENSE FOR MORE DETAILS.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/<br /><br />
 *
 *
 * \section intro_sec Introduction
 * \image html QNetLog-onload.png
 *
 * This application is intended to provide radio net control operators
 * (NCO) with a tool to automate the logging process. Logging is often
 * done manually with mixed results. Sometimes, on larger and more active
 * nets, a secondary or assistant NCO may have the sole responsibility
 * of logging while the primary NCO takes the check in calls.
 *
 * QNetLog allows the net operator, with responsibility of logging, to
 * create pre-defined nets of specific properties used throughout the
 * logging process. Nets are defined once, no matter how often they may
 * be referenced. Some nets meet multiple times per day or week.
 *
 * Occurances of net sessions are logged with a header and statistics are
 * accumulated per session in that header recording. Each check-in is
 * logged against the net session record and reference the pre-defined
 * net definition as well as the session log header rather than
 * duplicating the data.
 *
 * Once ended, the net session is available for reporting in a variety of
 * formats for printing or export to PDF. Export to ADIF is also available
 * on a net session basis or range of dates.
 *
 * Net logging software is not new. In fact we wish to give credit to
 * a long-time popular application known as NetLogger by John Martin, AC0ZG
 * that has been around a long time. There is also an attempt to make the
 * legacy program into a cross-platform application replicating the look and
 * feel of the older Windows-only application.
 *
 * QNetLog is not such an effort. Any resemblance to that application
 * is not intended and is purely co-incidental.
 *
 * The essential difference being that QNetLog is not Internet based. The
 * session log is held in a local SQL Datatbase. While we do have plans to
 * make a client / server version of this software, this initial release
 * is local only. We do however, include the ability to import and export
 * the session data or the entire logbook to another user as an ADIF file.
 * The import method includes options to select whether the local database
 * or the incoming ADIF data will serve as master for data collisions.
 *
 * We may (at our option) provide a client/server version of this program
 * that may morph into a Internet capable version. The decision has not been
 * made at this initial offering.
 *
 * - - -
 * Some definitions are necessary to be clear about the meaning of terms
 * used in this documentation. These terms may be common amoung ham radio
 * operators but are provided here to establish a common vocabulary for
 * the documentation:<br><br>
 *
 * <ul>
 * <li><strong>Net:</strong> A meeting of radio operators at a specific time
 * and frequency using a common mode (AM, FM, SSB, digital, etc.). The
 * purpose of each net varys according to the net control operators and
 * administrators.</li>
 *
 * <li><strong>Net Control:</strong> In most nets, there is one point of contact;
 * the net control operator or NCO. Some nets do not allow communication
 * between stations checking in to the net unless allowed to do so by the
 * NCO. This is known as a "directed" net. Directed nets are more formal than
 * so-called "fun" nets and serve a particular public service function.</li>
 *
 * <li><strong>Net Session:</strong> A particular meeting of a net on a given
 * date and time. For instance, if Net &ldquo;A&rdquo; weekly on Saturday
 * at 10:00 AM Central Time -- Net &ldquo;B&rdquo; meets daily at 15:00 UTC
 * and again at 20:00 UTC. Net &ldquo;A&rdquo; will meet on the 40 meter
 * frequency of 7.265 Mhz and Net &ldquo;B&rdquo; will meet on the 20 meter
 * frequency of 14.260 Mhz. Each time Net &ldquo;A&rdquo; or Net &ldquo;B&rdquo;
 * meets (even if multiple times in the same day) it is considered a net session.</li>
 *
 * <li><strong>Net Session Header:</strong> This term refers to a portion of
 * the electronic log that contains collective satistics about the Session
 * as a whole. For instance, the header will contain a reference to the
 * pre-defined net which operates the net session, the start time, end time
 * and number of check-in callsigns of the net session.</li>
 *
 * <li><strong>Net Session Details:</strong> This term refers to the record
 * of each check-in to the net session. It will contain different information
 * than the session header, although it will contain a reference to the
 * session header. The recording of a detail record forms a master/detail
 * style data relationship in the log database. This is important when
 * seeking specific information on a particular session or list of sessions
 * attended by a particular station. It is not necessary for the user to
 * worry about how to do this, as it is maintained automatically by the
 * QNetLog program. This master/detail structure also forms the basis for
 * the report and search functions provided.</li>
 *
 * <li><strong>Net Control Station:</strong> The station call of the equipment
 * operated by the NCO. Sometimes the net control operator will be on a site to
 * use a club station or a remotely controlled station not within the site of
 * the NCO.</li>
 *
 * <li><strong>Net Log:</strong> A list of stations that have contacted the net
 * control operator and have been acknowledged as checking in to the net. This
 * contact record serves as a log of operation during the net session. Logs may
 * be written, printed, or electronic form. In the case of QNetLog, the log is
 * recorded electronicly, and can be exported in various other forms including
 * ADIF, PDF, printed, and displayed on the computer screen ad hoc.</li>
 * </ul>
 *
 * \note The FCC no longer requires that ham radio operators keep a log of every
 * transmission or radio contact. Although, it may be a very good idea
 * to provide reference to other operators as confirmation or to the FCC where
 * operation is in question on a particular date or time. However, the net
 * manager/administrator may require the NCS to keep records of net activity.
 *
 *
 * - - -
 * \section intro_menus Menus
 * \ref main_menu_page Main Menu
 *
 * \section intro_mainwindow Main Window
 * \ref main_window_page
 *
 * \section intro_reports Reports
 * \ref reports_page
 *
 * \section intro_reportwriter ReportWriter
 * \ref reportwriter_page
 *
 * - - -
 * \section intro_install Platform Installation
 * \ref install_sec Installation
 *
 * \section intro_api Programmer Documentation
 * \ref api_sec<br /><br /><br />
 *
 *
 *
 * \page main_menu_page Main Menus
 * \section menu_sec Application Main Window Menus
 *
 * \subsection menu_file File Menu
 *
 * \image html filemenuexp.png
 *
 * This menu is simple by design. It is designed to roughly mimick the
 * Microsoft windows application design standards and Open Software GUI
 * design. Currently, only the Exit selection is shown to end the
 * application.<br /><br /><br />
 *
 * \subsection menu_database Database Menu
 *
 *  \image html dbmenuexp.png
 *
 * This menu allows access to key database maintenance functions.<br /><br /><br />
 *
 * \subsection dbmenu_nets DB Maintenance&nbsp;&ndash;&nbsp;Nets
 *
 * \image html netsmaint.png
 *
 *
 * The image above shows the initial view of the Net Maintenance dialog with the first
 * defined net selected in the list by a click on that row. This selection displays the
 * net information, as defined, in the entry form. It also shows that the "Add Net" button
 * has been disabled. The "Add Net" button will only be activated by the lack of a selection
 * in the nets list or by pressing the "Cancel" button to clear any selection.
 *
 * The selection of any row in the nets listing will automatically place the form in
 * the edit mode. Once the row has been selected and the data displayed in the form, editing
 * may take place and will be preserved by pressing "Save". Pressing "Save" will write
 * the new data to the database, redisplay the list of nets, and place the form in "idle"
 * mode.
 *
 * Selection of a net from the list will also allow that net definition to be permanently
 * deleted from the database by pressing "Delete" while the net information is displayed
 * in the entry form.
 *
 * The "Save", "Cancel" and "Delete" buttons (by extension those functions) are not available
 * in "idle" mode.
 *
 * <strong>Fields</strong>
 * <strong>Net ID:</strong> This entry box is mostly for display purposes, as it is not
 * used on add or edit of any net definition and is not activated.<br>
 * <strong>Net Time:</strong> This time selection is a pre-formatted edit tool that allows
 * the user to edit hours (in 12 hr format), minutes and AM or PM as individual elements of net time - or use
 * the spin buttons on the right of the edit tool to increment time in pre-defined
 * increments.<br>
 * <strong>Time Format:</strong> UTC or Local - select one or the other to apply to the
 * net time to the left.<br>
 * <strong>Net Name:</strong> A text descrition of the net designed to be informative but
 * brief.<br>
 * <strong>Band: </strong> Band selection is made by selecting from the drop-down box to
 * the right of the net name.<br /><br /><br />
 *
 *
 * \subsection dbmenu_import DB Maintenance&nbsp;&ndash;&nbsp;Import Callsigns from ADIF
 *
 * \image html menudbimportexp.png
 *
 * This is a convenience selection to allow the callsigns database to be updated
 * with external, and perhaps, more up to date sources (like other NCS operator logs)
 * or separate applications (like FLDIGI) that keep logs internally in the ADIF format,
 * or can export logs to ADIF from the application menu.
 *
 * Callsigns, and (limited) related data, may be imported from another QNetLog user
 * or from another ADIF source such as FLDIGI log files. The callsigns database uses
 * limited information regardless of how much info is supplied in the ADIF file
 * imported, and is selectively loaded into the callsigns database. Import conflicts
 * are resolved by options set when starting the import process. Duplications are
 * always eliminated from the source and different information values between source
 * and current database values will update according to the options set.
 *
 * \subsection dbmenu_importsel DB Maintenance&nbsp;&ndash;&nbsp;Import, Setting Options
 * \image html importopts.png
 * \image html importstats.png
 *
 * This option dialog allows you to set up how an import of an ADIF file will update the
 * call signs database. The smaller windows just below the options window is the statistics
 * report that updates with real numbers while the import is taking place.<br /><br/><br />
 *
 * \subsection dbmenu_calls DB Maintenance&nbsp;&ndash;&nbsp;Callsigns Maintenance
 * \image html bandsmaint.png
 *
 * This selection allows editing of the bands logged by this application. Each band
 * maintained on the database displays when a net is edited or selected. When a
 * band entry is saved in the database for reference it is used speed the editing
 * process and reduce operator entry errors.
 *
 * \image html callsignsmaint.png
 *
 * This selection allows editing of the information captured by the check in process
 * of each net session. When a callsign is logged and has not appeared on the log before
 * then it is saved in the database for reference in a later session in order to speed
 * the logging process and reduce operator entry errors.
 *
 * \image html callsignslist.png
 * These callsigns will appear in the window to the right of the log entry area and
 * appear as each character is typed. Matching callsigns (to the partial callsign
 * entry) appear in a list of matching calls. Doubleclicking on a callsign in the
 * list will fill in the blanks of the entry area. If the call has never been recorded
 * then nothing will appear in the list. When the entry is completed, and the information
 * is logged, the new callsign will be permanently added to the callsigns database
 * for reference later along with the related information entered on the check in form.<br /><br /><br />
 *
 *
 * \subsection menu_reports Reports Menu
 * \image html reportmenuexp.png
 *
 * Various reports are provided from this menu. The list of reports is being developed
 * and may change with the full release version v1.00.
 *
 * \subsection menu_help Help Menu
 * \image html helpmenuexp.png
 *
 * This help is available from the Help Menu. The table of contents is on the
 * main page of the help documentation.
 *
 *
 * \page main_window_page Main Window
 * \section section_mainwindow Main Window
 * \image html QNetLog-onload.png
 * The first thing the net control or logger sees is the Main Window (as shown above).
 * This window provides a number of functions and displays used to input and
 * select net and check-in information. It is divided into several sections:
 *
 * <strong>Top Section</strong>
 * \image html maintopsection.png
 *
 * This top portion of the MainWindow not only has the Main Menu and the current tool
 * bar, it also contains the Nets Edit Form. There is a drop-down combo box to select
 * one of the pre-defined nets (see the Database Menu) and an edit box to enter the
 * callsign of the NCS operator for the net being entered (if an alternate logger is
 * used, that callsign is not entered - maybe v2.00 ?).
 *
 * The information that defines the net fills in the appropriate spaces in the Net
 * Entry Form and the cursor is positioned at the NCS operator call entry box. Before
 * the net is started, the logger is allowed to enter a note specifically for the
 * net being logged (e.g. "Conditions on the band are good today.").
 *
 * When all appropriate information for the net has been entered, the START NET button
 * can be clicked. Editing functions for entry of check-ins will then be enabled in
 * the next (center) section.
 *
 * All entries of check-ins for that point forward are entered against the net
 * information in the top section.
 *
 * You may also notice that there is an ABORT button and END NET button. If for some
 * reason it is necessary to abort the net entry function, pressing the ABORT button
 * will completely remove that net and the check-in information permanently.
 *
 * When all check-ins are complete, the END NET button will close the net information
 * and disable any more entries to the net. The information is then permanently stored
 * in the database and may be recalled or printed as needed.
 *
 *
 * <strong>Mid-Section</strong>
 * \image html mainmidsection.png
 *
 * This middle section is the entry form for all check-in stations contacted. The
 * call sign entry box has a special feature that helps with entry of the contact.
 * When characters are entered, the application checks to see if the call sign has
 * been entered before. If so, the callsign (or any like the character segment
 * entered) is displayed in the list box to the right of the form. If any of the
 * listed call signs are the one you wish to use, double-clicking on it will fill
 * in for the logger, all the stored information collected initially.
 *
 * Another unique feature of the contact entry form is, any callsign not previously
 * stored in the database will be automatically saved for recall later. It is
 * important to collect as much data on the first contact as possible so it may
 * be recalled later and speed entry of the contact check-in.
 *
 * As with the top section, you may enter a note that is unique to this login for
 * this net uniquely. Notes may be entered for each check-in on every net. This
 * is the place where comments or messages may be entered that were passed by
 * a particular station during the net.
 *
 *
 * <strong>Bottom Section</strong>
 * \image html mainbottomsection.png
 * This is the actual session log of all contacts that have checked-in during the
 * net that was started when the START NET button was clicked. It shows, in
 * tabular form, the information collected from the middle section entry form.
 *
 * <div style="float: right;
 *      width:15%;
 *      margin-top: 0px;
 *      margin-left: 6px;
 *      margin-right: 30px;">
 *   <img src="statusoptions.png" />
 *   <div style="text-align:center;"><strong>Status Option List</strong></div>
 * </div>
 * Should there be the need to correct or change what was entered, (e.g. temporary
 * location change while on vacation, late revelation of mobile or portable operation,
 * member id #, etc.) most of it can be edited right in the table, by double-clicking
 * on that item and entering the corrected information. Of course, some information
 * is not text, and an appropriate edit delegate is provided.
 *
 * One such item is the optional contact status item. To change the stored status
 * of a contact in the session log, double-click the Status item in the row that
 * contains the contact you wish to change. Doing so will display a drop-down box
 * in place of the value currently on the database. By clicking on the drop arrow,
 * a list from which you may select the appropriate status from the list displayed.
 * To change the status, click on any list item and press the tab key to move out
 * of the Status column and save the selected value to the database.
 *
 * Status  may change from &ldquo;Contacted&rdquo; to &ldquo;Check-in&rdquo; or
 * from &ldquo;Check-in&rdquo; to &ldquo;Comments&rdquo; or &ldquo;Check-In&rdquo;
 * to &ldquo;Radio Check&rdquo; as the net progresses. This is an optional feature
 * that may be used as the logger has the time during net operations. It could
 * serve as an indicator as to whether a contact has been given the chance to
 * make comments or that all contacts have been checked in.
 *
 * It should be noted that, not all items in a contact log entry will have the
 * ability to be edited or changed. That is by design; in order to maintain
 * the proper relationship to other data related to the contact or to the session
 * where the contact was logged. For instance, the ID column is not editable.
 * The ID is assigned automatically by the database server as a unique identifier
 * for that contact for that session of the net. The same is true for the
 * recorded time the contact is made.
 *
 * Individual contacts may not be deleted from this table during the net but
 * may be removed with an edit feature (see the Database menu) at a later time.
 * Special care should be used when editing or deleting data from the session
 * log so as to not destroy the permanent record from the net session unduely.
 *
 * \page reports_page REPORTS
 * \section reports_page Reporting From The Database
 * Reporting from the Database
 *
 * \subsection rpt_preformatted Pre-Formatted Reports
 *
 * Some <strong>pre-formatted reports</strong> supplied with the
 * application that do not have editing capabilities.
 *
 * <ul>
 *   <li><strong>Generic Session Log Report</strong> - Print all contacts from a particular session</li>
 *   <li><strong>SATERN SSB Net Session Log Report</strong> - Print all contacts from a particular session</li>
 *   <li><strong>SATERN Digital Net Session Log Report</strong> - Print all contacts from a particular session</li>
 *   <li><strong>Selected Callsign Participation Report</strong> - Print all sessions checked in by a particular callsign</li>
 *   <li><strong>Selected Net Session Meeting List</strong> - Print all meeting sessions of a particular net</li>
 *   <li><strong>Nets Listing</strong> - Print all nets defined on the database with their particulars</li>
 * </ul><br /><br />
 *
 * \sa ReportWriter<br /><br />
 * Printing of reports is somewhat different from most packaged applications, in
 * that, the logger has much more flexibility than before. There are some pre-defined
 * reports (shown above) included with the package release.
 *
 * Just how this is accomplished is up the adventurous user willing to learn
 * the use of the ReportWriter module.
 *
 *
 * \page reportwriter_page REPORTWRITER
 * \section reportwriter_page Report Writer / Creator
 *
 * Customizing Reports and Designing Custom Reports
 *
 * Printing of reports is somewhat different from most packaged applications in
 * that the logger has much more flexibility than before. There are some pre-defined
 * reports included with the package release. However, even the supplied reports
 * may be customized to suit the individual needs of the user.
 *
 * This module will allow any user to alter pre-defined reports, or create
 * completely custom reports using the visual report designer feature of the
 * ReportWriter module. This tool will allow visual creation of custom reports
 * as a WYSIWYG layout model. There is also a SQL designer that accompanies it
 * allowing connection to the data stored in the database that needs reporting.
 *
 *
 * \page install_page Platform Installation Information
 * \section install_sec Installation
 *
 * \subsection win_install Microsoft Windows
 * This application will install on standard installations of the following:<br />
 * Windows XP SP3<br />
 * Windows Vista<br />
 * Windows 7<br />
 * Windows 8<br />
 *
 * A minimum of hardware running Windows is as follows:
 * Windows XP SP3 - Intel Celeron 2.4 Ghz or higher, 2 Gb Ram or more, 30 Mb Free disk space<br />
 * Windows Vista  - Intel Celeron 2.4 Ghz or higher, 3 Gb Ram or more, 30 Mb Free disk space<br />
 * Windows 7,8 (32 bit) - Intel iSeries single or multiple core 2.3 Ghz or higher, 3 Gb Ram or more, 30 Mb Free disk space<br />
 * Windows 7,8 (64 bit) - Intel iSeries multiple core 2.4 Ghz or higher, 6 Gb Ram, 50 Mb Free disk space<br />
 *
 *
 * \subsection lin_install Linux Installations
 * All 32 bit distros   - Intel Celeron 2.3 Ghz or higher, 2 Gb Ram or more, 30 Mb Free disk space.<br />
 * All 64 bit distros   - Intel or AMD 64 bit CPU 2.4 Ghz or higher, 3 Gb Ram or more, 60 Mb free disk space.<br />
 *
 *
 * \subsection mac_install Mac OS X Installations
 * All 32 bit Intel CPUs 2.3 Ghz or higher, 2 Gb Ram or more, 30 Mb Free disk space.<br />
 * All Intel 64 bit CPU 2.4 Ghz or higher, 3 Gb Ram or more, 60 Mb free disk space.<br />
 *
 *
 *
 * \page api_docs Programming API Documentation
 * \section api_sec Application Programming Interface
 * \version v0.97.1<br /><br />
 * Revision History
 *
 * 0.95.5&nbsp;&nbsp;&ndash;&nbsp;worked on documentation and fix some minor bugs:<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Added the bands maint form and menu launcher selection<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modified database classes to be consistent and added more docs<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Added adornments to programmer docs to be more attractive<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modified callers of database classes to use current methods
 *
 * 0.95.6&nbsp;&nbsp;&ndash;&nbsp;continued work on docs and fix some minor bugs:<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Callsigns Maint  - fix bug where incomplete recordset is displayed.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Callsigns Maint  - fox bug where delete of row aborts the dialog.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Implemented delete function.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Maint        - fix table sizing on dialog resize event.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Maint        - change the quit icon to be different from cancel.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Maint        - Added the contact add function to session details<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Session Log      - edit of status changes all to same as first row.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database Menu    - entries double when redisplay. Not clearing from previous.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Form         - something funny when all letters of call entered. Leave <br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;last letter off and pick from list it works.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Callsigns Import - does not import all from source.
 *
 * 0.96.0&nbsp;&nbsp;&ndash;&nbsp;continued work on docs and fix some minor bugs<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Detail Maint - Added new form to edit contact entry. Resembles<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the main window entry form.<br />
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Log Session Maint- Implemented edit functions for details.<br />
 *
 * 0.96.1&nbsp;&nbsp;&ndash;&nbsp;&nbsp;Log Edit  - add read accessor to property so compiler would not complain.<br />
 *
 * 0.97.0&nbsp;&nbsp;&ndash;&nbsp;&nbsp;Reports   - Beginning development of the report generator for the pre-formatted reports.<br />
 * 0.97.1&nbsp;&nbsp;&ndash;&nbsp;&nbsp;Main      - minor update to add plugins folder to library search<br /><br />
 *
 * This application is written entirely in C++ using the Digia Qt Framework
 * (Open Source License) available at www.qt.io. This work started with v5.5.0.
 * Later versions may be used as they are made available when they are backward
 * compatible with the earlier version. Non-compatible updates may require a
 * complete re-compile and even syntax changes in the code.
 *
 * The QtCreator IDE will accommodate the built-in QMake or external CMake as desired. Qt will compile on Linux (almost
 * all flavors), Mac OS X mountain lion or higher, and Windows XP SP3, Windows 7,
 * and Windows 8. Windows 10 was not available for testing at the time of the
 * initial release.
 *
 * Other Links<br />
 * \ref QNETLOG      The main application<br>
 * \ref ADIF         Programing interface <br>
 * \ref DATABASE     Database <br>
 *                                         */
#include <QtCore/QTranslator>
#include <QtWidgets/QApplication>

#include "mainwindow.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;
    a.installTranslator(&translator);
    qApp->addLibraryPath("./plugins");

    MainWindow w;

    w.show();

    return a.exec();
}
