#ifndef TABLEHDRS_HPP
#define TABLEHDRS_HPP
/*! \file tablehdrs.hpp */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlQuery>

#include "globals.hpp"

/*! \ingroup ADIF
 * \class TableHdrs
 * \details
 * Constructs a database interface to the Custom Table Headers table.
 * Implementation is the same no matter what database server is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">QSqlQuery</span>: readHeadersSeq()  */
class TableHdrs : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)
    Q_PROPERTY(QString errmsg READ getLastErrMsg)

public:
    /*!
     * \brief
     * Database Interface for the Custom Table Headers db table. This table provides a
     * matchup between the ADIF model and the local database being imported to.
     *
     * \param QObject parent                       */
    explicit TableHdrs(QObject *parent=0);
    //virtual ~TableHdrs();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief Retrieve the headers from the database ordered by the seq no
     * \return QSqlQuery of header titles            */
    QSqlQuery readHeadersSeq();

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;

};
#endif // TABLEHDRS_HPP
