#include <QtCore/QSettings>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QTableWidgetItem>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>

#include "globals.hpp"
#include "bands.hpp"

#include "dlgbandsmaint.hpp"

#define FORM_TITLE "Bands Maintenance"

class DlgBandsMaint::BandsPrivateData
{
public:
    bool inAddMode;              //!<
    bool inChgMode;              //!<
    bool inDelMode;              //!<
    bool formLoaded;             //!<
    bool m_utc;                  //!<
    bool needsSaving;            //!<
    bool font_bold;              //!<
    bool font_underline;         //!<
    bool font_italic;            //!<

    quint16 font_size;           //!<
    quint16 color_r;             //!<
    quint16 color_g;             //!<
    quint16 color_b;             //!<
    quint16 color_a;             //!<
    quint32 recIndex;            //!<
    quint32 recNdx;              //!<
    quint32 row_count;           //!<

    QFont font;                  //!<
    QFont altFont;               //!<

    QColor altColor;             //!<

    QPoint pos;                  //!<
    QSize  size;                 //!<

    QString colwidths;           //!<
    QString colhdrs;             //!<
    QString font_family;         //!<
    QString Label;               //!<

    QByteArray spltWindow;       //!<
    QByteArray spltTop;          //!<
};

DlgBandsMaint::DlgBandsMaint(QWidget *parent) : QDialog(parent)
{
    bm_data = new BandsPrivateData;
    // initialize local var values
    bm_data->row_count = 0;
    bm_data->recIndex = 0;
    bm_data->recNdx = 0;
    bm_data->font_size = 8;
    bm_data->color_a = 255;
    bm_data->color_b = 255;
    bm_data->color_g = 255;
    bm_data->color_r = 255;

    bm_data->colwidths  = "";
    bm_data->colhdrs  = "";
    bm_data->font_family = "";
    bm_data->Label = "";

    ui = new Ui::DlgBandsMaint;
    ui->setupUi(this);

    createObjects();

    readSettings();

    displaySettings();

    connectSlots();

    bm_data->inAddMode = false;
    bm_data->inChgMode = false;
    bm_data->inDelMode = false;

    clearForm();
    resetButtons();
    ui->tblBands->clearContents();
    loadBandsTable();

    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

DlgBandsMaint::~DlgBandsMaint()
{
    delete ui;
    delete bm_data;
}

void DlgBandsMaint::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DlgBandsMaint::resizeEvent(QResizeEvent *)
{
    bm_data->pos = this->pos();
    bm_data->size = this->size();
    quint32 w = bm_data->size.width()  - 4;
    quint32 h = bm_data->size.height() - 20;
    ui->splitterWindow->setGeometry(2,2,w,h);
    ui->splitterTop->setGeometry(0,0,w,h/2);
//    ui->splitterTop->restoreGeometry(bm_data->spltTop);
//    bm_data->spltWindow = ui->splitterWindow->saveGeometry();
//    bm_data->spltTop    = ui->splitterTop->saveGeometry();
    saveColSizes();
    saveSettings();
}

void DlgBandsMaint::closeEvent(QCloseEvent *)
{
    // save the window geometry
    bm_data->pos = pos();
    bm_data->size = size();
    // preserve the splitter geometry
    bm_data->spltTop = ui->splitterTop->saveGeometry();
    bm_data->spltWindow = ui->splitterWindow->saveGeometry();
    // save table column widths
    saveColSizes();
    bm_data->spltWindow = ui->splitterWindow->saveGeometry();
    bm_data->spltTop = ui->splitterTop->saveGeometry();
    // make it permanent before the deconstructor fires
    saveSettings();
}

void DlgBandsMaint::showEvent(QShowEvent *)
{
    bm_data->formLoaded = true;
}

void DlgBandsMaint::slotRowSel()
{
    // get which row is selected
    int x = ui->tblBands->currentRow();
    if ( x < 0 ) return;  // nothing selected so return

    // display fields and set buttons to edit mode
    bm_data->recIndex = ui->tblBands->item(x,0)->text().toInt();
    bm_data->recNdx = ui->tblBands->item(x,1)->text().toInt();
    bm_data->Label = ui->tblBands->item(x,2)->text();

    ui->spinNdx->setValue(bm_data->recNdx);
    ui->editLabel->setText(bm_data->Label);

    ui->btnADD->setEnabled(false);
    ui->btnUpdt->setEnabled(true);
    ui->btnDel->setEnabled(true);
    ui->btnReset->setEnabled(true);
    ui->btnCancel->setEnabled(false);
    ui->btnQuit->setEnabled(false);

    QString t = FORM_TITLE + tr(" - Band Selected: ");
    t += ui->editLabel->text().trimmed();
    setWindowTitle(t);
}

void DlgBandsMaint::slotReset()
{
    ui->tblBands->clearContents();
    resizeTblCols();
    loadBandsTable();

    bm_data->inAddMode = false;
    bm_data->inChgMode = false;
    bm_data->inDelMode = false;

    clearForm();
    resetButtons();
}

void DlgBandsMaint::slotCancel()
{
    bm_data->inAddMode = false;
    bm_data->inChgMode = false;
    bm_data->inDelMode = false;

    clearForm();
    resetButtons();

    ui->tblBands->clear();
    resizeTblCols();
    loadBandsTable();
    QString t = FORM_TITLE;
    t += " ";
    t += APP_VERSION;
    setWindowTitle(t);
}

void DlgBandsMaint::slotExit()
{
    ui->btnADD->setText(tr("ADD"));
    ui->btnQuit->setText(tr("QUIT"));
    ui->btnDel->setEnabled(true);
    ui->btnCancel->setEnabled(true);

    if ( !bm_data->inAddMode &&
         !bm_data->inDelMode   &&
         !bm_data->inChgMode
       )
        close();
    else
    {
        QString msg = tr("Sorry you must cancel the active mode first. ");
        if ( bm_data->inAddMode ) msg += tr("In ADD Mode.");
        if ( bm_data->inDelMode )   msg += tr("In DELETE Mode.");
        if ( bm_data->inChgMode )   msg += tr("In Change Mode.");
        QMessageBox mbox;
        mbox.setText(msg);
        mbox.exec();
    }
}

void DlgBandsMaint::slotUpdtRec()
{
    if ( bm_data->inChgMode )
    { // save data from form to calls table
        updtDataFromForm();

        bm_data->inChgMode = false;

        clearForm();
        resetButtons();

        ui->tblBands->clear();
        resizeTblCols();
        loadBandsTable();
    }
    else
    {
        // not in update mode yet so get it set up
        ui->btnUpdt->setText(tr("SAVE"));
        ui->btnUpdt->setEnabled(true);
        ui->btnADD->setEnabled(false);
        ui->btnDel->setEnabled(false);
        ui->btnQuit->setEnabled(false);
        ui->btnCancel->setEnabled(true);
        ui->btnReset->setEnabled(false);
        ui->editLabel->setEnabled(true);
        ui->spinNdx->setEnabled(true);
        QString t = FORM_TITLE + tr(" - UPDATE Mode - ");
        setWindowTitle(t);
        bm_data->inChgMode = true;
    }
}

void DlgBandsMaint::slotAddRec()
{
    if ( !bm_data->inAddMode )
    {
        ui->btnADD->setText(tr("ADD"));
        ui->btnQuit->setText(tr("QUIT"));

        bm_data->inAddMode = true;

        ui->spinNdx->setEnabled(true);
        ui->editLabel->setEnabled(true);

        ui->btnADD->setText(tr("SAVE"));
        ui->btnDel->setEnabled(false);
        ui->btnUpdt->setEnabled(false);
        ui->btnCancel->setEnabled(false);
        ui->btnReset->setEnabled(true);
        ui->btnQuit->setText(tr("CANCEL"));
    }
    else
    {
        // save data from form to callsigns table
        saveDataFromForm();

        bm_data->inAddMode = false;

        QString t = FORM_TITLE + tr("ADD Mode");
        setWindowTitle(t);
        clearForm();
        resetButtons();

        ui->tblBands->clearContents();
        resizeTblCols();
        loadBandsTable();
    }
}

void DlgBandsMaint::updtDataFromForm()
{
    Bands       band(this);
    BandsRec    rec;

    bm_data->recNdx = ui->spinNdx->value();
    bm_data->Label  = ui->editLabel->text().trimmed();

    rec.id   = bm_data->recIndex;
    rec.ndx  = bm_data->recNdx;
    rec.Label = bm_data->Label;

    band.update(rec);
    slotReset();
}

void DlgBandsMaint::saveDataFromForm()
{
    Bands       band(this);
    BandsRec    rec;

    bm_data->recNdx = ui->spinNdx->value();
    bm_data->Label  = ui->editLabel->text().trimmed();

    if ( bm_data->inAddMode )
        rec.id = 0;
    else
        rec.id = bm_data->recIndex;
    rec.ndx  = bm_data->recNdx;
    rec.Label = bm_data->Label;

    band.save(rec);

    bm_data->inAddMode = false;
    bm_data->inChgMode = false;
    bm_data->inDelMode = false;
}

void DlgBandsMaint::resetButtons()
{
    ui->btnADD->setText(tr("ADD"));
    ui->btnUpdt->setText(tr("UPDATE"));
    ui->btnQuit->setText(tr("QUIT"));

    ui->btnADD->setEnabled(true);
    ui->btnUpdt->setEnabled(false);
    ui->btnDel->setEnabled(false);
    ui->btnCancel->setEnabled(false);
    ui->btnReset->setEnabled(false);
    ui->btnQuit->setEnabled(true);
}

void DlgBandsMaint::clearForm()
{
    ui->spinNdx->setValue(0);
    ui->editLabel->setText("");

    QString t = FORM_TITLE;
    t += " ";
    t += APP_VERSION;
    setWindowTitle(t);
}

void DlgBandsMaint::loadBandsTable()
{
    // initialize to first row - one more than header
    quint32 r = 0;  // set to zero to add header
    quint32 id = 0;
    quint32 ndx = 0;
    QString s = "";

    Bands band;
    QSqlQuery qry;

    qry =  band.findBands();   // generalize query of all bands in order by indx for display

    // check for valid query
    qry.first();

    if ( !qry.isValid() ) { return; }
    // we have results to show
    qry.last();
    qry.first();
    // add a row for results
    ui->tblBands->setRowCount(0);
    bm_data->font.setBold(true);
    ui->tblBands->setFont(bm_data->font);
    // build the header labels
    bm_data->colhdrs = tr("ID|Index|Label");
    QList<QString> lbls = bm_data->colhdrs.split("|",QString::KeepEmptyParts);
    ui->tblBands->setHorizontalHeaderLabels(lbls);
    bm_data->font.setBold(false);
    ui->tblBands->setFont(bm_data->font);
    ui->tblBands->setSortingEnabled(false);

    // now load the display with results
    r = 0;
    // - store first item in table
    ui->tblBands->insertRow(r);
    QTableWidgetItem *idItem;
    QTableWidgetItem *ndxItem;
    QTableWidgetItem *nameItem;

    // fill in blanks in new row
    id = qry.record().field("id").value().toInt();
    idItem   = new QTableWidgetItem(QString("%1").arg(id));
    ndx = qry.record().field("ndx").value().toInt();
    ndxItem  = new QTableWidgetItem(QString("%1").arg(ndx));
    nameItem = new QTableWidgetItem(qry.record().field("Label").value().toString());

    // fill in blanks in new row
    ui->tblBands->setItem(r,0,idItem);
    ui->tblBands->setItem(r,1,ndxItem);
    ui->tblBands->setItem(r,2,nameItem);
    qry.next(); // ready the next result for the loop
    do
    {
        if ( !qry.isValid() ) break;// ?EOF? //
        // resize table
        ++r;
        ui->tblBands->insertRow(r);
        // fill in blanks in new row
        id = qry.record().field("id").value().toInt();
        idItem   = new QTableWidgetItem(QString("%1").arg(id));
        ndx = qry.record().field("ndx").value().toInt();
        ndxItem  = new QTableWidgetItem(QString("%1").arg(ndx));
        QString nam = qry.record().field("Label").value().toString();
        nameItem = new QTableWidgetItem(nam);
        // fill new row with results
        ui->tblBands->setItem(r,0,idItem);
        ui->tblBands->setItem(r,1,ndxItem);
        ui->tblBands->setItem(r,2,nameItem);
        ui->tblBands->setRowHeight(r,22);
        // ready next result for display
        qry.next();
    } while ( qry.isValid() );
    // all rows shown now finish
    qry.finish();
    ui->tblBands->resizeColumnsToContents();
    ui->tblBands->setSortingEnabled(true);
    ui->tblBands->setCornerButtonEnabled(false);
    ui->tblBands->repaint();
}

void DlgBandsMaint::resizeTblCols()
{  // resize the log columns by stored string
    // set up color for alternate rows
    QPalette palette;
    QBrush altbrush;

    altbrush.setColor(bm_data->altColor);
    palette.setBrush(QPalette::AlternateBase, altbrush);
    ui->tblBands->setPalette(palette);
    ui->tblBands->setAlternatingRowColors(true);

    // force header font to following
    bm_data->font.setFamily(bm_data->font_family);
    bm_data->font.setPointSize(8);
    bm_data->font.setBold(true);
    bm_data->font.setItalic(false);
    bm_data->font.setUnderline(false);
    bm_data->font.setFamily(bm_data->font_family);
    bm_data->font.setPointSize(bm_data->font_size);
    bm_data->font.setItalic(bm_data->font_italic);
    bm_data->font.setUnderline(bm_data->font_underline);

    bm_data->font.setBold(true);
    ui->tblBands->setFont(bm_data->font);
    // build the header labels
    bm_data->colhdrs = "ID|Index|Label";
    QList<QString> lbls = bm_data->colhdrs.split("|",QString::KeepEmptyParts);
    ui->tblBands->setHorizontalHeaderLabels(lbls);
    bm_data->font.setBold(false);
    ui->tblBands->setFont(bm_data->font);
    ui->tblBands->setSortingEnabled(false);
}

void DlgBandsMaint::createObjects()
{
    ui->tblBands->setWindowTitle("Valid Bands Recorded");
    ui->tblBands->setAlternatingRowColors(true);
    ui->tblBands->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tblBands->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tblBands->setColumnCount(3);
    ui->tblBands->setRowCount(0);
    ui->tblBands->show();

    bm_data->needsSaving = false;
    ui->btnADD->setEnabled(true);
    ui->btnUpdt->setEnabled(false);
    ui->btnDel->setEnabled(false);
}

void DlgBandsMaint::connectSlots()
{
    connect(ui->btnQuit,  SIGNAL(clicked()), this,   SLOT(slotExit()));
    connect(ui->btnADD,   SIGNAL(clicked()), this,   SLOT(slotAddRec()));
    connect(ui->btnDel,   SIGNAL(clicked()), this,   SLOT(close()));
    connect(ui->btnUpdt,  SIGNAL(clicked()), this,   SLOT(slotUpdtRec()));
    connect(ui->btnCancel,SIGNAL(clicked()), this,   SLOT(slotCancel()));
    connect(ui->btnReset, SIGNAL(clicked()), this,   SLOT(slotReset()));
    connect(ui->tblBands,SIGNAL(itemSelectionChanged()),this,SLOT(slotRowSel()));

    QMetaObject::connectSlotsByName(this);
}

void DlgBandsMaint::displaySettings()
{
    move(bm_data->pos);
    resize(bm_data->size);

    ui->splitterWindow->restoreGeometry(bm_data->spltWindow);
    ui->splitterTop->restoreGeometry(bm_data->spltTop);

    QFont font;
    font.setFamily(bm_data->font_family);
    font.setPointSize(bm_data->font_size);
    font.setBold(bm_data->font_bold);
    font.setItalic(bm_data->font_italic);
    font.setUnderline(bm_data->font_underline);
    bm_data->altFont = font;
    ui->tblBands->setFont(font);
}

void DlgBandsMaint::saveColSizes()
{
    // save the log columns to stored string
    quint16 y = 0;
    quint16 z = 0;

    // saving current column widths
    bm_data->colwidths = "";

    for ( y = 0; y < 3; ++y )
    {
        z = ui->tblBands->columnWidth(y);
        if (z == 0) break;
        bm_data->colwidths += QString("%1").arg(z);
        if ( y != 3 ) bm_data->colwidths += "|";
    }
}

void DlgBandsMaint::readSettings()
{
    quint16 p;
    QSettings settings("AD5XJ", "QNetLog");

    settings.beginGroup("BandsMaint");
     bm_data->pos       = settings.value("pos", QPoint(100,200)).toPoint();
     bm_data->size      = settings.value("size", QSize(556,683)).toSize();
     bm_data->spltTop   = settings.value("splitTop",QByteArray("")).toByteArray();
     bm_data->spltWindow = settings.value("splitWindow",QByteArray("")).toByteArray();
     bm_data->colwidths = settings.value("ColWidths","33|68|108|82|43|60|100|336").toString();
     bm_data->color_r = settings.value("altRowColorR", "150").toInt();
     bm_data->color_g = settings.value("altRowColorG", "150").toInt();
     bm_data->color_b = settings.value("altRowColorB", "150").toInt();
     bm_data->color_a = settings.value("altRowColorA", "255").toInt();
     bm_data->altColor = QColor(bm_data->color_r,bm_data->color_g,bm_data->color_b,bm_data->color_a);
     bm_data->font_family = "Arial";
     bm_data->font_family = settings.value("font_family", "DejaVu Sans").toString();
     p = settings.value("font_point", 8).toInt();
     if ( p < 8 || p > 54 ) p = 10;
     bm_data->font_size  = p;
     bm_data->font_bold = false;
     if (settings.value("font_bold", "0").toString() == "1") bm_data->font_bold = true;
     bm_data->font_italic = false;
     if (settings.value("font_italic", "0").toString() == "1") bm_data->font_italic = true;
     bm_data->font_underline = false;
     if (settings.value("font_underline", "0").toString() == "1") bm_data->font_underline = true;
    settings.endGroup();
}

void DlgBandsMaint::saveSettings()
{
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("BandsMaint");
     settings.setValue("pos",           bm_data->pos);
     settings.setValue("size",          bm_data->size);
     settings.setValue("splitTop",      bm_data->spltTop);
     settings.setValue("splitWindow",   bm_data->spltWindow);
     settings.setValue("ColWidths",     bm_data->colwidths);
     settings.setValue("altRowColorR",  bm_data->color_r);
     settings.setValue("altRowColorG",  bm_data->color_g);
     settings.setValue("altRowColorB",  bm_data->color_b);
     settings.setValue("altRowColorA",  bm_data->color_a);
     settings.setValue("font_family",   bm_data->font_family);
     settings.setValue("font_point",    bm_data->font_size);
     settings.setValue("font_bold",     bm_data->font_bold);
     settings.setValue("font_italic",   bm_data->font_italic);
     settings.setValue("font_underline", bm_data->font_underline);
    settings.endGroup();
    settings.sync();
}

