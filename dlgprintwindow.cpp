#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtGui/QFont>
#include <QtGui/QPainter>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QProgressDialog>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlError>
#include <QtPrintSupport/QPrinterInfo>
#include <QtPrintSupport/QPrinter>

#include "sessionloghdr.hpp"
#include "sessionlogdtl.hpp"
#include "dlgprintwindow.hpp"

typedef QList<QFont *> AvailableFonts;

class DlgPrintWindow::dpPrivateData
{
public:
    // local vars on the stack
    int markedCount;

    QString print_dest;
    QString dest_path;

    QFont   currentFont;

    QRect page_view;

    QList<int> sampleSizes;
    QMap<QString, AvailableFonts> currentPageMap();
    QMap<QString, AvailableFonts> pageMap;
    // local vars on the heap
    QPrinter *printer;    //!< printer device selected from combo
    QPainter *painter;    //!< painter device doing the actual printing to any destination
};

DlgPrintWindow::DlgPrintWindow(QWidget *parent) : QMainWindow(parent)
{
    dp_data = new dpPrivateData;

    ui = new Ui::DlgPrintWindow;
    ui->setupUi(this);
    ui->retranslateUi(this);

    loadPrintersCombo();
    // Update UI when selected printer is changed.
    connect(ui->cboPrinters,SIGNAL(activated(QString)),this,SLOT(slotRefreshUi()));
    connect(ui->btnBrowse,SIGNAL(clicked(bool)),this,SLOT(slotBrowse()));
    connect(ui->actionDocSessionLog,SIGNAL(triggered(bool)),this,SLOT(slotSessionLog()));

    // set up initial state of controls and widgets
    ui->cboPrinters->setCurrentIndex(0);
    ui->btnBrowse->setEnabled(true);
    ui->txtLoc->setEnabled(false);
}

DlgPrintWindow::~DlgPrintWindow()
{
    delete ui;
    delete dp_data;
}

void DlgPrintWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
void DlgPrintWindow::slotSessionLog()
{
//    dp_data->pageMap = dp_data->currentPageMap();

//    if (dp_data->pageMap.count() == 0) return;

    printSessionLog();
}
#endif

void DlgPrintWindow::slotBrowse()
{
    // Browse button pressed
    qDebug() << "Browse button pressed...";
    QString path = QDir::currentPath();
    path += "/";
    QString directory = QFileDialog::getExistingDirectory(this,tr("Find Files"), path);

    if ( !directory.isEmpty() )
    {
        ui->txtLoc->setText(directory);
    }
    else
    {
        ui->txtLoc->setText("?");
    }
    ui->txtLoc->setEnabled(true);
}

void DlgPrintWindow::slotPrinterInfo()
{
    if ( QPrinterInfo::availablePrinterNames().isEmpty() )
    {
        QMessageBox::critical(this,
                              tr("Error"),
                              tr("No printers found. The application will not display any useful information.")
                             );
    }

    // Initially display information for first printer.
    slotRefreshUi();
}

void DlgPrintWindow::slotRefreshUi()
{
    qDebug() << "Combo Printer names select triggered...";
    // We get the currently selected printer's name from the combo box,
    // then get the QPrinterInfo object for that printer by calling the
    // static method QPrinterInfo::printerInfo.
    QString name = ui->cboPrinters->currentText();
    QPrinterInfo info;
    qDebug() << "printer: " << name;
    // we need to know if this is a printer device or a file destination
    if ( name.left(4) == "FILE" )
    {
        // we must test for a valid QDir path if file destination
        // the default "?" indicates no destination has been set

        // Set up the display for file location
        ui->txtRemote->setText(tr("NO"));
        ui->txtDescription->setText(tr("Print to file (PDF) in location below"));
        ui->txtMakeModel->setText(tr("PDF painter"));
        ui->txtPageMin->setText(tr("Auto"));
        ui->txtPgMax->setText(tr("Auto"));
        ui->txtPageSize->setText(tr("Default"));
    }
    else
    {
        info = QPrinterInfo::printerInfo(name);
        ui->txtDescription->setText(info.description());
        // We call various QPrinterInfo methods like description() and fill
        // in the user interface. Some of these methods are new in Qt 5.3
        ui->txtDefault->setText(info.isDefault() ? tr("Yes") : tr("No"));
        ui->txtNull->setText(info.isNull() ? tr("Yes") : tr("No"));
        ui->txtRemote->setText(info.isRemote() ? tr("Yes") : tr("No"));
        ui->txtLoc->setText(info.location());  // net url or path if file
        ui->txtMakeModel->setText(info.makeAndModel());

        // It is good practice to make sure the object is valid, so we call
        // isNull to do a sanity check in case there is no printer info.
        if ( info.isNull() ) { return; }
        // The method to return the printer state is new in Qt 5.3.
        // We display it using suitable (localized) text. Since this
        // can changed dynamically, ideally it should be updated with
        // a timer so that it reflects the current status when it
        // changes. This is simple to do, but I omitted it as it would
        // obscure the clarity of the code. Maybe in next version update.
        switch (info.state())
        {
            case QPrinter::Idle:
                ui->txtStatus->setText(tr("Idle"));
                break;
            case QPrinter::Active:
                ui->txtStatus->setText(tr("Active"));
                break;
            case QPrinter::Aborted:
                ui->txtStatus->setText(tr("Aborted"));
                break;
            case QPrinter::Error:
                ui->txtStatus->setText(tr("Error"));
                break;
            default:
                ui->txtStatus->setText(tr("Unknown"));
                break;
        }
        // All of the methods below are new in Qt 5.3.
        ui->txtPageSize->setText(info.defaultPageSize().name());
        ui->txtPageMin->setText(info.minimumPhysicalPageSize().name());
        ui->txtPgMax->setText(info.maximumPhysicalPageSize().name());
        // We must test for both machine or file destination before
        // trying to test for this ability.
//    ui->customPageSizesLabel->setText(info.supportsCustomPageSizes() ? tr("Yes") : tr("No"));
        // We fill in the combox box with a description of the screen resolutions.
        ui->cboRes->clear();
        foreach ( int res, info.supportedResolutions() )
        {
            ui->cboRes->addItem(QString::number(res) + tr(" dpi"));
        }

        // We do the same for the page sizes.
        ui->cboPgSizes->clear();
        foreach ( QPageSize size, info.supportedPageSizes() )
        {
            ui->cboPgSizes-> addItem(size.name());
        }
    }
}

void DlgPrintWindow::loadPrintersCombo()
{
    ui->cboPrinters->addItem("FILE (PDF *.pdf)","");
    ui->cboPrinters->addItems(QPrinterInfo::availablePrinterNames());
}

void DlgPrintWindow::printSessionLog()
{
    bool firstPage = true;
    int page = 0;
    QPrinter printer; //create your QPrinter
    printer.setOutputFileName("output.pdf");
    printer.setResolution(QPrinter::HighResolution);
    printer.setPageSize(QPrinter::Letter);
    printer.setOrientation(QPrinter::Portrait);
    printer.setPageMargins (1.5,1.5,1.5,1.5,QPrinter::Inch);
    printer.setFullPage(false);
    printer.setOutputFormat(QPrinter::PdfFormat); //you can use native format of system usin QPrinter::NativeFormat
    dp_data->page_view = printer.pageRect();

    QPainter painter; // create a painter which will paint 'on printer'.

    QProgressDialog progress(tr("Preparing Session Log Report..."),
                             tr("&Cancel"),
                             0,
                             dp_data->pageMap.count(),
                             this
                            );
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setWindowTitle(tr("Printing Session Log"));

    painter.begin(&printer);

    firstPage = true;
    for ( page = printer.fromPage(); page <= printer.toPage(); ++page )
    {
        if ( progress.wasCanceled() )    break;
        if (!firstPage)                  printer.newPage();

        qApp->processEvents();

        printLogPage(page - 1, &painter, &printer, firstPage);
        progress.setValue(page);
        firstPage = false;
    }
    painter.end();
    close();
}

void DlgPrintWindow::printLogPage(int index, QPainter *painter, QPrinter *printer, bool fstPage)
{
    bool  firstPage = fstPage;
    bool  italic = false;
    int   font_pointsize  = 22;
    int   weight = Qt::DisplayRole;
    int   pu = 0;
    qreal w = 0.0;
    qreal h = 0.0;
    QString family = "DejaVu Sans"; //ui->fontComboBox->currentText();
    QString style = "normal";
    QString title = tr("Sesson Log");

    AvailableFonts items = dp_data->pageMap[family];

    // Find the dimensions of the text on each page.
    // current measurement units will be converted to points
    int units = printer->pageLayout().units();
    switch ( units )
    {
    case QPrinter::Inch:
        pu = 72; // points per inch
        break;
    case QPrinter::Millimeter:
        pu = int((1 / 25) * 72); // points per mm
        break;
    case QPrinter::Didot:
        pu = int(((1 / 25) *  .375) * 72); // points per french inch 0.375mm
        break;
    case QPrinter::Cicero:
        pu = int(((1 / 25) * 4.5) * 72);   // points per french inch 12 Didot 4.5 mm
        break;
    default: // default to inch
        pu = 72; // points per inch
        break;
    }
    int pw = dp_data->page_view.width();
    int ph = dp_data->page_view.height();
    w = pu * pw; // width in page units * points per unit
    h = pu * ph; // height in page units * points per unit

    QFont font;
    font.setFamily(family);
    font.setPointSize(font_pointsize);
    font.setWeight(weight);
    font.setItalic(italic);
    font.setStyleName(style);
    QFontMetricsF fm(font);


    // Calculate the maximum width and total height of the text.
    qreal cw = fm.averageCharWidth();
    qreal fh = fm.height();
    qreal fs = fm.lineSpacing();
    qreal font_height = fh + fs;
    // (
    //   ((pagewidth_points / 2) x charwidth_points -
    //   ((titlelen_chars / 2)   x charwidthpoints )
    //
    qreal title_len = (title.length() / 2.0)  * cw;
    qreal page_ptctr = w / 2;
    qreal x = page_ptctr - title_len;
    qreal y = font_height * 2;

    qreal xScale = pw / w;
    qreal yScale = ph / h;
    qreal scale = qMin(xScale, yScale);

//    qreal remainingHeight = (printer->pageRect().height() / scale) - h;
    //qreal spaceHeight = (remainingHeight / 4.0) + 1;
    qreal interLineHeight = (fm.lineSpacing());

    painter->save();
    painter->translate(printer->pageRect().width() / 2.0, printer->pageRect().height() / 2.0);
    painter->scale(scale, scale);
    painter->setBrush(QBrush(Qt::black));


    if ( firstPage )
    {
        font.setPointSize(22);
        font.setBold(true);
        painter->setFont(font);
        // format data for header
        painter->drawText(4,2,title);
        font.setBold(false);
        font.setPointSize(font_pointsize);
        painter->setFont(font);
    }
    /*
    // create database query for log header
    SessionHdrRecord hdrrec;
    hdrrec.id = 13; // fixed for now...will select from list later
    SessionHeader hdr;
    QSqlQuery hdrs = hdr.findHdrRecord(hdrrec);
    QString SessionName = hdrs.record().field("")
    // print header data
    // create database query for log detail data
    //   read data for the selected header
    //   format data for each line
    //   Draw each line of text.
    foreach (int size, sampleSizes)
    {
        QFont font(family, size, weight, italic);
        font.setStyleName(style);
        font = QFont(font, painter->device());
        QFontMetricsF fontMetrics(font);
        QRectF rect = fontMetrics.boundingRect(QString("%1 %2").arg(
                          font.family()).arg(style));
        y += rect.height();
        painter->setFont(font);
        painter->drawText(QPointF(x, y), QString("%1 %2").arg(family).arg(style));
        y += interLineHeight;
        y += spaceHeight;
    }
    */
    painter->restore();
}

