#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtWidgets/QMessageBox>
#include <QtSql/QSqlTableModel>
#include <QtWidgets/QTableWidgetItem>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>

#include "callsigns.hpp"
#include "dlgcallsigns.hpp"

#define FORM_TITLE "Call Signs Maintenance"

class dlgCallSigns::cmPrivateData
{
public:
    bool inAddMode;              //!<
    bool formLoaded;             //!<
    bool m_utc;                  //!<
    bool needsSaving;            //!<
    bool AddMode;                //!<
    bool ChgMode;                //!<
    bool DelMode;                //!<
    bool font_bold;              //!<
    bool font_underline;         //!<
    bool font_italic;            //!<

    quint16 font_size;           //!<
    quint16 color_r;             //!<
    quint16 color_g;             //!<
    quint16 color_b;             //!<
    quint16 color_a;             //!<

    QFont font;                  //!<
    QFont altFont;               //!<

    QColor altColor;             //!<

    QPoint pos;                  //!<
    QSize  size;                 //!<

    QString colwidths;           //!<
    QString colhdrs;             //!<
    QString font_family;         //!<

    QByteArray spltWindow;       //!<
    QByteArray spltTop;          //!<

    int recIndex;                //!<
    int row_count;               //!<
};

dlgCallSigns::dlgCallSigns(QWidget *parent) : QDialog(parent)
{
    cm_data = new cmPrivateData;

    setModal(true);

    ui = new Ui::dlgCallSigns;
    ui->setupUi(this);
    ui->retranslateUi(this);

    createObjects();

    readSettings();

    displaySettings();

    connectSlots();

    cm_data->inAddMode = false;
    cm_data->AddMode = false;
    cm_data->ChgMode = false;
    cm_data->DelMode = false;

    clearForm();
    resetButtons();
    ui->tblViewCalls->clearContents();
    resizeTblCols();
    loadCallsignsTable();

    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

dlgCallSigns::~dlgCallSigns()
{
    delete ui;
    delete cm_data;
}

void dlgCallSigns::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void dlgCallSigns::resizeEvent(QResizeEvent *)
{
    cm_data->pos = pos();
    cm_data->size = size();
    ui->splitter_2->setGeometry(3,3,width()-12,height()-20);
    cm_data->spltWindow = ui->splitter_2->saveGeometry();
    cm_data->spltTop = ui->splitter->saveGeometry();
    saveColSizes();
    saveSettings();
}

void dlgCallSigns::closeEvent(QCloseEvent *)
{
    // save the window geometry
    cm_data->pos = pos();
    cm_data->size = size();
    // preserve the splitter geometry
    cm_data->spltTop = ui->splitter->saveGeometry();
    cm_data->spltWindow = ui->splitter_2->saveGeometry();
    // save table column widths
    saveColSizes();
    // make it permanent before the deconstructor fires
    saveSettings();
}

void dlgCallSigns::showEvent(QShowEvent *)
{
    cm_data->formLoaded = true;
}

void dlgCallSigns::slotRowSel()
{
    // get which row is selected
    int x = ui->tblViewCalls->currentRow();
    if ( x < 0 ) return;  // nothing selected so return

    // display fields and set buttons to edit mode
    cm_data->recIndex = ui->tblViewCalls->item(x,0)->text().toInt();
    ui->editID->setText(QString("%1").arg(cm_data->recIndex));
    ui->editCallsign->setText(ui->tblViewCalls->item(x,1)->text());
    ui->editOperName->setText(ui->tblViewCalls->item(x,2)->text());
    ui->editCity->setText(ui->tblViewCalls->item(x,3)->text());
    ui->editSt->setText(ui->tblViewCalls->item(x,4)->text());
    ui->editGrid->setText(ui->tblViewCalls->item(x,5)->text());

    ui->btnAdd->setEnabled(false);
    ui->btnUpdt->setEnabled(true);
    ui->btnDel->setEnabled(true);
    ui->btnReset->setEnabled(true);
    ui->btnCancel->setEnabled(false);
    ui->btnExit->setEnabled(false);

    QString t = FORM_TITLE + tr(" - Call Selected: ");
    t += ui->editCallsign->text().trimmed();
    setWindowTitle(t);
}

void dlgCallSigns::slotReset()
{
    ui->tblViewCalls->clearContents();
    resizeTblCols();
    loadCallsignsTable();

    cm_data->inAddMode = false;
    cm_data->AddMode = false;
    cm_data->ChgMode = false;
    cm_data->DelMode = false;

    clearForm();
    resetButtons();
}

void dlgCallSigns::slotCancel()
{
    cm_data->inAddMode = false;
    cm_data->AddMode = false;
    cm_data->ChgMode = false;
    cm_data->DelMode = false;

    clearForm();
    resetButtons();

    ui->tblViewCalls->clear();
    resizeTblCols();
    loadCallsignsTable();
    QString t = FORM_TITLE;
    t += " ";
    t += APP_VERSION;
    setWindowTitle(t);
}

void dlgCallSigns::slotExit()
{
    ui->btnAdd->setText(tr("ADD"));
    ui->btnExit->setText(tr("QUIT"));
    ui->btnDel->setEnabled(true);
    ui->btnCancel->setEnabled(true);
    if ( !cm_data->inAddMode &&
         !cm_data->DelMode   &&
         !cm_data->ChgMode
       ) close();
    cm_data->inAddMode = false;
}

void dlgCallSigns::slotDelRec()
{
    if ( !cm_data->DelMode )
    {
        QString t = FORM_TITLE + tr(" - CAUTION DELETING! ! !");
        this->setWindowTitle(t);
        // disable buttons until done
        ui->btnAdd->setEnabled(false);
        ui->btnUpdt->setEnabled(false);
        ui->btnDel->setEnabled(true);
        ui->btnExit->setEnabled(false);
        ui->btnCancel->setVisible(true);
        ui->btnDel->setText(tr("COMMIT"));
        cm_data->DelMode = true;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("You are about to permanently delete this record.");
        msgBox.setInformativeText("Is this what you really want to do?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::No);
        int ret = msgBox.exec();

        switch ( ret  )
        {
        case QMessageBox::Yes:
        {
            CallSigns calls;
            CallSignsRecord rec;

            rec.ID = ui->editID->text().toInt();
            calls.del(rec); // save the data from the form
            cm_data->DelMode = false;
        }
        case QMessageBox::No:
            cm_data->DelMode = false;
            break;
        case QMessageBox::Cancel:
            slotReset();
            cm_data->DelMode = false;
            clearForm();
            break;
        }
        clearForm();
    }
}

void dlgCallSigns::slotUpdtRec()
{
    if ( cm_data->ChgMode )
    { // save data from form to calls table
        updtDataFromForm();

        cm_data->ChgMode = false;

        clearForm();
        resetButtons();

        ui->tblViewCalls->clear();
        resizeTblCols();
        loadCallsignsTable();
    }
    else
    {
        // not in update mode yet so get it set up
        ui->btnUpdt->setText(tr("SAVE"));
        ui->btnUpdt->setEnabled(true);
        ui->btnAdd->setEnabled(false);
        ui->btnDel->setEnabled(false);
        ui->btnExit->setEnabled(false);
        ui->btnCancel->setEnabled(true);
        ui->btnReset->setEnabled(false);
        ui->editCallsign->setEnabled(true);
        ui->editCity->setEnabled(true);
        ui->editGrid->setEnabled(true);
        ui->editOperName->setEnabled(true);
        ui->editSt->setEnabled(true);
        QString t = FORM_TITLE + tr(" - UPDATE Mode - ");
        t += ui->editCallsign->text().trimmed();
        setWindowTitle(t);
        cm_data->ChgMode = true;
    }
}

void dlgCallSigns::slotAddRec()
{
    if (!cm_data->inAddMode)
    { // save data from form to callsigns table
        saveDataFromForm();

        ui->btnAdd->setText(tr("ADD"));
        ui->btnExit->setText(tr("QUIT"));

        cm_data->inAddMode = false;
        cm_data->AddMode = false;

        clearForm();
        resetButtons();

        ui->tblViewCalls->clearContents();
        resizeTblCols();
        loadCallsignsTable();
    }
    else
    {
        clearForm();

        ui->editCallsign->setEnabled(true);
        ui->editCity->setEnabled(true);
        ui->editGrid->setEnabled(true);
        ui->editOperName->setEnabled(true);
        ui->editSt->setEnabled(true);

        ui->btnAdd->setText(tr("SAVE"));
        ui->btnDel->setEnabled(false);
        ui->btnUpdt->setEnabled(false);
        ui->btnCancel->setEnabled(false);
        ui->btnReset->setEnabled(true);
        ui->btnExit->setText(tr("CANCEL"));

        cm_data->inAddMode = true;
        cm_data->AddMode = true;

        QString t = FORM_TITLE + tr("ADD Mode");
        setWindowTitle(t);
    }
}

void dlgCallSigns::updtDataFromForm()
{
    CallSigns       calls(this);
    CallSignsRecord rec;

    rec.ID   = cm_data->recIndex;
    rec.Call = ui->editCallsign->text().trimmed();
    rec.Name = ui->editOperName->text().trimmed();
    rec.City = ui->editCity->text().trimmed();
    rec.St   = ui->editSt->text().trimmed();
    rec.Grid = ui->editGrid->text().trimmed();

    calls.update(rec);
}

void dlgCallSigns::saveDataFromForm()
{
    CallSigns       calls(this);
    CallSignsRecord rec;

    rec.ID = 0;
    rec.Call = ui->editCallsign->text().trimmed();
    rec.City = ui->editCity->text().trimmed();
    rec.Grid = ui->editGrid->text().trimmed();
    rec.Name = ui->editOperName->text().trimmed();

    calls.save(rec);

    cm_data->inAddMode = false;
    cm_data->AddMode = false;
    cm_data->ChgMode = false;
    cm_data->DelMode = false;
}

void dlgCallSigns::resetButtons()
{
    ui->btnAdd->setText(tr("ADD"));
    ui->btnUpdt->setText(tr("UPDATE"));
    ui->btnExit->setText(tr("QUIT"));

    ui->btnAdd->setEnabled(true);
    ui->btnUpdt->setEnabled(false);
    ui->btnDel->setEnabled(false);
    ui->btnCancel->setEnabled(false);
    ui->btnReset->setEnabled(false);
    ui->btnExit->setEnabled(true);
}

void dlgCallSigns::clearForm()
{
    ui->editCallsign->setText("");
    ui->editCity->setText("");
    ui->editGrid->setText("");
    ui->editOperName->setText("");
    ui->editSt->setText("");

    QString t = FORM_TITLE;
    t += " ";
    t += APP_VERSION;
    setWindowTitle(t);

    ui->editCallsign->setEnabled(false);
    ui->editCity->setEnabled(false);
    ui->editGrid->setEnabled(false);
    ui->editOperName->setEnabled(false);
    ui->editSt->setEnabled(false);

    resetButtons();
    loadCallsignsTable();
}

void dlgCallSigns::loadCallsignsTable()
{
    // initialize to first row - one more than header
    int r = ui->tblViewCalls->rowCount();
    int id = 0;
    QString s = "";

    CallSigns calls;
    QSqlQuery qry;

    qry =  calls.findAll();   // generalize query of all nets in order by name for display

    // check for valid query
    qry.first();

    if ( !qry.isValid() ) { return; }
    // we have results to show
    qry.last();
    quint16 cnt = qry.record().count();
    qry.first();
    // add a row for results
    ui->tblViewCalls->setRowCount(cnt+1);
    // now load the display with results
    r = 0;
    // - store first item in table
    QTableWidgetItem *idItem;
    QTableWidgetItem *callItem;
    QTableWidgetItem *nameItem;
    QTableWidgetItem *marsItem;
    QTableWidgetItem *cityItem;
    QTableWidgetItem *stateItem;
    QTableWidgetItem *gridItem;
    QTableWidgetItem *notesItem;

    // fill in blanks in new row
    id = qry.record().field("id").value().toInt();
    idItem   = new QTableWidgetItem(QString("%1").arg(id));
    callItem = new QTableWidgetItem(qry.record().field("call").value().toString());
    nameItem = new QTableWidgetItem(qry.record().field("operator").value().toString());
    cityItem = new QTableWidgetItem(qry.record().field("city").value().toString());
    stateItem = new QTableWidgetItem(qry.record().field("st").value().toString());
    gridItem = new QTableWidgetItem(qry.record().field("grid").value().toString());
    marsItem = new QTableWidgetItem(qry.record().field("marscall").value().toString());
    notesItem = new QTableWidgetItem(qry.record().field("notes").value().toString());
    idItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);
    stateItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);
    gridItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);

    // fill in blanks in new row
    ui->tblViewCalls->setItem(r,0,idItem);
    ui->tblViewCalls->setItem(r,1,callItem);
    ui->tblViewCalls->setItem(r,2,nameItem);
    ui->tblViewCalls->setItem(r,3,cityItem);
    ui->tblViewCalls->setItem(r,4,stateItem);
    ui->tblViewCalls->setItem(r,5,gridItem);
    ui->tblViewCalls->setItem(r,6,marsItem);
    ui->tblViewCalls->setItem(r,7,notesItem);
    ui->tblViewCalls->setRowHeight(r,22);
    qry.next(); // ready the next result for the loop
    for ( r = 1; r < cnt; ++r )
    {
        if ( !qry.isValid() ) break;// ?EOF? //
        // resize table
        // fill in blanks in new row
        id = qry.record().field("id").value().toInt();
        idItem   = new QTableWidgetItem(QString("%1").arg(id));
        callItem = new QTableWidgetItem(qry.record().field("call").value().toString());
        nameItem = new QTableWidgetItem(qry.record().field("operator").value().toString());
        cityItem = new QTableWidgetItem(qry.record().field("city").value().toString());
        stateItem = new QTableWidgetItem(qry.record().field("st").value().toString());
        gridItem = new QTableWidgetItem(qry.record().field("grid").value().toString());
        marsItem = new QTableWidgetItem(qry.record().field("marscall").value().toString());
        notesItem = new QTableWidgetItem(qry.record().field("notes").value().toString());
        idItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);
        stateItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);
        gridItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignCenter);
        // fill new row with results
        ui->tblViewCalls->setItem(r,0,idItem);
        ui->tblViewCalls->setItem(r,1,callItem);
        ui->tblViewCalls->setItem(r,2,nameItem);
        ui->tblViewCalls->setItem(r,3,cityItem);
        ui->tblViewCalls->setItem(r,4,stateItem);
        ui->tblViewCalls->setItem(r,5,gridItem);
        ui->tblViewCalls->setItem(r,6,marsItem);
        ui->tblViewCalls->setItem(r,7,notesItem);
        ui->tblViewCalls->setRowHeight(r,22);
        // ready next result for display
        qry.next();
        if ( !qry.isValid() ) break;
    };
    // all rows shown now finish
    qry.finish();
    ui->tblViewCalls->resizeColumnsToContents();
    ui->tblViewCalls->setSortingEnabled(true);
    ui->tblViewCalls->setCornerButtonEnabled(false);
    ui->tblViewCalls->repaint();
}

void dlgCallSigns::resizeTblCols()
{  // resize the log columns by stored string
    // set up color for alternate rows
    QPalette palette;
    QBrush altbrush;
    altbrush.setColor(cm_data->altColor);
    palette.setBrush(QPalette::AlternateBase, altbrush);
    ui->tblViewCalls->setPalette(palette);
    ui->tblViewCalls->setAlternatingRowColors(true);

    // force header font to following
    cm_data->font.setFamily(cm_data->font_family);
    cm_data->font.setPointSize(8);
    cm_data->font.setBold(true);
    cm_data->font.setItalic(false);
    cm_data->font.setUnderline(false);
    cm_data->font.setFamily(cm_data->font_family);
    cm_data->font.setPointSize(cm_data->font_size);
    cm_data->font.setItalic(cm_data->font_italic);
    cm_data->font.setUnderline(cm_data->font_underline);

    cm_data->font.setBold(true);
    ui->tblViewCalls->setFont(cm_data->font);
    // build the header labels
    cm_data->colhdrs = "ID|Callsign|Operator|City|State|Grid |MARS Call|      N o t e s        ";
    QList<QString> lbls = cm_data->colhdrs.split("|",QString::KeepEmptyParts);
    ui->tblViewCalls->setHorizontalHeaderLabels(lbls);
    cm_data->font.setBold(false);
    ui->tblViewCalls->setFont(cm_data->font);
    ui->tblViewCalls->setSortingEnabled(false);
/*
    QStringList col_list = cm_data->colwidths.split("|",QString::KeepEmptyParts);
    int x = 0;
    int y = 0;
    for ( y = 0; y < 7; ++y )
    {
        x = col_list.at(y).toInt();
        ui->tblViewCalls->setColumnWidth(y,x);
    }
*/
}

void dlgCallSigns::createObjects()
{
    ui->tblViewCalls->setWindowTitle("Valid Callsigns Recorded");
    ui->tblViewCalls->setAlternatingRowColors(true);
    ui->tblViewCalls->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tblViewCalls->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tblViewCalls->setColumnCount(8);
    ui->tblViewCalls->setRowCount(0);
    ui->tblViewCalls->show();

    cm_data->needsSaving = false;
    ui->btnAdd->setEnabled(true);
    ui->btnUpdt->setEnabled(false);
    ui->btnDel->setEnabled(false);
}

void dlgCallSigns::connectSlots()
{
    connect(ui->btnExit,  SIGNAL(clicked()), this,   SLOT(slotExit()));
    connect(ui->btnAdd,   SIGNAL(clicked()), this,   SLOT(slotAddRec()));
    connect(ui->btnDel,   SIGNAL(clicked()), this,   SLOT(slotDelRec()));
    connect(ui->btnUpdt,  SIGNAL(clicked()), this,   SLOT(slotUpdtRec()));
    connect(ui->btnCancel,SIGNAL(clicked()), this,   SLOT(slotCancel()));
    connect(ui->btnReset, SIGNAL(clicked()), this,   SLOT(slotReset()));
    connect(ui->tblViewCalls,SIGNAL(itemSelectionChanged()),this,SLOT(slotRowSel()));

    QMetaObject::connectSlotsByName(this);
}

void dlgCallSigns::displaySettings()
{
    move(cm_data->pos);
    resize(cm_data->size);

    ui->splitter->restoreGeometry(cm_data->spltTop);
    ui->splitter_2->restoreGeometry(cm_data->spltWindow);

    QFont font;
    font.setFamily(cm_data->font_family);
    font.setPointSize(cm_data->font_size);
    font.setBold(cm_data->font_bold);
    font.setItalic(cm_data->font_italic);
    font.setUnderline(cm_data->font_underline);
    cm_data->altFont = font;
    ui->tblViewCalls->setFont(font);
}

void dlgCallSigns::saveColSizes()
{
    // save the log columns to stored string
    quint16 y = 0;
    quint16 z = 0;

    // saving current column widths
    cm_data->colwidths = "";

    for ( y = 0; y < 7; ++y )
    {
        z = ui->tblViewCalls->columnWidth(y);
        if (z == 0) break;
        cm_data->colwidths += QString("%1").arg(z);
        if ( y != 7 ) cm_data->colwidths += "|";
    }
}

void dlgCallSigns::readSettings()
{
    quint16 p;
    QSettings settings("AD5XJ", "QNetLog");

    settings.beginGroup("CallMaint");
     cm_data->pos       = settings.value("pos", QPoint(100,200)).toPoint();
     cm_data->size      = settings.value("size", QSize(556,683)).toSize();
     cm_data->spltTop   = settings.value("splitTop",QByteArray("")).toByteArray();
     cm_data->spltWindow = settings.value("splitWindow",QByteArray("")).toByteArray();
     cm_data->colwidths = settings.value("ColWidths","33|68|108|82|43|60|100|336").toString();
     cm_data->color_r = settings.value("altRowColorR", "150").toInt();
     cm_data->color_g = settings.value("altRowColorG", "150").toInt();
     cm_data->color_b = settings.value("altRowColorB", "150").toInt();
     cm_data->color_a = settings.value("altRowColorA", "255").toInt();
     cm_data->altColor = QColor(cm_data->color_r,cm_data->color_g,cm_data->color_b,cm_data->color_a);
     cm_data->font_family = "Arial";
     cm_data->font_family = settings.value("font_family", "DejaVu Sans").toString();
     p = settings.value("font_point", 8).toInt();
     if ( p < 8 || p > 54 ) p = 10;
     cm_data->font_size  = p;
     cm_data->font_bold = false;
     if (settings.value("font_bold", "0").toString() == "1") cm_data->font_bold = true;
     cm_data->font_italic = false;
     if (settings.value("font_italic", "0").toString() == "1") cm_data->font_italic = true;
     cm_data->font_underline = false;
     if (settings.value("font_underline", "0").toString() == "1") cm_data->font_underline = true;
    settings.endGroup();
}

void dlgCallSigns::saveSettings()
{
    QSettings settings("AD5XJ", "QNetLog");
    settings.beginGroup("CallMaint");
     settings.setValue("pos",           cm_data->pos);
     settings.setValue("size",          cm_data->size);
     settings.setValue("splitTop",      cm_data->spltTop);
     settings.setValue("splitWindow",   cm_data->spltWindow);
     settings.setValue("ColWidths",     cm_data->colwidths);
     settings.setValue("altRowColorR",  cm_data->color_r);
     settings.setValue("altRowColorG",  cm_data->color_g);
     settings.setValue("altRowColorB",  cm_data->color_b);
     settings.setValue("altRowColorA",  cm_data->color_a);
     settings.setValue("font_family",   cm_data->font_family);
     settings.setValue("font_point",    cm_data->font_size);
     settings.setValue("font_bold",     cm_data->font_bold);
     settings.setValue("font_italic",   cm_data->font_italic);
     settings.setValue("font_underline", cm_data->font_underline);
    settings.endGroup();
    settings.sync();
}
