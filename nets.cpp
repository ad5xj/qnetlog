#include <QtCore/QDebug>
#include <QtSql/QSqlError>

#include "nets.hpp"

Nets::Nets(QObject *parent) : QObject(parent)
{
    // Nets database maintenance implementation
}

Nets::~Nets()
{
    //
}

bool Nets::del(NetsRecord rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    if ( rec.id <= 0 )
    {
        qDebug() << "DB Error no record ID specified: err - " << err << " " << errmsg;
        return success;
    }

    sqlStr  = "DELETE FROM nets ";
    sqlStr += "WHERE id=" + QString("%1").arg(rec.id) + ", ');";
    //qDebug() << "preparing ADD SQL:" << sqlStr;

    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
      success = true;
    }
    else
    {
        qDebug() << "DB Error deleting Nets data: err - " << err << " " << errmsg;
        success = false;
    }
    return success;
}

bool Nets::save(NetsRecord rec)
{
    bool success = false;
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString sqlStr;

    // the ID primary key is autoincrement so we do not supply a value here
    sqlStr  = "INSERT INTO nets ";
    sqlStr += "(";
    sqlStr += " timebase,";
    sqlStr += " occurs,";
    sqlStr += " dayofweek,";
    sqlStr += " netband,";
    sqlStr += " netmode,";
    sqlStr += " netname,";
    sqlStr += " netsttime, ";
    sqlStr += " netfreq,";
    sqlStr += " netnotes) ";
    sqlStr += "VALUES (";
    if (rec.TimeBase)
        sqlStr += "1, ";
    else
        sqlStr += "0, ";
    // indicates which chk box was used 1-Dly 2-WkDays 3-Wkly 4-BiWkly 5-Mthly
    sqlStr += QString("%1").arg(rec.Occurs) + ", ";
    // packed bits one for each day of the week
    sqlStr += QString("%1").arg(rec.DayOfWeek) + ", ";
    // indicates which radio btn was used 1-SSB 2-digital 3-AM 4-FM
    sqlStr += QString("%1").arg(rec.NetMode);
    sqlStr += ", '" + rec.NetBand;
    sqlStr += "', '" + rec.NetStTime.toString("hh:mm AP") + "', ";
    sqlStr += "'" + rec.NetName + "', ";  // relational index to nets ID
    sqlStr += "'" + rec.NetFrequency + "', ";
    sqlStr += "'" + rec.NetNotes + "');";
    qDebug() << "preparing ADD SQL:" << sqlStr;
    qry.prepare(sqlStr);
    //qDebug() << "executing...";
    qry.exec();
    err = qry.lastError().type();
    errmsg = qry.lastError().text();
    if ( err == QSqlError::NoError )
    {
        err = QSqlError::NoError;
        errmsg = "";
        success = true;
    }
    else
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        success = false;
    }
    return success;
}

bool Nets::update(NetsRecord rec)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML = "";

    strDML  = "UPDATE nets SET ";
    strDML += "timebase=";
    if (rec.TimeBase)
        strDML += "1, ";
    else
        strDML += "0, ";
    strDML += "occurs="     + QString("%1 ").arg(rec.Occurs) + ", ";
    strDML += "dayofweek="  + QString("%1 ").arg(rec.DayOfWeek) + ", ";
    strDML += "netband="    + QString("%1 ").arg(rec.NetBand) + ", ";
    strDML += "netmode="    + QString("%1 ").arg(rec.NetMode) + ", ";
    strDML += "netname='"   + rec.NetName + "', ";
    strDML += "netsttime='" + rec.NetStTime.toString("hh:mm AP") + "', ";
    strDML += "netfreq='"   + rec.NetFrequency + "', ";
    strDML += "netnotes='"  + rec.NetNotes + "' ";
    strDML += "WHERE id="   + QString("%1").arg(rec.id) + "; ";
    //qDebug() <<  "preparing UPDT SQL:" << sqlStr;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        err = QSqlError::NoError;
        errmsg = "";
        return true;
    }
    else
    {
        err = qry.lastError().number();
        errmsg = qry.lastError().text();
        return false;
    }
}

bool Nets::findNetsCallSign(NetsRecord)
{
    // find all nets where callsign appears as checkin
    return false;
}

QSqlQuery Nets::findNetsRecord(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT id,";
    strDML += " timebase,";
    strDML += " occurs, ";
    strDML += " dayofweek, ";
    strDML += " netname,";
    strDML += " netfreq,";
    strDML += " netband, ";
    strDML += " netmode,";
    strDML += " netsttime,";
    strDML += " netnotes ";
    strDML += " FROM nets ";
    strDML += " WHERE id = ";
    strDML += QString("%1").arg(id);
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(strDML);
    qry.exec();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        err = QSqlError::NoError;
        errmsg = "";
    }
    else
    {
        err = qry.lastError().type();
        errmsg = qry.lastError().text();
        qDebug() << "Nets query failure:" << QString("%1").arg(qry.lastError().type())
                 << " - " << qry.lastError().text()
                 << "SQL:" << strDML;
    }
    return qry;
}

QSqlQuery Nets::findNetID(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT id,";
    strDML += " timebase,";
    strDML += " occurs, ";
    strDML += " dayofweek, ";
    strDML += " netname,";
    strDML += " netfreq,";
    strDML += " netband, ";
    strDML += " netmode,";
    strDML += " netsttime,";
    strDML += " netnotes ";
    strDML += " FROM nets ";
    strDML += " WHERE id = " + QString("%1").arg(id);
    strDML += " ORDER BY netname";
    //qDebug() << "preparing Select SQL:" << sqlStr;
    qry.prepare(strDML);
    qry.exec();
    qry.first();
    if ( qry.lastError().type() == QSqlError::NoError )
    {
        err = QSqlError::NoError;
        errmsg = "";
    }
    else
    {
        qDebug() << "DB Error on Nets query: " << strDML
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
    }
    return qry;
}

QSqlQuery Nets::findNets()
{
    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery qry(db);
    QString strDML;

    strDML  = "SELECT id,";
    strDML += " timebase,";
    strDML += " occurs,";
    strDML += " dayofweek,";
    strDML += " netname,";
    strDML += " netfreq,";
    strDML += " netband,";
    strDML += " netmode,";
    strDML += " netsttime,";
    strDML += " netnotes ";
    strDML += " FROM nets ";
    strDML += " ORDER BY netname ASC";
    //qDebug() << "preparing Select SQL:" << strDML;
    qry.prepare(strDML);
    qry.exec();
    switch ( qry.lastError().type() )
    {
    case QSqlError::NoError:
        err = QSqlError::NoError;
        errmsg = "";
        break;
    case QSqlError::ConnectionError:
        qDebug() << "DB Connection Error on Nets query: "
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        break;
    case QSqlError::StatementError:
        qDebug() << "DB Statement Error on Nets query: "
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        break;
    case QSqlError::TransactionError:
        qDebug() << "DB Transaction Error on Nets query: "
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        break;
    case QSqlError::UnknownError:
        qDebug() << "DB Unknown Error on Nets query: "
                 << " err-" << qry.lastError() << " " << qry.lastError().text();
        break;
    }
    return qry;
}
