#ifndef IMPORTSTATS_HPP
#define IMPORTSTATS_HPP
/*! \file importstats.hpp   */
#include <QtCore/QObject>
#include <QtWidgets/QDialog>

#include "ui_importstats.h"

namespace Ui {
class dlgImportStats;
}

/*! \ingroup QNETLOG
 * \details
 * This dialog provides control of database records import from files in ADIF format.
 *
 * Private Slots connected to signals intended for this form include:<br />
 * <span style="color: green;">void</span>:slotImported()<br />
 * <span style="color: green;">void</span>:slotImportAdd()<br />
 * <span style="color: green;">void</span>:slotImportUpdt()<br />             */
class dlgImportStats : public QDialog
{
    Q_OBJECT

public:
    /*!
     * \brief The dlgImportStats class
     * \param QWidget parent
     * \sa ADIF                           */
    explicit dlgImportStats(QWidget *parent = 0);

    /*!
     * \brief
     * &nbsp;&nbsp;dlgImportStats::<span style="color: green;">~dlgImportStats()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~dlgImportStats();

private Q_SLOTS:
    void slotImported();
    void slotImportAdd();
    void slotImportUpdt();

private:
    /*! \class statPrivateData
     * \brief Local definition of private data  */
    class statPrivateData;
    statPrivateData *stat_data;          //!< *d-ptr to local private data values

    Ui::dlgImportStats *ui;              //!< *d-ptr to GUI form
};

#endif // IMPORTSTATS_HPP
