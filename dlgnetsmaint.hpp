#ifndef DLGNETSMAINT_HPP
#define DLGNETSMAINT_HPP
/*! \file dlgnetsmaint.hpp */
#include <QtCore/QEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QShowEvent>
#include <QtGui/QMoveEvent>

#include "ui_dlgnetsmaint.h"

class QSqlTableModel;
class QSqlQuery;

namespace Ui {
class dlgNetsMaint;
}

/*! \ingroup QNETLOG
 * \class dlgNetsMaint
 * \details
 * This dialog provides database maintenance functions for the Nets table
 * in the database.
 *
 * Private Slots connected to widgets in this form include:<br />
 * <span style="color: green;">void</span>:slotAddNet() - connected to ui->btnAdd click signal<br />
 * <span style="color: green;">void</span>:slotChgNet() - connected to ui->btnChg click signal<br />
 * <span style="color: green;">void</span>:slotDelNet() - connected to ui->btnDel click signal<br />
 * <span style="color: green;">void</span>:slotCancel() - connected to ui->btnCancel click signal<br />
 * <span style="color: green;">void</span>:slotReset()  - connected to ui->btnReset click signal<br />
 * <span style="color: green;">void</span>:slotRowSel() - connected to ui->tblNets rowselchanged signal<br />        */
class dlgNetsMaint : public QDialog
{
    Q_OBJECT

public:
    /*!
    * \brief The Nets Database Table Maintenance Dialog */
    explicit dlgNetsMaint(QWidget *parent = 0);
    virtual ~dlgNetsMaint();

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);
    void moveEvent(QMoveEvent *);
    void closeEvent(QCloseEvent *);

private slots:
    void slotAddNet();
    void slotChgNet();
    void slotDelNet();
    void slotCancel();
    void slotReset();
    void slotRowSel();

private:
    /*! \class nmPrivateData
     * \brief Local data object definitions   */
    class nmPrivateData;
    nmPrivateData *nm_data;  //!< *d-ptr to local private data

    Ui::dlgNetsMaint *ui;    //!< *d-ptr to form definition

    void createObjects();
    void createConnections();
    void clearForm();
    void resetButtons();
    void saveNetRec();
    void updtNetRec();
    void displaySettings();
    void enableForm(bool b);
    void resizeTblCols();         //!< function to reset colums using stored values
    void loadNetsList();
    void saveColWidths();
    void readSettings();
    void saveSettings();
};

#endif // DLGNETSMAINT_HPP
