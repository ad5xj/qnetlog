#ifndef COMBOBOXDELEGATE_HPP
#define COMBOBOXDELEGATE_HPP
/*! \file combobocdelegate.hpp   */
#include <QtCore/QObject>
#include <QtCore/QSize>
#include <QtCore/QModelIndex>
#include <QtCore/QAbstractItemModel>
#include <QtWidgets/QStyledItemDelegate>
#include <QtWidgets/QStyleOptionViewItem>
#include <QtWidgets/QAbstractItemDelegate>

/*! \ingroup QNETLOG
 * \brief The ComboBoxDelegate class */
class ComboBoxDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ComboBoxDelegate(QWidget *parent=0);
    virtual ~ComboBoxDelegate();

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const Q_DECL_OVERRIDE;

    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const Q_DECL_OVERRIDE;
    void setEditorData(QWidget *editor, const QModelIndex &index) const Q_DECL_OVERRIDE;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const Q_DECL_OVERRIDE;

private:
};

#endif // COMBOBOXDELEGATE_HPP
