#ifndef STATUS_HPP
#define STATUS_HPP
/*! \file status.hpp */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>

#include "globals.hpp"

/*! \ingroup DATABASE
 * \class Status
 * \details
 * Constructs a database interface to the Status table. Implementation is the same
 * no matter what database server is used.<br /><br />
 * Database access is provided by public methods:<br><br>
 * <span style="color: green;">bool</span>:save(StatusRec rec)<br />
 * <span style="color: green;">bool</span>:del(StatusRec rec)<br />
 * <span style="color: green;">bool</span>:update(StatusRec rec)<br />
 * <span style="color: green;">QSqlQuery</span>:findBands()<br />
 * <span style="color: green;">QSqlQuery</span>:findBandsID(int id) and<br />
 * <span style="color: green;">QSqlQuery</span>:findBandsRecord(int id)<br /><br /> */
class Status : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)        //!<
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
     * <span style="color: green;">QObject::Status(QWidget * parent = 0)</span><br><br>
     * &nbsp;&nbsp;The implementation of the interface to the Status table in the database
     * provides a wrapper around the SQL functions needed to access and maintain
     * records in the status table of the database.
     * \param QObject *parent                     */
    explicit Status(QObject *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;Status::<span style="color: green;">~Status(QWidget * parent = 0)</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~Status();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(StatusRec rec)</span><br /><br />
     * SQL function to save status record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(StatusRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(Status rec)</span><br /><br />
     * SQL function to delete a specific status record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(StatusRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(StatusRec rec)</span><br /><br />
     * SQL function to update a specific status record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(StatusRec);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findStats(StatusRec rec)</span><br /><br />
     * SQL query returning all rows in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findStats();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findStatusID(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findStatID(int id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findStatsRecord(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findStatsRecord(int id);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;
};

#endif // STATUS_HPP

