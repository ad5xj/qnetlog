#include <QtCore/QDebug>
#include <QtGui/QPainter>
#include <QtWidgets/QComboBox>

#include "comboboxdelegate.hpp"

ComboBoxDelegate::ComboBoxDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{
    //
}

ComboBoxDelegate::~ComboBoxDelegate()
{
    //
}

QSize ComboBoxDelegate::sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const
{
    return QStyledItemDelegate::sizeHint(option, index);
}

QWidget* ComboBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStringList itemlist;
    if ( index.column() == 9 )
    {
        itemlist << "Fixed" << "Mobile" << "Port/QRP";
    }
        else if (index.column() == 3 )
    {
        itemlist << "Contacted"  << "Early Ck In"
                 << "Reg Chk in" << "Check Out"
                 << "Comments"   << "Short time"
                 << "Traffic"    << "Re-Check"
                 << "Radio Check";
    }
    QComboBox *selector = new QComboBox(parent);
    selector->setFrame(false);
    selector->addItems(itemlist);
    selector->setCurrentIndex(0);
    return selector;
}

void ComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    qint16 val = index.model()->data(index, Qt::EditRole).toInt();

    QComboBox *selector = static_cast<QComboBox*>(editor);
    selector->setCurrentIndex(val);
}

void ComboBoxDelegate::setModelData(QWidget *selector, QAbstractItemModel *model, const QModelIndex &index) const
{
    // find the item in the model and update it
    QComboBox *comboBox = static_cast<QComboBox*>(selector);
    model->setData(index, comboBox->currentIndex(), Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)  // to get rid of warnings

    // resize to container
    editor->setGeometry(option.rect);
}

