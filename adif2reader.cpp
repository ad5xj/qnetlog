/*************************************************************************
 *  Impementation of the ADIF Specific XML Reader                        *
 *  interface adifreader.hpp                                             *
 *               -------------------                                     *
 *  begin                : MAY 2014                                      *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 *  GNU General Public License for more details.                         *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#include <QtCore/QDebug>
#include <QtCore/QXmlStreamReader>
#include <QtCore/QDebug>
#include <QtWidgets/QApplication>

#include "adif2reader.hpp"

ADIF2Reader::ADIF2Reader(QWidget *parent) : QObject(parent)
{
    //
}

void ADIF2Reader::read(QIODevice *device)
{
    QString val  = "";
    QXmlStreamReader xml;
    errorString = "";

    xml.setDevice(device);

    do
    {
        QString name = xml.name().toString();
        QString ver  = xml.attributes().value("version").toString();

        if ( xml.StartElement )
        {
            QString elname = xml.tokenString();
            saveToken(elname);
        }
        else
            xml.readNextStartElement();
    } while ( !xml.atEnd() );
}

/*!
 * \brief
 * saveToken function to preserve tokens for examination later.
 *
 * \param elname as QString
 */
void ADIF2Reader::saveToken(QString elname)
{
    // not used yet just output in debug
    qDebug() << elname;
}
