#ifndef BANDS_HPP
#define BANDS_HPP
/*! \file bands.hpp */
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>

/*! \defgroup DATABASE */
#include "globals.hpp"


/*! \ingroup DATABASE
 * \class Bands
 * \brief
 * <span style="color: green;">QObject::Bands(QWidget * parent = 0)</span><br><br>
 * &nbsp;&nbsp;The implementation of the interface to the Bands table in the database
 * provides a wrapper around the SQL functions needed to access and maintain
 * records in the bands table of the database. */
class Bands : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 err READ getLastErr)
    Q_PROPERTY(QString errmsg READ getLastErrMsg)

public:
    /*!
     * \details
     * Constructs a database interface to the Bands table. Implementation is the same
     * no matter what database server is used.<br /><br />
     * Database access is provided by public methods:<br><br>
     * <span style="color: green;">bool</span>:save(LocTypeRec rec)<br />
     * <span style="color: green;">bool</span>:del(LocTypeRec rec)<br />
     * <span style="color: green;">bool</span>:update(LocTypeRec rec)<br />
     * <span style="color: green;">QSqlQuery</span>:findBands()<br />
     * <span style="color: green;">QSqlQuery</span>:findBandsID(int id) and<br />
     * <span style="color: green;">QSqlQuery</span>:findBandsRecord(int id)<br /><br />
     * \param QObject *parent                     */
    explicit Bands(QObject *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;Bands::<span style="color: green;">~Bands()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt.      */
    virtual ~Bands();

    quint32 getLastErr()    { return err; }
    QString getLastErrMsg() { return errmsg; }

    /*!
     * \brief
     * &nbsp;&nbsp;bool <span style="color: green;">save(BandsRec rec)</span><br /><br />
     * SQL function to save bands record. This is an interface function to perform
     * SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool save(BandsRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">del(BandsRec rec)</span><br /><br />
     * SQL function to delete a specific bands record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool del(BandsRec);

    /*!
     * &nbsp;&nbsp;bool <span style="color: green;">update(BandsRec rec)</span><br /><br />
     * SQL function to update a specific bands record. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return bool Success returns true             */
    bool update(BandsRec);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findBands(BandsRec rec)</span><br /><br />
     * SQL query returning all rows in the table. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findBands();

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findBandsID(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findBandsID(int id);

    /*!
     * &nbsp;&nbsp;QSqlQuery <span style="color: green;">findBandsID(int id)</span><br /><br />
     * SQL query returning all fields for row with id. This is an interface function
     * to perform SQL database functions regardless of what type of database is used.
     * \return QSqlQuery Returns a QSqlQuery result to the caller     */
    QSqlQuery findBandsRecord(int id);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;err : <span style="color: green;">quint32</span>
     * </div><br /><br />
     * This property holds the last error number to be returned.
     *
     * The number will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is QSqlDatabase::NoError (0).<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErr() </div>     */
    quint32 err;

    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errmsg : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errmsg;

};

#endif // BANDS_HPP

