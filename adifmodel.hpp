/*************************************************************************
 *  Interface adif2model.hpp                                             *
 *  for QNetLogger ADIF 2 import / export function                       *
 *                                                                       *
 *               -------------------                                     *
 *  begin                : JUNE 2014                                     *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied       *
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     *
 *  THE AUTHOR NOR SUPPLIER OF THIS SOFTWARE ARE NOT LIBABLE FOR ANY     *
 *  CONSEQUENCE OF USE OF THIS PRODUCT EITHER IMPLIED OR ACTUAL. See     *
 *  the GNU General Public License for more details.                     *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#ifndef ADIFMODEL_HPP
#define ADIFMODEL_HPP
/*! \file  adifmodel.hpp  */
#include <QtCore/QFileInfo>
#include <QtCore/QDir>
#include <QtCore/QVector>
#include <QtXml>
#include <QtXmlPatterns/QXmlNamePool>
#include <QtXmlPatterns/QSimpleXmlNodeModel>
#include <QtXmlPatterns/QAbstractXmlNodeModel>
#include <QtXmlPatterns/QXmlNodeModelIndex>

/*! \ingroup QNetLog */
/*! \defgroup ADIF */

/*! \ingroup ADIF
 * \class ADIFModel
 * \param QXmlNamePool &namePool
 *
 * \brief
 * Simple XML Node Model for use with the  ADIF 2 import / export functions
 *
 * Current code base is for ADIF 2.2.7 .adi file. ADIF .adx format not covered.
 * format should follow adif.org specs as of 06-11-2011  */
class ADIFModel : public QSimpleXmlNodeModel
{

public:
    ADIFModel(const QXmlNamePool &namePool);

    QXmlNodeModelIndex nodeFor(const QString &fileName) const;

    virtual QXmlNodeModelIndex::DocumentOrder compareOrder(const QXmlNodeModelIndex&, const QXmlNodeModelIndex&) const;
    virtual QXmlName name(const QXmlNodeModelIndex &node) const;
    virtual QUrl documentUri(const QXmlNodeModelIndex &node) const;
    virtual QXmlNodeModelIndex::NodeKind kind(const QXmlNodeModelIndex &node) const;
    virtual QXmlNodeModelIndex root(const QXmlNodeModelIndex &node) const;
    virtual QVariant typedValue(const QXmlNodeModelIndex &node) const;

protected:
    virtual QVector<QXmlNodeModelIndex> attributes(const QXmlNodeModelIndex &element) const;
    virtual QXmlNodeModelIndex nextFromSimpleAxis(SimpleAxis, const QXmlNodeModelIndex&) const;

private:
    /*! \enum complexType    */
    enum complexType
    {
        sequence
    };

    /*! \enum simpleType    */
    enum simpleType
    {
        restriction
    };

    /*! \enum simpleContent    */
    enum simpleContent
    {
        extension
    };

    inline QXmlNodeModelIndex nextSibling(const QXmlNodeModelIndex &nodeIndex,
                                          const QXmlName &name,
                                          qint8 offset) const;
    inline const QFileInfo &toFileInfo(const QXmlNodeModelIndex &index) const;
    inline QXmlNodeModelIndex toNodeIndex(const QFileInfo &index,QXmlNodeModelIndex::NodeKind attributeName) const;
    inline QXmlNodeModelIndex toNodeIndex(const QFileInfo &index) const;

    /*! \todo TODO:
     One possible improvement is to use a hash, and use the &*&value()
     trick to get a pointer, which would be stored in data() instead
     of the index.    */
    const QDir::Filters         m_filterAllowAll;
    const QDir::SortFlags       m_sortFlags;

    QVector<QXmlName>           m_names;
    mutable QVector<QFileInfo>  m_fileInfos;
};
#endif // ADIFMODEL_HPP
