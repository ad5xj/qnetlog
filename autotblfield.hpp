#ifndef AUTOTBLFIELD_HPP
#define AUTOTBLFIELD_HPP
#include <QtCore/QSize>

#include "rptabstracttablefield.hpp"

class QTextDocument;
class QTextTableCell;
class QAbstractItemModel;

namespace NetReports {

class AutoTableElementPrivate;

/*! \ingroup REPORTS
 * \class AutoTableField
 * \brief
 * The NetReports::AutoTableField class represents a table in the report,
 * whose data is provided by a QAbstractItemModel. */
class AutoTableField : public AbstractTableElement
{
public:
    /*! \details
     * A header row is added if the QAbstractItemModel has horizontal header data,
     * and a header column is added if the QAbstractItemModel has vertical header data.
     * The header row is repeated on every page if the table is broken across page
     * boundaries.
     * \param QAbstractItemModel* tableModel      */
    explicit AutoTableField( QAbstractItemModel* tableModel );

    /*! \brief
     * Creates a table field that does not have an associated model
     * yet. The association will be done later using the model key.
     * \param const QString& modelKey      */
    explicit AutoTableField( const QString& modelKey );

    /*! \brief
     * Copies a table element.
     * The model and settings are copied over.
     * \param const AutoTableField &other     */
    AutoTableField(const AutoTableField &other);

    /*! \brief
     * Copies the model and settings from another table field.
     * \param const AutoTableField *other      */
    AutoTableField &operator=(const AutoTableField &other);

    ~AutoTableField();


    /*! \brief
     * \return the model associated with this element      */
    QAbstractItemModel* tableModel() const;

    /*! \brief
     * set the model associated with this element      */
    void setTableModel(QAbstractItemModel *tableModel);

    /**
     * set the model key associated with this element
     * \since 1.4
     */
    void setModelKey(const QString &modelKey);

    /**
     * Sets whether to show a vertical header (showing header data from the model,
     * or row numbers by default).
     * This is true by default, call setVerticalHeaderVisible(false) to hide the vertical header.
     */
    void setVerticalHeaderVisible( bool visible );

    /**
     * Sets whether to show a horizontal header, showing header data from the model.
     * This is true by default, call setHorizontalHeaderVisible(false) to hide the horizontal header.
     */
    void setHorizontalHeaderVisible( bool visible );

    /**
     * \return true if the vertical header will be visible
     * \since 1.1
     */
    bool isVerticalHeaderVisible() const;

    /**
     * \return true if the horizontal header will be visible
     * \since 1.1
     */
    bool isHorizontalHeaderVisible() const;

    /**
     * Sets the background color of the headers
     * The default color is gray.
     * Call setHeaderBackground(QBrush()) to disable the background color
     * and have transparent headers instead.
     */
    void setHeaderBackground( const QBrush& brush );

    /**
     * \return the background color of the headers
     * \since 1.4
     */
    QBrush headerBackground() const;

    /**
     * Sets the size of the decoration icons, in pixels.
     * This is used when setting the DecorationRole to a QIcon in the model.
     * Note that the model can also set it to a QPixmap or QImage, in which case
     * the size in the report will simply be the size of the pixmap or image.
     * \since 1.1
     */
    void setIconSize( const QSize& iconSize );

    /**
     * \return the size passed to setIconSize
     * \since 1.3
     */
    QSize iconSize() const;

    /**
     * @internal
     * @reimp
     */
    void build( ReportBuilder& ) const;
    /**
     * @internal
     * @reimp
     */
    Element* clone() const;

    enum {
        DecorationAlignmentRole = 0x2D535FB1, ///< This model role allows to specify whether the icon should go before the text (Qt::AlignLeft) or after the text (Qt::AlignRight).
        NonBreakableLinesRole = 0x2D535FB2 ///< This model role allows to specify that line-breaking is not allowed in this table cell. \since 1.7.
    };

private:
    AutoTableElementPrivate* const d;
};

}

#endif // AUTOTBLFIELD_HPP

