/*************************************************************************
 *  Interface adif2exporter.hpp for QNetLog ADIF 2 export function       *
 *  implemented by adif2exporter.cpp                                     *
 *               -------------------                                     *
 *  begin                : JUNE 2014                                     *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied       *
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     *
 *  THE AUTHOR NOR SUPPLIER OF THIS SOFTWARE ARE NOT LIBABLE FOR ANY     *
 *  CONSEQUENCE OF USE OF THIS PRODUCT EITHER IMPLIED OR ACTUAL. See     *
 *  the GNU General Public License for more details.                     *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#ifndef ADIF2EXPORTER_HPP
#define ADIF2EXPORTER_HPP
/*! \file  adif2exporter.hpp  */
#include <QtCore/QObject>
#include <QtCore/QIODevice>
#include <QtWidgets/QWidget>

#include "globals.hpp"

/*! \ingroup ADIF
 * \class ADIF2Exporter
 * \brief
 * Near XML Export Object to write ADIF 2 data to file from contacts DB records
 * displayed in a table on the contacts tab
 *
 * Current code base is for ADIF 2.2.7 .adi file
 * format should follow adif.org specs as of 06-11-2011  */
class ADIF2Exporter : public QObject
{
    Q_OBJECT

public:
    explicit ADIF2Exporter(QWidget *parent=0);


    CallSignsRecord rec;  /*!<  Global definition of contacts record  */

    /*!
     * \brief
     * Helper function to implement IODevice write function from QIODevice
     *
     * \param iodevice                    */
    void write(QIODevice *iodevice);

    /*!
     * \brief
     * Helper function to process export of data to ADIF 2 .adi file
     *
     * \param fill    as string that is the URL of the output ADIF file (.adi)
     * \param indata  as string list that is the data rows of input from the application */
    void processADIF2Export(QString fill, QStringList indata);

signals:
    /*!
     * \brief
     * Signal emited to update MainWindow with status message
     *
     * \param msg
     * \return msg as QString formatted for display on statusbar  */
    QString signalUpdateStatusMsg(QString msg);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errorString : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errorString;

    /*! \typedef struct tblhdrs   */
    typedef struct local_tbl_hdrs
    {
        int     ID;
        int     seq;
        QString label;
        QString dbfield;
        QString adif_field;
    } tblhdrs;

    /*!
     * \brief
     * Helper function to save token names when needed...
     * currently unimplemented
     *
     * \param QString elname     */
    void saveToken(QString elname);
};
#endif // ADIF2EXPORTER_HPP
