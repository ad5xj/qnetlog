#include <QtCore/QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>

#include "tablehdrs.hpp"


TableHdrs::TableHdrs(QObject *parent) : QObject(parent) {  }

QSqlQuery TableHdrs::readHeadersSeq()
{
    QSqlDatabase db = QSqlDatabase::database();
    QString dmlstr = "SELECT ";
    dmlstr += "[contcols].[id],";
    dmlstr += "[contcols].[seq],";
    dmlstr += "[contcols].[hdrtxt],";
    dmlstr += "[contcols].[dbfield],";
    dmlstr += "[contcols].[adifnam] ";
    dmlstr += "FROM [contcols] ";
    dmlstr += "ORDER BY [contcols].[seq] ASC;";
    QSqlQuery qry(db);
    qry.prepare(dmlstr);
    qry.exec();
    //qDebug() << dmlstr << " - " << qry.lastError().text();
    return qry;
}
