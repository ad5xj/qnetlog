TEMPLATE = app
TARGET   = qnetlog
DESTDIR  = lib

#VER_MAJ  = 0
#VER_MIN  = 97
#VER_PAT  = 3
#VER_BUILD = 015

QT       += widgets svg sql xml xmlpatterns
CONFIG   += qt thread resources app_bundle
unix:  {
CONFIG   += x11
}
win32: {
CONFIG   +=  windows
}

MOC_DIR     = moc
OBJECTS_DIR = obj
RCC_DIR     = rcc

HEADERS += \
    globals.hpp

HEADERS += \
    tablehdrs.hpp \
    adif2exporter.hpp \
    adif2parser.hpp \
    adif2reader.hpp \
    adifmodel.hpp

HEADERS += \
    nets.hpp \
    bands.hpp \
    modes.hpp \
    status.hpp \
    loctyp.hpp \
    callsigns.hpp \
    sessionloghdr.hpp \
    sessionlogdtl.hpp \
    chkboxdelegate.hpp \
    comboboxdelegate.hpp \
    editsessionlogmodel.hpp \
    yesnodelegate.hpp \
    dlgimportstats.hpp \
    dlgbandsmaint.hpp \
    dlgnetsmaint.hpp \
    dlgcallsigns.hpp \
    dlglogmaint.hpp \
    dlgnetslogged.hpp \
    dlglogedit.hpp \
    dlgimportops.hpp \
    about.hpp \
    mainwindow.hpp

SOURCES += \
    adif2exporter.cpp \
    adif2parser.cpp \
    adif2reader.cpp

SOURCES += \
    nets.cpp \
    bands.cpp \
    modes.cpp \
    callsigns.cpp \
    tablehdrs.cpp \
    chkboxdelegate.cpp \
    comboboxdelegate.cpp \
    editsessionlogmodel.cpp \
    yesnodelegate.cpp \
    status.cpp \
    loctyp.cpp \
    sessionloghdr.cpp \
    sessionlogdtl.cpp \
    dlgimportstats.cpp \
    dlgbandsmaint.cpp \
    dlgnetsmaint.cpp \
    dlgcallsigns.cpp \
    dlglogmaint.cpp \
    dlgnetslogged.cpp \
    dlglogedit.cpp \
    dlgimportops.cpp \
    about.cpp \
    mainwindow.cpp \
    main.cpp

FORMS += \
    about.ui \
    dlgbandsmaint.ui \
    dlgcallsigns.ui \
    dlglogmaint.ui \
    dlgnetslogged.ui \
    dlglogedit.ui \
    dlgnetsmaint.ui \
    dlgimportoptions.ui \
    importstats.ui \
    mainwindow.ui

RESOURCES += application.qrc

#unix:!macx: LIBS += -LQtRpt/lib -lQtRpt

