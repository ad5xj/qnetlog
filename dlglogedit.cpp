#include <QtWidgets/QMessageBox>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>

#include "callsigns.hpp"
#include "sessionlogdtl.hpp"
#include "dlglogedit.hpp"

class DlgLogEdit::dtlPrivateData
{
public:
    bool font_bold;
    bool font_italic;
    bool font_underline;
    bool goodCallSign;

    quint16 color_r;
    quint16 color_g;
    quint16 color_b;
    quint16 color_a;
    quint32 sizeh;
    quint32 font_size;

    QString currentCall;

    SessionDtlRecord rec;         //!< record struct to set record values
};

DlgLogEdit::DlgLogEdit(QWidget *parent) : QDialog(parent)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    dtl_data = new dtlPrivateData;

    ui = new Ui::DlgLogEdit;
    ui->setupUi(this);

    QStringList itemlist;
    itemlist << "Contacted"  << "Early Ck In"
             << "Reg Chk in" << "Check Out"
             << "Comments"   << "Short time"
             << "Traffic"    << "Re-Check"
             << "Radio Check";
    ui->cboStatus->addItems(itemlist);

    connect(ui->grpButtons,   SIGNAL(accepted()),   this,SLOT(slotSaveLogEntry()));
    connect(ui->grpButtons,   SIGNAL(rejected()),   this,SLOT(close()));
    connect(ui->chkMARS,      SIGNAL(toggled(bool)),this,SLOT(slotMARSchgd()));
    connect(ui->editCallSign, SIGNAL(textChanged(QString)),this,SLOT(slotCallChgd(QString)));
    connect(ui->lstViewCalls, SIGNAL(doubleClicked(QModelIndex)), this,SLOT(slotCallSelected()));

    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

DlgLogEdit::~DlgLogEdit() { delete ui; }

// ============================================================= //
// ===================  Event Handlers  ======================== //
// ============================================================= //
void DlgLogEdit::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

// ============================================================= //
// ==============  Property Sets And Gets  ===================== //
// ============================================================= //
void DlgLogEdit::setHdrNdx(quint32 ndx)
{
    m_nethdrndx = ndx;
}

void DlgLogEdit::setDtlNdx(quint32 ndx)
{
    m_netdtlndx = ndx;
    findDtlRec();
}

// ============================================================= //
// ==================   Public Methods  ====================== //
// ============================================================= //
void DlgLogEdit::displayDtlRec()
{
    int err = 0;
    quint32 n = 0;
    QString s = "";
    SessionDetail dtl;
    QSqlQuery query = dtl.getLogEntry(m_netdtlndx);
    Q_UNUSED(err)

    // check for valid query
    query.first();
    if ( !query.isValid() )
    {
        err = dtl.getLastErr();
        QString errmsg = dtl.getLastErrMsg();
        QString msg = tr("An error has occured on the database:");
        msg += QString("%1").arg(err);
        msg += errmsg;
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.exec();
        reject();
    }
    // we have results to show
    s = tr("Editing Record with ID=");
    s += QString("%1").arg(m_netdtlndx);
    this->setWindowTitle(s);
    s = "";

    s = query.record().field("rectime").value().toString();
    ui->editLoggedTime->setText(s);
    s = "";
    s = query.record().field("callsign").value().toString();
    ui->editCallSign->setText(s);
    s = "";
    s = query.record().field("marscall").value().toString();
    ui->editMarsCall->setText(s);
    s = query.record().field("firstname").value().toString();
    ui->editName->setText(s);
    s = query.record().field("city").value().toString();
    ui->editCity->setText(s);
    s = query.record().field("st").value().toString();
    ui->editSt->setText(s);
    s = query.record().field("grid").value().toString();
    ui->editGrid->setText(s);
    n = query.record().field("dtlseq").value().toInt();
    ui->spinSeq->setValue(n);
    switch ( query.record().field("stationtype").value().toInt() )
    {
    case 1:
        ui->btnFixed->setChecked(true);
        break;
    case 2:
        ui->btnMobile->setChecked(true);
        break;
    case 3:
        ui->btnPortQrp->setChecked(true);
        break;
    }
    int v = query.record().field("ares").value().toInt();
    if ( v == 1 ) ui->chkARES->setChecked(true);
    v = query.record().field("satern").value().toInt();
    if ( v == 1 ) ui->chkSATERN->setChecked(true);
    v = query.record().field("mars").value().toInt();
    if ( v == 1 ) ui->chkMARS->setChecked(true);
    v = query.record().field("traffic").value().toInt();
    if ( v == 1 ) ui->chkTraffic->setChecked(true);
    s = query.record().field("notes").value().toString();
    n = ui->cboStatus->findText(query.record().field("status").value().toString());
    ui->cboStatus->setCurrentIndex(n);
    ui->editComment->document()->setPlainText(s);
    ui->editMarsCall->setEnabled(ui->chkMARS->isChecked());
}

// ============================================================= //
// ===================  Slot Handlers  ========================= //
// ============================================================= //
void DlgLogEdit::slotCallChgd(QString txt)
{
    bool valid = false;
    int x = 0;
    QString c;
    CallSigns calls;
    QSqlQuery n;

    c = txt.trimmed().toUpper();
    bool mobile = c.contains("/");
    if ( mobile )
    {
        x = c.indexOf("/",0);
        c = c.left(x);
    }
    // clear the list if any items shown
    ui->lstViewCalls->clear();
    QString strRow = "";
    ui->lstViewCalls->clear();
    if ( c == "" ) return;
    // find the records
    n = calls.findCallSigns(c);
    n.first();
    if (!n.isValid())
    {
        ui->editCallSign->setText(c);
        return;
    }
    // good records so start by adding first record
    // add first one found
    valid = n.first();
    if ( valid )
    {
        do
        {
          // insert the rest of the rows from the data
          strRow = n.value(1).toString();
          if ( strRow.left(c.length()) == c )
              new QListWidgetItem(strRow, ui->lstViewCalls);
          valid = n.next();
        } while( valid );
    }
    ui->lstViewCalls->setEnabled(true);
    if ( dtl_data->goodCallSign )
    {
        int ret = 0;
        QMessageBox msgBox;
        QSqlQuery qry;
        CallSignsRecord rec;
        CallSigns calls;
        SessionDetail log;
        SessionDtlRecord logrec;

        rec.Call = dtl_data->currentCall;
        qry = calls.findCallSigns(dtl_data->currentCall);
        qry.first();
        if ( dtl_data->goodCallSign )
        {
            rec.Call = dtl_data->currentCall;
            QSqlQuery qry = calls.findCallSigns(dtl_data->currentCall);
            qry.first();
            if ( qry.isValid() )
            {
                msgBox.setWindowTitle(tr("New Data Available"));
                QString msg = tr("The data on file is different. ");
                msg += tr("Selecting NO or CANCEL below will not change the information for the current contact record. ");
                msg += tr("To use the new information -  answer YES. ");
                msg += tr("Otherwise answer NO to continue.");
                msgBox.setInformativeText(msg);
                msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
                ret = msgBox.exec();
                switch ( ret )
                {
                case QMessageBox::Yes:
                    ui->editName->setText(qry.value(2).toString());
                    ui->editCity->setText(qry.value(3).toString());
                    ui->editSt->setText(qry.value(4).toString());
                    ui->editGrid->setText(qry.value(5).toString());
                    break;
                case QMessageBox::No:
                    break;
                case QMessageBox::Cancel:
                    break;
                }
            }
            else
                return;
        }
        //qDebug() << "Checking Log for callsign..." << currentCall;
        logrec.callsign = dtl_data->currentCall;
        logrec.hdrndx = m_nethdrndx;
        logrec.id = m_netdtlndx;
        logrec.callsign = dtl_data->currentCall;
        // begin
        ui->txtLstLogged->setText(log.findLastLogged(logrec));
    }
    ui->editCallSign->setText(c);
}

void DlgLogEdit::slotCallSelected()
{
    dtl_data->currentCall = ui->lstViewCalls->item(ui->lstViewCalls->currentRow())->text();
    dtl_data->goodCallSign = true;
    ui->editCallSign->setText(dtl_data->currentCall);
}

void DlgLogEdit::slotMARSchgd() { ui->editMarsCall->setEnabled(ui->chkMARS->isChecked()); }

bool DlgLogEdit::slotSaveLogEntry()
{
    // scrape form for data to save
    SessionDetail dtl;
    SessionDtlRecord rec;

    // clear record for defaults
    rec.id = 0;
    rec.dtlseq = 0;
    rec.ares = 0;
    rec.mars = 0;
    rec.satern = 0;
    rec.traffic = 0;
    // fill with current values
    rec.id = m_netdtlndx;
    rec.rectime = ui->editLoggedTime->text().trimmed();
    rec.callsign = ui->editCallSign->text().trimmed();
    rec.marscall = ui->editMarsCall->text().trimmed();
    rec.firstname = ui->editName->text().trimmed();
    if ( ui->chkARES->isChecked() )   rec.ares = 1;
    if ( ui->chkMARS->isChecked() )   rec.mars = 1;
    if ( ui->chkSATERN->isChecked() ) rec.satern = 1;
    if ( ui->btnFixed->isChecked() )
        rec.stationtype = 1;
    else if ( ui->btnMobile->isChecked() )
        rec.stationtype = 2;
    else if ( ui->btnPortQrp->isChecked() )
        rec.stationtype = 3;
    else
        rec.stationtype = 1;
    rec.city = ui->editCity->text().trimmed();
    rec.st = ui->editSt->text().trimmed();
    rec.gridsquare = ui->editGrid->text().trimmed();
    rec.dtlseq = ui->spinSeq->value();
    rec.status = ui->cboStatus->currentText().trimmed();
    rec.notes = ui->editComment->document()->toPlainText();

    if ( dtl.update(rec) ) return true;
    return false;
}



// ============================================================= //
// =================  Friendly Functions  ====================== //
// ============================================================= //
void DlgLogEdit::findDtlRec()
{   // prepare display to show results
    // property m_netdtlndx must be set before calling
    dtl_data->rec.id = m_netdtlndx;
    clearForm();
    displayDtlRec();
}


void DlgLogEdit::clearForm()
{
    ui->editLoggedTime->setText("");
    ui->btnFixed->setChecked(true);
    ui->btnMobile->setChecked(false);
    ui->btnPortQrp->setChecked(false);
    ui->chkARES->setChecked(false);
    ui->chkMARS->setChecked(false);
    ui->chkSATERN->setChecked(false);
    ui->chkTraffic->setChecked(false);
    ui->editCallSign->setText("");
    ui->editComment->document()->setPlainText("");
}
