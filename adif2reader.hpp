/*************************************************************************
 *  Interface adif2reader.hpp for QNetLog ADIF 2 import function         *
 *  implemented by adif2reader.cpp                                       *
 *               -------------------                                     *
 *  begin                : JUNE 2014                                     *
 *  copyright            : (C) 2014 by AD5XJ Ken Standard                *
 *  email                : ad5xj@arrl.net                                *
 *                                                                       *
 *                                                                       *
 *  This module contains code derived from source code contained in the  *
 *  Qt SDK Examples and Demos and is covered under the GPL and LGPL      *
 *  License by Digia Corporation. This modification retains that license *
 *  restriction and is also covered under the GNU GPL sited below.       *
 *                                                                       *
 *  This program is free software: you can redistribute it and/or modify *
 *  it under the terms of the GNU General Public License as published by *
 *  the Free Software Foundation, either version 3 of the License, or    *
 *  (at your option) any later version.                                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY OF ANY KIND; without even the implied       *
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     *
 *  THE AUTHOR NOR SUPPLIER OF THIS SOFTWARE ARE NOT LIBABLE FOR ANY     *
 *  CONSEQUENCE OF USE OF THIS PRODUCT EITHER IMPLIED OR ACTUAL. See     *
 *  the GNU General Public License for more details.                     *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/
#ifndef ADIF2READER_HPP
#define ADIF2READER_HPP
/*! \file adif2reader.hpp */
#include <QtCore/QObject>
#include <QtCore/QIODevice>
#include <QtWidgets/QWidget>

#include "globals.hpp"

/*! \ingroup ADIF
 * \class ADIF2Reader
 * \Details
 * XML Reader Object
 *
 * To parse ADIF data from file   */
class ADIF2Reader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString errmsg READ getLastErrMsg)  //!<

public:
    /*!
     * \brief
     *
     * Current code base is for ADIF 2.2.7 .adi file
     * format should follow adif.org specs as of 06-11-2011
     */
    explicit ADIF2Reader(QWidget *parent=0);

    QString getLastErrMsg() { return errorString; }     //!< property return definition

    /*!
     * \brief
     * Read function to implement QIODevice functions
     *
     * \param device as QIODevice
     */
    void read(QIODevice *device);

private:
    /*! \brief
     * <div style="width: 70%; border-radius: 5px; background-color: #dddfbb;">
     * &nbsp;&nbsp;errorString : <span style="color: green;">QString</span>
     * </div><br /><br />
     * This property holds the message text to be displayed.
     *
     * The text will be returned from the database table handler upon last
     * action.<br /><br />
     *
     * The default value of this property is an empty string.<br /><br />
     *
     * <strong>Access functions:</strong><br />
     * <div style="width: 20%; border-radius: 8px; margin: 3px 3px 3px 3px; background-color: #eeeeee;">
     * &nbsp;&nbsp;QString	getLastErrMsg() </div>                               */
    QString errorString;

    void saveToken(QString elname);
};
#endif // ADIF2READER_HPP
