#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QStringList>
#include <QtWidgets/QFileDialog>

#include "adif2parser.hpp"
#include "adif2reader.hpp"
#include "dlgimportstats.hpp"
#include "dlgimportops.hpp"

class dlgImportOpts::importPrivateData
{
public:
    ADIF2Parser    *adif2parser;

    dlgImportStats *dlgStats;
};

dlgImportOpts::dlgImportOpts(QWidget *parent) : QDialog(parent)
{
    imp_data = new importPrivateData;

    ui = new Ui::dlgImportOptions;
    ui->setupUi(this);
    ui->retranslateUi(this);

    ui->lblNotes->setVisible(false);
    ui->btnAddNotes->setVisible(false);
    ui->btnRplNotes->setVisible(false);
    connectSlots();
}

dlgImportOpts::~dlgImportOpts()
{
    delete ui;
    delete imp_data;
}

void dlgImportOpts::slotImportADIF()
{
    QDir dir;
    QString param = "";
    QString dirstr = "";
    QString infileStr = "";
    QStringList fileslist;

    ui->btnImport->setEnabled(false);
    QFileDialog *file = new QFileDialog(this);
    dir.cd("../xml");
    dirstr = dir.canonicalPath();
    file->setDirectory(QDir(dirstr));
    file->setNameFilter("*.adi");
    if ( file->exec() )  fileslist = file->selectedFiles();
    infileStr = fileslist[0];
    delete file;
    qApp->processEvents();
    repaint();

    // create the string that gets sent to the parser
    if ( ui->btnCallDB->isChecked() )
        param += "1|";
    else
        param += "0|";
    if ( ui->btnMyOp->isChecked() )
        param += "1|";
    else
        param += "0|";
    if ( ui->btnMyCity->isChecked() )
        param += "1|";
    else
        param += "0|";
    if ( ui->btnMySt->isChecked() )
        param += "1|";
    else
        param += "0|";
    if ( ui->btnMyGrid->isChecked() )
        param += "1|";
    else
        param += "0|";
    if ( ui->btnAddNotes->isChecked() )
        param += "1";
    else
        param += "0";
    if ( infileStr.right(3) == "adi" ||
         infileStr.right(3) == "ADI"
       )
        ui->lblStatus->setText(tr("Importing ADIF 2 file...please wait"));
    imp_data->dlgStats = new dlgImportStats(this);

    imp_data->adif2parser = new ADIF2Parser(this);
    connect(imp_data->adif2parser, SIGNAL(signalDone()),       this,               SLOT(slotDone()));
    connect(imp_data->adif2parser, SIGNAL(signalImportAdd()),  imp_data->dlgStats, SLOT(slotImportAdd()));
    connect(imp_data->adif2parser, SIGNAL(signalImported()),   imp_data->dlgStats, SLOT(slotImported()));
    connect(imp_data->adif2parser, SIGNAL(signalImportUpdt()), imp_data->dlgStats, SLOT(slotImportUpdt()));

    imp_data->dlgStats->show();
    imp_data->adif2parser->processADIF2Import(infileStr,param);
}

void dlgImportOpts::slotUpdateStatusMsg(QString msg)
{
    ui->lblStatus->setText(msg);
}

void dlgImportOpts::slotDone()
{
    ui->btnImport->setEnabled(false);
    ui->btnImport->setVisible(false);
    ui->btnCancel->setText(tr("CLOSE"));
    ui->btnCancel->setFocus();
}

void dlgImportOpts::connectSlots()
{
    connect(ui->btnImport,SIGNAL(clicked()),this,SLOT(slotImportADIF()));
    connect(ui->btnCancel,SIGNAL(clicked()),this,SLOT(close()));
}
