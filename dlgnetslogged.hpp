#ifndef DLGNETSLOGGED_HPP
#define DLGNETSLOGGED_HPP
#include <QtCore/QObject>
#include <QtWidgets/QDialog>

#include "globals.hpp"
#include "nets.hpp"
#include "sessionloghdr.hpp"

#include "ui_dlgnetslogged.h"

namespace Ui {
class dlgNetsLogged;
}

/*! \ingroup QNETLOG
 * \class dlgNetsLogged
 * \details
 * This dialog provides database maintenance functions for the SessionHeader table
 * in the database.
 *
 * Properties that should be set before showing the dialog include:<br />
 * <span style="color: green;">void</span>:setNetname(int nam)<br />
 * <span style="color: green;">void</span>:setDateTime(QString)<br /><br />
 * Values for properties may be accessed with:<br />
 * <span style="color: green;">int</span>:getNetname()<br />
 * <span style="color: green;">QString</span>:getNetDateTime()<br />   */
class dlgNetsLogged : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(int p_netname READ getNetname WRITE setNetname)
    Q_PROPERTY(QString p_datetime READ getNetDateTime WRITE setDateTime)

public:
    /*!
     * \brief
     * The Database Maintenance Dialog for nets logged
     * \param QWidget parent
     * \sa Nets
     * \sa DATABASE                           */
    explicit dlgNetsLogged(QWidget *parent=0);

    /*!
     * \brief
     * &nbsp;&nbsp;dlgNetsLogged::<span style="color: green;">~dlgNetsLogged()</span><br><br>
     * Destructor method of this class. It will destroy all locally created
     * objects before destruction by Qt called after the closeEvent() funciton.      */
    virtual ~dlgNetsLogged();

    int     getNetname()            { return p_netname; }
    QString getNetDateTime()        { return p_datetime; }
    void    setNetname(int ndx)     { Q_UNUSED(ndx); } // mainwindow sets this so we display the right thing when starting
    void    setDateTime(QString dt) { Q_UNUSED(dt); }  // mainwindow sets this so we display the right thing when starting

signals:
    void netnameChgd();

protected:
    void changeEvent(QEvent *e);
    void showEvent();

private Q_SLOTS:
    void slotFindIt();

private:
    // local property vars
    int p_netname;

    QString p_datetime;
    // local var definitions
    /*! \class nmPrivateData
     * \brief Local definition of private data  */
    class NetsMaintPrivateData;
    NetsMaintPrivateData *nm_data;       //!< *d-ptr to local private data values
    // form definition
    Ui::dlgNetsLogged *ui;               //!< *d-ptr to GUI form
    // local private methods
    void findNetsLogged();
    void loadNetLog();
};
#endif // DLGNETSLOGGED_HPP
