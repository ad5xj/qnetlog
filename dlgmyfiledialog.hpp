#ifndef DLGMYFILEDIALOG_HPP
#define DLGMYFILEDIALOG_HPP
#include <QtCore/QObject>
#include <QtCore/QEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>

#include "ui_dlgmyfiledialog.h"

class QString;
class QStringList;

namespace Ui {
class dlgMyFileDialog;
}

class dlgMyFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit dlgMyFileDialog(QWidget *parent = 0);
    virtual ~dlgMyFileDialog();

    void setPath(QString path);
    void setPattern(QString pattern);
    void setTitle(QString title);
    QString getOpenFileName();

signals:
    QString signalNoFileSel(QString name);
    void signalFileSelected();

protected:
    void changeEvent(QEvent *e);

private slots:
    void slotBrowse();
    void slotFind();
    void slotOpenFileOfItem(int row, int col);
    void slotKeepName();

private:
    class dmfPrivateData;
    dmfPrivateData *dmf_data;

    Ui::dlgMyFileDialog *ui;

    void showFiles(const QStringList &files);
    QStringList findFiles(const QStringList &files, const QString &text);
};

#endif // DLGMYFILEDIALOG_HPP
